# -*- encoding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import
import xml.etree.ElementTree as ET

imp = Import('http://www.w3.org/2001/XMLSchema',
              location='http://www.w3.org/2001/XMLSchema.xsd')
imp.filter.add('http://tempuri.org/')
doctor = ImportDoctor(imp)

url = "http://85.194.72.27:8085/Sbarro/Webservice1.asmx?WSDL"
client = Client(url,retxml=True,doctor=doctor)

###categories
response =  client.service.GET_ODOO_POS_CATEGORIES('6000', '01533')
root = ET.fromstring(response)
for item in  root.findall(".//ODOO_POS_CATEGORIES"):
    #print item.findall("*")
    print item.find("./MSC_ITEM_CAT_DESC").text , item.find("./MSC_ITEM_CAT").text

###categories
response =  client.service.GET_ODOO_KDS_SECTIONS('6000', '02527')
root = ET.fromstring(response)
for item in  root.findall(".//ODOO_KDS_UNITS"):
    print item.find("./MUKS_SECTION").text , item.find("./MUKS_DESC").text

# ###payment type
# response =  client.service.GET_ODOO_PAYMENT_TYPES('6000')
# root = ET.fromstring(response)
# for item in  root.findall(".//ODOO_PAYMENT_TYPES"):
#     print item.findall("*")
#     #print item.find("./PTL_TRANS_DESC").text , item.find("./PTL_TRANS_CODE").text, item.find("./PTL_CODE").text
#
# ###Selling Items
# response =  client.service.GET_ODOO_SELLING_ITEMS('6000','01533')
# root = ET.fromstring(response)
# for item in  root.findall(".//ODOO_POS_SELLING_ITEMS"):
#     print "selling items",item.findall("*")
#     #print item.find("./ITEM_ITEM").text , item.find("./ITEM_DESC1").text, item.find("./LCL_PRICE1").text
#
# ###Recipe
# response =  client.service.GET_ODOO_RECIPES('6000','01533')
# root = ET.fromstring(response)
# for item in  root.findall(".//ODOO_RECIPES"):
#     print "recipe",item.findall("*")
#     #print item.find("./ITEM_ITEM").text , item.find("./ITEM_DESC1").text
#
# ###Bom with Choice
# response =  client.service.GET_ODOO_BOM_WITH_CHOICE('6000','01533')
# root = ET.fromstring(response)
# for item in  root.findall(".//ODOO_BOM_WITH_CHOICE"):
#     print item.findall("*")
#     #print item.find("./ITEM_ITEM").text , item.find("./ITEM_DESC1").text
#
#
# ###Employees
# response = client.service.GET_ODOO_EMPLOYEES('6000', '01533')
# root = ET.fromstring(response)
# for item in root.findall(".//ODOO_EMPLOYEES"):
#     print item.findall("*")

# ###Unit
# response = client.service.GET_ODOO_UNIT_PREFIX('6000','01533')
# root = ET.fromstring(response)
# for item in root.findall(".//ODOO_UNIT_PREFIX"):
#     print item.findall("*")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
