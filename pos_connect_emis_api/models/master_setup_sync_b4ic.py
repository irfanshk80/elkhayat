# -*- encoding: utf-8 -*-
from openerp import models, api,fields, _
import logging
from PIL import Image, ImageDraw, ImageFont
import arabic_reshaper
from bidi.algorithm import get_display
import sys
import base64
import StringIO
from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import
import xml.etree.ElementTree as ET
from datetime import datetime
from pytz import timezone
_logger = logging.getLogger(__name__)
reload(sys)
sys.setdefaultencoding('utf-8')

def call_emis_web_api(args={}):
    ### connection establishment
    imp = Import('http://www.w3.org/2001/XMLSchema',
                 location='http://www.w3.org/2001/XMLSchema.xsd')
    imp.filter.add('http://tempuri.org/')
    doctor = ImportDoctor(imp)

    url = "http://85.194.72.27:8085/Sbarro/Webservice1.asmx?WSDL"
    client = Client(url, retxml=True, doctor=doctor)

    bu = args.get('business_unit', False)
    unit = args.get('unit', False)
    res_list = []
    ###categories
    if args.get('method', False) == "POS_CATEGORIES":
        response = client.service.GET_ODOO_POS_CATEGORIES(bu , unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_POS_CATEGORIES"):
            record = []
            record.append(item.find("./MSC_BUS_UNIT").text)
            record.append(item.find("./MSC_ITEM_CAT").text)
            record.append(item.find("./MSC_ITEM_CAT_DESC").text)
            res_list.append(record)
    if args.get('method',False) == "KDS_UNITS":
        response = client.service.GET_ODOO_KDS_SECTIONS(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_KDS_UNITS"):
            record = []
            record.append(item.find("./MUKS_SECTION").text)
            record.append(item.find("./MUKS_DESC").text)
            res_list.append(record)

    elif args.get('method',False)== "PAYMENT_TYPES":###payment type
        response = client.service.GET_ODOO_PAYMENT_TYPES(bu)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_PAYMENT_TYPES"):
            record = []
            record.append(item.find("./PTL_BUS_UNIT").text)
            record.append(item.find("./PTL_CODE").text)
            record.append(item.find("./PTL_TRANS_CODE").text)
            record.append(item.find("./PTL_TRANS_DESC").text)
            record.append(item.find("./PTL_CARD_SER_CHRG").text)
            record.append(item.find("./PTL_CRE_BY").text)
            record.append(item.find("./PTL_CRE_DATE").text)
            res_list.append(record)

    elif args.get('method',False)== "SELLING_ITEMS":###SELLING_ITEMS
        response = client.service.GET_ODOO_SELLING_ITEMS(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_POS_SELLING_ITEMS"):
            record = []
            record.append(item.find("./ITEM_BUS_UNIT").text)
            record.append(item.find("./ITEM_ITEM").text)
            record.append(item.find("./ITEM_DESC1").text)
            record.append(item.find("./ITEM_DESC2").text if item.find("./ITEM_DESC2") is not None else '')
            record.append(item.find("./ITEM_UOM").text)
            record.append(item.find("./ITEM_CLASS").text)
            record.append(item.find("./ITEM_CAT").text)
            record.append(item.find("./ITEM_STATUS").text)
            record.append(item.find("./ITEM_STOCKED").text)
            record.append(item.find("./LCL_CODE").text)
            record.append(float(item.find("./LCL_PRICE").text))
            record.append(item.find("./LCL_PRICE1").text)
            record.append(item.find("./LCL_PRICE2").text)
            record.append(item.find("./LCL_NOTES").text if item.find("./LCL_NOTES") is not None else '')
            record.append(item.find("./LCL_ITEM_CAT").text)
            res_list.append(record)
    elif args.get('method',False)== "RECIPES": ###Recipe
        response = client.service.GET_ODOO_RECIPES(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_RECIPES"):
            record = []
            record.append(item.find("./ITEM_ITEM").text)
            record.append(item.find("./ITEM_DESC1").text)
            res_list.append(record)
    elif args.get('method', False) == "BOM_WITH_CHOICE": ###Bom with Choice
        response = client.service.GET_ODOO_BOM_WITH_CHOICE(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_BOM_WITH_CHOICE"):
            record = []
            record.append(item.find("./BOM_PARENT_PFX").text)
            record.append(int(item.find("./BOM_SEQ_NO").text ))
            record.append(item.find("./BOM_PARENT").text)
            record.append(item.find("./BOM_PARENT_REV").text)
            record.append(item.find("./PARENT_NAME").text)
            record.append(item.find("./BOM_COMPONENT").text)
            record.append(item.find("./BOM_COMPONENT_REV").text)
            record.append(item.find("./COMPONENT_NAME").text)
            record.append(float(item.find("./BOM_QTY_PER").text))
            record.append(item.find("./BOM_UOM").text)
            record.append(item.find("./BOM_COMPONENT1").text if item.find("./BOM_COMPONENT1") is not None else '')
            record.append(item.find("./BOM_COMPONENT_REV1").text if item.find("./BOM_COMPONENT_REV1") is not None else '')
            record.append(item.find("./CHOICE_NAME").text if item.find("./CHOICE_NAME") is not None else '')
            record.append(float(item.find("./BOM_QTY_PER1").text) if item.find("./BOM_QTY_PER1") is not None else '')
            record.append(item.find("./BOM_UOM1").text if item.find("./BOM_UOM1") is not None else '')

            res_list.append(record)

    elif args.get('method', False) == "EMPLOYEES": ###Employees
        response = client.service.GET_ODOO_EMPLOYEES(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_EMPLOYEES"):
            record = []
            record.append(item.find("./EMP_BUS_UNIT").text)
            record.append(item.find("./UEMP_UNIT").text)
            record.append(item.find("./EMP_EMPLOYEE").text)
            record.append(item.find("./EMP_PASSWORD").text)
            record.append(item.find("./EMP_NAME1").text)
            record.append(item.find("./EMP_SV").text)
            record.append(item.find("./UEMP_DEFAULT").text)
            res_list.append(record)

    elif args.get('method', False) == "UNIT_PREFIX": ###Unit
        response = client.service.GET_ODOO_UNIT_PREFIX(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//ODOO_UNIT_PREFIX"):
            record = []
            record.append(item.find("./UNITPFX_BUS_UNIT").text)
            record.append(item.find("./UNITPFX_UNIT").text)
            record.append(item.find("./UNITPFX_PFX").text)
            record.append(item.find("./UNITPFX_TYPE").text)
            record.append(item.find("./PMPF_DESC").text)
            record.append(item.find("./PMPF_NEXTNUMBER").text)
            res_list.append(record)

    elif args.get('method', False) == "ITEM_VAT": ###Unit
        response = client.service.GET_ITEM_TAX_SETUP(bu, unit)
        root = ET.fromstring(response)
        for item in root.findall(".//TAX"):
            record = []
            record.append(item.find("./ITS_ITEM").text)
            record.append(item.find("./ITS_TCS_CODE").text)
            record.append(item.find("./ITS_TAX_CODE").text)
            record.append(item.find("./ITS_TCS_RATE").text)
            res_list.append(record)

    return res_list

def convert_to_png(name):
    reload(sys)
    sys.setdefaultencoding('utf-8')

    text = unicode(name)

    reshaped_text = arabic_reshaper.reshape(text)
    bidi_text = get_display(reshaped_text)
    # create new image
    imgx = 350 # width in pixels
    imgy = 34  # image height in pixels
    image = Image.new("RGB", (imgx, imgy), (255, 255, 255))
    d = ImageDraw.Draw(image)

    font_path = "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf"
    #font_path = "arial.ttf"
    font = ImageFont.truetype(font_path, 15)
    # print font
    d.text((10, 10), bidi_text.decode('utf-8'), (0, 0, 0), font=font)
    img_str = StringIO.StringIO()
    image.save(img_str, "PNG")
    encoded_img_str = base64.b64encode(img_str.getvalue())
    return encoded_img_str

class PosCategory(models.Model):

    _inherit = 'pos.category'

    active = fields.Boolean('Active?',default=True)

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    emis_id = fields.Char(string='Emis Id')

class RestaurantPrinter(models.Model):
    _inherit = 'restaurant.printer'

    emis_id = fields.Char("EMIS ID")


class ImportMasterData(models.Model):
    """
    import master data wizard, it imports the master data from MF Odoo server to local.
    """
    _name = 'import.master.data'
    
    @api.multi
    def import_data_from_mf_server(self):
        company = self.env['res.company']._company_default_get('res.partner')
        business_unit = company.business_unit
        unit = company.unit_id
        args = {"business_unit": business_unit, "unit": unit}

        kds_dict = {}
        #########################
        #unit, prefix,

#         args['method'] = "UNIT_PREFIX"
#         unit_rows = call_emis_web_api(args)
#         print 'unit rows -----', unit_rows
#         for unit_row in unit_rows:
#             if unit_row[3] == 'U_SO':
#                 company.sales_prefix = unit_row[2]
#             elif unit_row[3] == 'U_RW':
#                 company.return_prefix = unit_row[2]
#             elif unit_row[3] == 'U_SF':
#                 company.shift_prefix = unit_row[2]
# 
#         ############
#         # POS Kitchen Section
#         pos_printer_obj = self.env['restaurant.printer']
#         # all_printer_ids = pos_printer_obj.search([])
#         # all_printer_ids.write({'active': False})
#         args['method'] = "KDS_UNITS"
#         printer_rows = call_emis_web_api(args)
#         for row in printer_rows:
#             printer_exist = pos_printer_obj.search([('emis_id', '=', row[0])])
#             if printer_exist:
#                 pass
#                 #printer_exist.name = row[2]
#             else:  ###### Update inactive record into active####
#                 new_printer_id = pos_printer_obj.create({'name': row[1], 'emis_id': row[0]})

        #########################
        #pos.category
#         pos_categ_obj = self.env['pos.category']
#         args['method'] = "POS_CATEGORIES"
#         all_categ_ids = pos_categ_obj.search([])
#         all_categ_ids.write({'active': False})
#         categ_rows = call_emis_web_api(args)
#         for row in categ_rows:
#             categ_exist = pos_categ_obj.search([('emis_id', '=', row[1])])
#             if categ_exist:
#                 categ_exist.name = row[2]
#             else:  ###### Update inactive record into active####
#                 lcl_categ_obj_ia = pos_categ_obj.search(
#                     [('emis_id', '=', row[1]), ('active', '=', False)])
#                 if lcl_categ_obj_ia:
#                     lcl_categ_obj_ia.active = True
#                     lcl_categ_obj_ia.name = row[2]
#                 else:
#                     new_categ_id = pos_categ_obj.create({'name': row[2], 'emis_id': row[1]})
        ############

        ##########################
        # selling product
        #to-do change it to product.product for brew and sbarro after testing
        product_obj = self.env['product.template']
        args['method'] = "SELLING_ITEMS"
        all_product_ids = product_obj.search([('available_in_pos', '=', True)])
        all_product_ids.write({'available_in_pos':False })

        item_rows = call_emis_web_api(args)
        ###
        product_template = self.env['product.template']
        pos_categ_obj = self.env['pos.category']
        printer_obj = self.env['restaurant.printer']
        for row in item_rows:
            item_exist = product_template.search([('default_code', '=', row[1])])
            categ = pos_categ_obj.search([('emis_id', '=', row[14])])
#             printer_brw = printer_obj.search([('emis_id', '=', row[13])])

            categ_id = 1.
            if categ:
                categ_id = categ.id

#             printer_id = False
#             if printer_brw:
#                 printer_id = printer_brw.id
# 
#             if not kds_dict.get(printer_id, False):
#                 kds_dict[printer_id] = [categ_id]
#             else:
#                 kds_dict[printer_id].append(categ_id)

            if item_exist:
                item_exist.name = row[2].title()
                item_exist.available_in_pos = True
                item_exist.ar_display_name = row[3]
                item_exist.list_price = row[10]
                item_exist.sale_ok = True
                #item_exist.pos_categ_id = categ_id
                if row[3] and not item_exist.ar_display_image:
                    item_exist.ar_display_image = convert_to_png(row[3])
                print "existing_si", item_exist.name
            else:
                new_item_id = product_template.create(
                    {'default_code': row[1], 'name': row[2], 'available_in_pos': True,
                     'ar_display_name': row[3], 'pos_categ_id': categ_id, 'list_price': row[10], 'sale_ok': True ,
                     'ar_display_image':convert_to_png(row[3])
                     })

#         for key, value in kds_dict.iteritems():
#             brw_printer = printer_obj.browse(key)
#             brw_printer.write({'product_categories_ids':[(6,0,value)]})

        ############

        ##########################
        # product recipe
        all_product_ids = product_obj.search([('available_in_pos', '=', False)])
        all_product_ids.write({'active': False })

        args['method'] = 'RECIPES'
        item_rows = call_emis_web_api(args)
        product_template = self.env['product.template']
        for row in item_rows:
            item_exist = product_template.search([('emis_id', '=', row[0]),('active', '=', False)])
            if item_exist:
                item_exist.name = row[1]
                item_exist.default_code = row[0]
                item_exist.emis_id = row[0]
                item_exist.available_in_pos = False
                item_exist.sale_ok = False
                item_exist.active = True
            else:
                new_item_id = product_template.create(
                    {'default_code': row[0], 'name': row[1], 'available_in_pos': False,
                     'sale_ok': False,'emis_id':row[0]})

        #########################
        ##bom detail
        all_bom = self.env['product.bom.detail'].search([])
        all_bom.unlink()
        args['method'] = "BOM_WITH_CHOICE"
        bom_rows = call_emis_web_api(args)
        bom_detail_obj = self.env['product.bom.detail']
        bom_choice_obj = self.env['product.bom.choice']
        product_obj = self.env['product.template']
        parent_id = False
        product_emis_id = False
        bom_detail_id = False
        for row in bom_rows:
            if row[1] == 0:
                parent_id = product_obj.search([('default_code', '=', row[2])])
                product_emis_id = False
                bom_detail_id = False
                continue
            else:
                if product_emis_id != row[5]:
                    product_emis_id = row[5]
                    product_id = product_obj.search([('default_code', '=', row[5])])
                    bom_detail_id = bom_detail_obj.create(
                        {'parent_id': parent_id.id, 'product_id': product_id.id, 'qty': row[8],
                         'uom': row[9], 'has_choice': True if row[10] else False, 'unit': unit})
                if row[10]:
                    choice_id = product_obj.search([('default_code', '=', row[10])])
                    bom_choice_obj.create(
                        {'parent_id': bom_detail_id.id, 'product_id': choice_id.id, 'uom': row[14], 'qty': row[13]})

        ############

        ##########################
        # TAX VAT SETUP
        args['method'] = "ITEM_VAT"
        item_rows = call_emis_web_api(args)
        ###
        product_template = self.env['product.template']
        account_tax = self.env['account.tax']
        tax_id = {}
        type_tax_use = ""
        print 'TIMENOW', datetime.now(timezone('Asia/Riyadh')).strftime("%Y-%m-%d")
        if (datetime.now(timezone('Asia/Riyadh')).strftime("%Y-%m-%d") >= '2018-01-01'):
            type_tax_use = 'sale'
        else:
            type_tax_use = 'none'
        print 'type tax use ',type_tax_use
        for row in item_rows:
            item_exist = product_template.search([('default_code', '=', row[0])])
            if not tax_id.get(row[1],False):
                res_tax_ids = account_tax.search([('name', '=',row[1])])
                if res_tax_ids:
                    write_tax_id = res_tax_ids.write({'type_tax_use':type_tax_use})
                    print 'Tax Edited',write_tax_id
                    if not tax_id.get(row[1],False):
                        tax_id[row[1]] = [res_tax_ids[0].id]
                    else:
                        tax_id[row[1]] = tax_id[row[1]].append(res_tax_ids[0].id)
                else:
                    new_tax_id = account_tax.create({'name':row[1],'amount':float(row[3]),
                                        'amount_type':'percent','type_tax_use':type_tax_use, 
                                        'price_include': True, 'include_base_amount': True})
                    print 'Tax Created',new_tax_id
                    tax_id[row[1]] = [new_tax_id.id]

            if item_exist and tax_id.get(row[1],False):
                item_exist[0].taxes_id = [(6,0,tax_id[row[1]])]
            else:
                print "issue with tax set up of " , row[0] , row[1]

        ############

        ##########################
        # Payment Methods
#         args['method'] = "PAYMENT_TYPES"
#         pay_methods = call_emis_web_api(args)
#         pay_method_obj = self.env['account.journal']
#         for row in pay_methods:
#             cust_exist = pay_method_obj.search([('emis_id', '=', row[2])])
#             if cust_exist:
#                 pass
#             if not cust_exist:
#                 vals = {}
#                 vals['name'] = row[3]
#                 vals['type'] = 'cash' if row[1] != 'CRCARD' else 'bank'
#                 vals['emis_id'] = row[2]
#                 vals['card_type'] = False if row[1] != 'CRCARD' else True
#                 vals['journal_user'] = True
#                 new_payment_method_id = pay_method_obj.create(vals)
#                 print "created payment method", new_payment_method_id

        ############
