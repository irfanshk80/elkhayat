{
    'name': "EMIS Data Import - API Integration",
    'version': "1.0",
    'sequence':1,
    'author': "Fore-Vision - Almusbah Groups",
    'category': "Tools",
    'summary':'Integration between Odoo POS and EMIS System through APIs',
    'license':'LGPL-3',
    'data': [
        'views/product_view.xml',
        'wizard/import_data_view.xml'
    ],
    'demo': [],
    'depends': ['point_of_sale','pos_restaurant','pos_connect_ecw','pos_brew92'],
    'installable': True,
}