from openerp import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)


class ImportData(models.TransientModel):
    """
    import master data wizard, it imports the master data from MF Odoo Cloud server to local.
    """
    _name = 'import.data'

    @api.multi
    def import_data(self):
        self.env['import.master.data'].sudo().import_data_from_mf_server()
        return {'type': 'ir.actions.act_window_close'}
    
