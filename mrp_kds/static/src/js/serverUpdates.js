var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
//var path = require('path');
 
 
 
// Initialize appication with route / (that means root of the application)
//app.get('/', function(req, res){
//  var express=require('express');
//  app.use(express.static(path.join(__dirname)));
//  //res.sendFile(path.join(__dirname, '../odoo_node_works', 'index.html'));
//  res.sendFile(path.join(__dirname, 'index.html'));
//});
 
// Register events on socket connection
io.on('connection', function(socket){ 
  socket.on('orderCompleted', function(oId,tId,products){
     io.emit('orderCompleted', oId,tId,products);
  });
//  Code to place emit on order submit
  socket.on('orderPlaced', function(){
     io.emit('orderPlaced');
  });
  
});
 
// Listen application request on port 3000
http.listen(3000, function(){
  console.log('listening on *:3000');
});

