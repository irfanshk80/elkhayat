var configUrl = 'http://192.168.1.121:3000';

var socket = io(configUrl);

odoo.define('mrp_kds.kitchen', function (require) {
"use strict";

var ajax = require('web.ajax');
var Model = require('web.Model');

//$(document).ready(function(){

  $('#order_id').change(function(){
      $('#oid').val($(this).val());
      $(this).val('ordered');
  });

  $(document).on('click','.order-update-notify',function(){
      var oId = $(this).attr('data-order-reference');
      var tId = $(this).attr('data-table-reference');
      var id = $(this).data('id') || false;
      var prod_ids = $(this).data('prod-ids') || [];
      var prod_list = prod_ids.split(',');
      // (filter - JS 1.6 and above)
       prod_list = prod_list.filter(function(n){ return n != "" });
      var mo_list = prod_list.map(function(item) {
                 return parseInt(item);
                });
//      socket.emit('orderCompleted', oId,tId);
      new Model("procurement.group")
                    .call("preparation_done_json", [id,mo_list])
                    .then(function (result) {
                        // socket.emit('orderCompleted', oId,tId,result);

//                        $('.oe_searchview_input').attr('contenteditable','false');
                        $('.oe-cp-switch-kanban').trigger('click');
//                      setTimeout(function(){  $('.oe_searchview_input').attr('contenteditable','true');  },400);

                    });
  });





  socket.on('orderPlaced', function(){
      $.urlParam = function(name){
        var results = new RegExp('[#=\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results===null){
        return null;
        }
        else{
        return results[1] || 0;
        }
        };
        var chkKitchen = $.urlParam('model');
        var view_type = $.urlParam('view_type');
        if(view_type ==='kanban' && (chkKitchen ==='procurement.group' || chkKitchen ==='stock.picking')) {
        //  $('.oe_searchview_input').attr('contenteditable','false');
          $('.oe-cp-switch-list').hide();
          $('.oe-cp-switch-kanban').trigger('click');
        }
     });

});
//});

