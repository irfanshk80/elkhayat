# -*- coding: utf-8 -*-
{
    'name': "Kitchen Display System through mrp",
    'summary': "Kitchen Display System for restaurant POS orders based on work order.",
    'version': '1.1.0',
    'author': 'Fore-Vision Business Solutions',
    'license': 'AGPL-3',
    'category': 'Point Of Sale',
    'website': 'https://www.fore-vision.com',
    'depends': [
                'point_of_sale','pos_restaurant',
                'web_one2many_kanban',
                'mrp'],
    'images': [],
    'data': [
        'security/kds_security.xml',
        'security/ir_rule.xml',
        'security/ir.model.access.csv',
        'views/kds_view.xml',
        'views/procurement_view.xml',
        'views/mrp_view.xml',
        'views/pos_view.xml',
        'views/templates.xml',
        'views/mrp_workflow.xml',
        'views/stock_view.xml',

        'wizard/mrp_order_produce_view.xml',
        'views/schedular.xml'
        
         
    ],
    'qweb':[
#        'static/src/xml/multiprint.xml',
    ],
    'installable': True,
}
