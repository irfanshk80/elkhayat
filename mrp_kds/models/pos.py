# -*- coding: utf-8 -*-

from openerp import models,fields, api
from openerp.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime, timedelta
from openerp.osv import fields as field, osv

class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    procurement_ids = fields.One2many('procurement.order', 'pos_line_id', string='Procurements')  
    note = fields.Text('Note', default='/')
#    production_state =  fields.Selection(compute='_compute_related_production_orders', search=_search_in_progress_orders,  copy=False,
    production_state =  fields.Selection(copy=False,
            selection=[
                ('draft', 'Draft'),
                ('cancel', 'Cancelled'),
                ('progress', 'In Progress'),
                ('done', 'Done'),
                ], string='Status', readonly=True, select=True, track_visibility='onchange', default='draft')
    @api.multi
    def _prepare_order_line_procurement(self, group_id=False):
        self.ensure_one()
        return {
#            'name': self.product_id.name,
            'name': self.note,
            'origin': self.order_id.name,
            'date_planned': datetime.strptime(self.order_id.date_order, DEFAULT_SERVER_DATETIME_FORMAT)+ timedelta(minutes=self.product_id.produce_delay+self.order_id.company_id.manufacturing_lead),
            'product_id': self.product_id.id,
            'product_qty': self.qty,
            'product_uom': self.product_id.uom_id.id,
            'company_id': self.order_id.company_id.id,
            'location_id': self.order_id.session_id.config_id.picking_type_id.default_location_dest_id.id,
            'warehouse_id': self.order_id.session_id.config_id.picking_type_id.warehouse_id.id,
            'group_id': group_id,
            'pos_line_id': self.id,
            'route_ids': self.product_id.route_ids and [(4, route.id) for route in self.product_id.route_ids] or [],
        }
        
    @api.multi
    def _action_procurement_create(self):
        """
        Create procurements based on quantity ordered. If the quantity is increased, new
        procurements are created. If the quantity is decreased, earlier procurement is cancelled and new procurement is created.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        new_procs = self.env['procurement.order'] #Empty recordset
        for line in self:
            if not line.product_id._need_procurement():
                continue
            qty = 0.0
            for proc in line.procurement_ids.filtered(
                lambda r: r.state in ('confirmed','running','done') and not r.production_id):
                qty += proc.product_qty
            if float_compare(qty, line.qty, precision_digits=precision) > 0:
                for proc in line.procurement_ids.filtered(
                lambda r: r.state in ('confirmed','running') and not r.production_id):
                    if qty > line.qty:
                        print"qty to compare....",qty,line.qty
                        proc.cancel()
                        proc.move_dest_id.procurement_id.cancel()
                        qty-=proc.product_qty
                
            if float_compare(qty, line.qty, precision_digits=precision) >= 0.0:
                continue

            if not line.order_id.procurement_group_id:
                vals = line.order_id._prepare_procurement_group()
                line.order_id.procurement_group_id = self.env["procurement.group"].create(vals)

            vals = line._prepare_order_line_procurement(group_id=line.order_id.procurement_group_id.id)
            vals['product_qty'] = line.qty - qty
            print 'creating procurement'
            new_proc = self.env["procurement.order"].create(vals)
            new_procs += new_proc
        new_procs.run()
        return new_procs
    
    @api.model
    def create(self, values):
        onchange_fields = ['price_unit']

        ctx = dict(self.env.context, kitchen=True)
        if values.get('order_id') and values.get('product_id') and any(f not in values for f in onchange_fields):
            line = self.new(values)
#            line.onchange_product_id()
            for field in onchange_fields:
                if field not in values:
                    values[field] = line._fields[field].convert_to_write(line[field])
        line = super(PosOrderLine, self).create(values)
        if line:

            line.with_context(ctx)._action_procurement_create()

        return line

    @api.multi
    def write(self, values):
        lines = False
        ctx = dict(self.env.context, kitchen=True)
        if 'qty' in values:
            precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            lines = self.filtered(
                lambda r: float_compare(r.qty, values['qty'], precision_digits=precision) != 0)
        result = super(PosOrderLine, self).write(values)
        if lines:
            lines.with_context(ctx)._action_procurement_create()
        return result

class pos_order(osv.osv):
    _inherit='pos.order'
    _columns={
    'procurement_group_id' : field.many2one('procurement.group', 'Procurement Group', copy=False, default=False),
    }
    

    def close_orders(self, cr, uid, context=None):
            
        MRP = self.pool['mrp.production']
        mrp_orders = MRP.search(cr, uid, [])
        for mrp_order in MRP.browse(cr, uid, mrp_orders):
            mrp_order.signal_workflow('button_confirm')
            mrp_order.force_production()
            mrp_order.signal_workflow('button_produce')
            if mrp_order.state == 'in_production':
                mrp_order.action_produce(mrp_order.id,mrp_order.product_qty,'consume_produce',False)
                mrp_order.signal_workflow('button_produce_done')
            elif mrp_order.state == 'recalled':
                mrp_order.signal_workflow('button_recalled_done')
#                To force the done from recalled state if not changed by workflow
                mrp_order.write({'state':'done'})
                
            elif mrp_order.state == 'cancel':
                mrp_order.signal_workflow('button_cancelled')
#                To force the cancelled state if not changed by workflow
                mrp_order.write({'state':'cancelled'})
        SP = self.pool['stock.picking']
        stock_pickings = SP.search(cr, uid, [('state','in',('assigned', 'partially_available','draft','confirmed','waiting'))])
        print"stock pickings...",stock_pickings
        for picking in SP.browse(cr, uid, stock_pickings):
            print picking.id
#            if picking.state == 'draft':
#                picking.action_confirm()
#    #            if self.state =='waiting':
#    #               self.force_assign() 
#                if picking.state != 'assigned':
#                    picking.action_assign()
#                    if picking.state != 'assigned':
#                        raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
##                else:
##                    picking.do_new_transfer()
#            if picking.state =='waiting':
#                picking.force_assign()
#Changes in done 
            if picking.state == 'draft':
                picking.action_confirm()
     
            if picking.state in  ('confirmed','waiting'):
                picking.action_assign()
                
            if picking.state in  ('confirmed','waiting','partially_available'):
                picking.force_assign()
                
#            if picking.state in  ('draft','partially_available','assigned'):
#                picking.do_new_transfer()
#End
            for pack in picking.pack_operation_ids:
    #            if pack.product_qty > 0:
                if pack.product_qty != 0:
                    pack.write({'qty_done': pack.product_qty})
                else:
                    pack.unlink()
            picking.do_transfer()
        return
    
    def _prepare_procurement_group(self, cr, uid, ids, context=None):
        order = self.browse(cr, uid, ids[0],context)
        return {'name': order.name,'group_reference':order.pos_reference,'table_id':order.table_id.id if order.table_id else False}
    
    def action_paid(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'paid'}, context=context)
#        self.create_picking(cr, uid, ids, context=context)
        return True
    

