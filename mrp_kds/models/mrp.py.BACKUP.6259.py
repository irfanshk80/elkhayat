from openerp import api, fields, models, _
from openerp.osv import osv
import time
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.osv import fields as Fields
import openerp.addons.decimal_precision as dp

def strToDatetime(strdate):
    return datetime.strptime(strdate, DEFAULT_SERVER_DATETIME_FORMAT)

class mrp_production(models.Model):
    """
    Production Orders / Manufacturing Orders
    """
    _inherit = 'mrp.production'
    note =  fields.Text('Note')
    pos_line_id = fields.Many2one('pos.order.line',  string='POS Line')
    sale_line_id = fields.Many2one('sale.order.line',  string='Sale Line')
    _columns = {
        'product_qty': Fields.float('Product Quantity', digits_compute=dp.get_precision('Product Unit of Measure'), required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'state': Fields.selection(
            [('draft', 'New'), ('cancel', 'Request to Cancel'), ('confirmed', 'Awaiting Raw Materials'),
                ('ready', 'Ready to Produce'), ('in_production', 'Production Started'), ('done', 'Done'),('recalled', 'Recalled'), ('cancelled', 'Cancelled')],
            string='Status', readonly=True,
            track_visibility='onchange', copy=False,
            help="When the production order is created the status is set to 'Draft'.\n"
                "If the order is confirmed the status is set to 'Waiting Goods.\n"
                "If any exceptions are there, the status is set to 'Picking Exception.\n"
                "If the stock is available then the status is set to 'Ready to Produce.\n"
                "When the production gets started then the status is set to 'In Production.\n"
                "When the production is over, the status is set to 'Done'."),
    }
    
<<<<<<< HEAD
=======
    def create(self, cr, uid, values, context=None):
        if context is None:
            context = {}
        print"Context ... ",context
        mrp_order = super(mrp_production, self).create(cr, uid, values, context=context)
        if context.get('kitchen',False):
            order = self.browse(cr, uid, mrp_order)
            if order.state =='draft':
                order.signal_workflow('button_confirm')

    #                    if order.state =='confirmed':
    #                        order.action_assign()
    #                        order.force_production()

            if order.state =='confirmed':
                order.force_production()
                order.signal_workflow('moves_ready')

            if order.state =='ready':
                order.signal_workflow('button_produce')
        return mrp_order
    
>>>>>>> 3e6089d209af6db447b11abfe41d9f2e4f501ab9
    def _check_qty(self, cr, uid, ids, context=None):
#        for order in self.browse(cr, uid, ids, context=context):
#            if order.product_qty <= 0:
#                return False
        return True

    _constraints = [
        (_check_qty, 'Order quantity cannot be negative or zero!', ['product_qty']),
    ]
    
    def action_in_production(self, cr, uid, ids, context=None):
        """ Changes state to In Production and writes starting date.
        @return: True
        """
        POSL = self.pool.get('pos.order.line')
        SOL = self.pool.get('sale.order.line')
        for prod_order in self.browse(cr, uid, ids):
            if prod_order.state !='done':
                self.write(cr, uid, prod_order.id, {'state': 'in_production', 'date_start': time.strftime('%Y-%m-%d %H:%M:%S')})
            else:
                self.write(cr, uid, prod_order.id, {'state': 'in_production' })
            if prod_order.pos_line_id:
                POSL.write(cr , uid, prod_order.pos_line_id.id,{'production_state':'progress'})
            if prod_order.sale_line_id:
                SOL.write(cr , uid, prod_order.sale_line_id.id,{'production_state':'progress'})
        return True
    
    def _hour_production_calc(self, cr, uid, ids, prop, unknow_none, context=None):
        """ Calculates total hours and total no. of cycles for a production order.
        @param prop: Name of field.
        @param unknow_none:
        @return: Dictionary of values.
        """
        result = {}
        for prod in self.browse(cr, uid, ids, context=context):
            result[prod.id] = {
                'hour_total': 0.0,
                'cycle_total': 0.0,
            }
            for wc in prod.workcenter_lines:
                result[prod.id]['hour_total'] += wc.hour
                result[prod.id]['cycle_total'] += wc.cycle
        return result
    
    hour_production = fields.Float(string='Total Hours')
    procurement_group_id = fields.Many2one('procurement.group', 'Procurement Group', copy=False)
    
    def action_production_end(self, cr, uid, ids, context=None):
        """ Changes production state to Finish and writes finished date.
        @return: True
        """
        self._compute_costs_from_production(cr, uid, ids, context)
        hour_production = strToDatetime(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) - strToDatetime(self.browse(cr, uid, ids[0]).date_start)
        write_res = self.write(cr, uid, ids, {'state': 'done', 'date_finished': time.strftime('%Y-%m-%d %H:%M:%S'),'hour_production':hour_production.total_seconds()/3600})
        # Check related procurements
        proc_obj = self.pool.get("procurement.order")
        procs = proc_obj.search(cr, uid, [('production_id', 'in', ids)], context=context)
        proc_obj.check(cr, uid, procs, context=context)
#        res = super(mrp_production, self).action_production_end(cr, uid, ids, context=context)
        POSL = self.pool.get('pos.order.line')
        SOL = self.pool.get('sale.order.line')
        for prod_order in self.browse(cr, uid, ids):
            if prod_order.pos_line_id:
                POSL.write(cr , uid, prod_order.pos_line_id.id,{'production_state':'done'})
            if prod_order.sale_line_id:
                SOL.write(cr , uid, prod_order.sale_line_id.id,{'production_state':'done'})
        return write_res

    def action_production_recall(self, cr, uid, ids, context=None):
        """ Changes production state to Recalled and writes finished date.
        @return: True
        """
        print"function recalled call in mrp for ",ids
        write_res = self.write(cr, uid, ids, {'state': 'recalled'})
        # Check related procurements
#        proc_obj = self.pool.get("procurement.order")
#        procs = proc_obj.search(cr, uid, [('production_id', 'in', ids)], context=context)
#        proc_obj.check(cr, uid, procs, context=context)
        return write_res
    
    def action_production_cancelled(self, cr, uid, ids, context=None):
        """ Changes production state to Cancelled and writes finished date.
        @return: True
        """
        print"Cancel request accepted"
        write_res = self.write(cr, uid, ids, {'state': 'cancelled'})
        # Check related procurements
#        proc_obj = self.pool.get("procurement.order")
#        procs = proc_obj.search(cr, uid, [('production_id', 'in', ids)], context=context)
#        proc_obj.check(cr, uid, procs, context=context)
        return write_res
    
    def button_recalled_done(self, cr, uid, ids, context=None):
        """ Changes production state to done again and writes finished date.
        @return: True
        """
        write_res = self.write(cr, uid, ids, {'state': 'done', 'date_finished': time.strftime('%Y-%m-%d %H:%M:%S'),'hour_production':hour_production.total_seconds()/3600})
        return write_res

class mrp_production_workcenter_line(models.Model):
    _inherit = 'mrp.production.workcenter.line'
    
    procurement_group_id = fields.Many2one('procurement.group', 'Procurement Group', copy=False)
    
class mrp_routing(osv.osv):
    """
    For specifying the users associated in routings of Work Centers.
    """
    _inherit = 'mrp.routing'
    _columns = {
        'user_ids': Fields.many2many('res.users','routing_user_rel', 'routing_id','user_id',string='Users for this routing.'),
        }
        