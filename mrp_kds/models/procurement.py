from openerp import api, fields, models, _
from openerp.osv import osv
import time

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.osv import fields as Fields
import openerp.addons.decimal_precision as dp
from itertools import chain

def strToDatetime(strdate):
    return datetime.strptime(strdate, DEFAULT_SERVER_DATETIME_FORMAT)



class procurement_order(osv.osv):
    _inherit = 'procurement.order'
    
    _columns = {
       'pos_line_id':Fields.many2one('pos.order.line', 'Pos Order Line'),
       'note': Fields.text('Note')

    }
    
    def _run_move_create(self, cr, uid, procurement, context=None):
        ''' Returns a dictionary of values that will be used to create a stock move from a procurement.
        This function assumes that the given procurement has a rule (action == 'move') set on it.

        :param procurement: browse record
        :rtype: dictionary
        '''
        newdate = (datetime.strptime(procurement.date_planned, '%Y-%m-%d %H:%M:%S') - relativedelta(days=procurement.rule_id.delay or 0)).strftime('%Y-%m-%d %H:%M:%S')
        group_id = False
        if procurement.rule_id.group_propagation_option == 'propagate':
            group_id = procurement.group_id and procurement.group_id.id or False
        elif procurement.rule_id.group_propagation_option == 'fixed':
            group_id = procurement.rule_id.group_id and procurement.rule_id.group_id.id or False
        #it is possible that we've already got some move done, so check for the done qty and create
        #a new move with the correct qty
        already_done_qty = 0
        for move in procurement.move_ids:
            already_done_qty += move.product_uom_qty if move.state == 'done' else 0
#        qty_left = max(procurement.product_qty - already_done_qty, 0)
        qty_left = procurement.product_qty - already_done_qty
        vals = {
            'name': procurement.name,
            'company_id': procurement.rule_id.company_id.id or procurement.rule_id.location_src_id.company_id.id or procurement.rule_id.location_id.company_id.id or procurement.company_id.id,
            'product_id': procurement.product_id.id,
            'product_uom': procurement.product_uom.id,
            'product_uom_qty': qty_left,
            'partner_id': procurement.rule_id.partner_address_id.id or (procurement.group_id and procurement.group_id.partner_id.id) or False,
            'location_id': procurement.rule_id.location_src_id.id,
            'location_dest_id': procurement.location_id.id,
            'move_dest_id': procurement.move_dest_id and procurement.move_dest_id.id or False,
            'procurement_id': procurement.id,
            'rule_id': procurement.rule_id.id,
            'procure_method': procurement.rule_id.procure_method,
            'origin': procurement.origin,
            'picking_type_id': procurement.rule_id.picking_type_id.id,
            'group_id': group_id,
            'route_ids': [(4, x.id) for x in procurement.route_ids],
            'warehouse_id': procurement.rule_id.propagate_warehouse_id.id or procurement.rule_id.warehouse_id.id,
            'date': newdate,
            'date_expected': newdate,
            'propagate': procurement.rule_id.propagate,
            'priority': procurement.priority,
        }
        return vals

    def _prepare_mo_vals(self, cr, uid, procurement, context=None):
        res_id = procurement.move_dest_id and procurement.move_dest_id.id or False
        newdate = self._get_date_planned(cr, uid, procurement, context=context)
        bom_obj = self.pool.get('mrp.bom')
        if procurement.bom_id:
            bom_id = procurement.bom_id.id
            routing_id = procurement.bom_id.routing_id.id
        else:
            properties = [x.id for x in procurement.property_ids]
            bom_id = bom_obj._bom_find(cr, uid, product_id=procurement.product_id.id,
                                       properties=properties, context=dict(context, company_id=procurement.company_id.id))
            bom = bom_obj.browse(cr, uid, bom_id, context=context)
            routing_id = bom.routing_id.id
        return {
            'note':procurement.name,
            'origin': procurement.origin,
            'product_id': procurement.product_id.id,
            'product_qty': procurement.product_qty,
            'product_uom': procurement.product_uom.id,
            'location_src_id': procurement.rule_id.location_src_id.id or procurement.location_id.id,
            'location_dest_id': procurement.location_id.id,
            'bom_id': bom_id,
            'routing_id': routing_id,
            'date_planned': newdate.strftime('%Y-%m-%d %H:%M:%S'),
            'move_prod_id': res_id,
            'company_id': procurement.company_id.id,
            'procurement_group_id':procurement.group_id.id,
            'pos_line_id':procurement.pos_line_id.id if procurement.pos_line_id else False,
            'sale_line_id':procurement.sale_line_id.id if procurement.sale_line_id else False
        }
        
    def _get_date_planned(self, cr, uid, procurement, context=None):
        format_date_planned = datetime.strptime(procurement.date_planned,
                                                DEFAULT_SERVER_DATETIME_FORMAT)
#        date_planned = format_date_planned - relativedelta(minutes=procurement.product_id.produce_delay or 0.0)
        date_planned = format_date_planned - relativedelta(minutes=procurement.company_id.manufacturing_lead)
        return date_planned

class procurement_group(models.Model):
    _inherit = 'procurement.group'
    
#    def fields_view_get(self, cr, user, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
#        ModelData = self.pool['ir.model.data']
#        if context is None:
#            context = {}
#
#        if ('product' in context) and (context['product']=='membership_product'):
#            if view_type == 'form':
#                view_id = ModelData.xmlid_to_res_id(
#                    cr, user, 'membership.membership_products_form', context=context)
#            else:
#                view_id = ModelData.xmlid_to_res_id(
#                    cr, user, 'membership.membership_products_tree', context=context)
#        return super(Product,self).fields_view_get(cr, user, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)

    
    @api.one

#    @api.depends('production_ids', 'production_ids.state', 'production_ids.date_planned')
    def _compute_related_production_orders(self):
        routing = self.env.context.get('routing_id',[])
        
##        self._cr.execute("""select
#                    routing_id
#                from
#                    routing_user_rel
#                where
#                    user_id = %s
                 #""", (self._uid,))
#        routing_list = self._cr.fetchall()
#        print"self....",routing_list
##        list_routing = list(chain.from_iterable(self._cr.fetchall()))
#        routing = [i[0] for i in routing_list]
#        print routing
        rem_min = 0.0
        self.production_state = 'draft'
        if routing:
            if self.max_time:
                time_rem = strToDatetime(self.max_time) - datetime.now()
                rem_min = time_rem.total_seconds()/60
#            new_orders = self.env['mrp.production'].search([('routing_id','=',routing),('procurement_group_id','=',self.id),('state','in', ['draft','confirmed','ready']),('product_id.produce_delay','>=',rem_min)])

#            new_orders = self.env['mrp.production'].search([('routing_id','in',routing),('procurement_group_id','=',self.id),('state','in', ['draft','confirmed','ready'])])
#            if new_orders:
#                for order in new_orders:
#                     
#                    if order.state =='draft':
#                        order.signal_workflow('button_confirm')
#                    
##                    if order.state =='confirmed':
##                        order.action_assign()
##                        order.force_production()
#                        
#                    if order.state =='confirmed':
#                        order.force_production()
#                        order.signal_workflow('moves_ready')
#                        
#                    if order.state =='ready':
#                        order.signal_workflow('button_produce')
#            self.related_production_ids = self.production_ids.search([('routing_id','=',routing),('procurement_group_id','=',self.id),('product_id.produce_delay','>=',rem_min)])
            group_mrp_orders = self.production_ids.search([('routing_id','in',routing),('procurement_group_id','=',self.id)])
#            if any([x.state == 'in_production' for x in group_mrp_orders]):
#                self.production_state = 'progress'
#            elif all([x.state == 'recalled' for x in group_mrp_orders]):
            if all([x.state == 'recalled' for x in group_mrp_orders]):
                self.production_state = 'recalled'
            elif all([x.state in ('done','cancelled') for x in group_mrp_orders]):
                self.production_state = 'done'
            elif all([x.state in ('draft','confirmed','ready') for x in group_mrp_orders]):
                self.production_state = 'draft'
            else:
                self.production_state = 'progress'
            self.related_production_ids = group_mrp_orders
            
#            if new_orders:
#                new_orders.force_production()
#                new_orders.signal_workflow('button_produce')
        else:
#            if any([x.state == 'in_production' for x in self.production_ids]):
#                self.production_state = 'progress'
#            elif all([x.state == 'recalled' for x in self.production_ids]):
            if all([x.state == 'recalled' for x in self.production_ids]):
                self.production_state = 'recalled'
            elif all([x.state in ('done','cancelled') for x in self.production_ids]):
                self.production_state = 'done'
            elif all([x.state in ('draft','confirmed','ready') for x in self.production_ids]):
                self.production_state = 'draft'
            elif all([x.state =='cancel' for x in self.production_ids]):
                self.production_state = 'cancel'
            else:
                self.production_state = 'progress'

            group_mrp_orders = self.production_ids.search([('procurement_group_id','=',self.id)])
            self.related_production_ids = group_mrp_orders
            
    @api.one        
#    @api.depends('procurement_ids', 'procurement_ids.priority', 'procurement_ids.date_planned')
    def get_min_max_time(self):
        """ Finds minimum and maximum time for order.
        @return: Dictionary of values
        """
        routing = self.env.context.get('routing_id',[])
        len_routing = len(routing)
        routing_list = '('
        i= 1
        for item in routing:
            if i < len_routing:
                routing_list+= str(item)+','
            else:
                routing_list+= str(item)
            
        routing_list+=')'
        if routing:
            self._cr.execute("""select
                    min(date_planned),
                    max(date_planned),
                    max(priority)
                from
                    mrp_production
                where
                    procurement_group_id = %s and routing_id IN %s and state in ('draft','confirmed','ready','in_production')
                 """%(self.id,routing_list))
        else:
            self._cr.execute("""select
                    min(date_planned),
                    max(date_planned),
                    max(priority)
                from
                    mrp_production
                where
                    procurement_group_id = %s
                 """, (self.id,))
                
        res = self._cr.fetchone()
        
        self.min_time = res[0]
        self.max_time = res[1]
        self.priority = res[2]
        if res[0]:

#            time_tuple = time.strptime(res[0], "%Y-%m-%d %H:%M:%S")
#            self.timestamp_min =  int(time.mktime(time_tuple))
            create_time_stamp = int(time.mktime(time.strptime(self.create_date, "%Y-%m-%d %H:%M:%S")))
            self.timestamp_min =  create_time_stamp
            
    @api.one
    def _state_get(self):
        ctx = self.env.context
        
        self.production_state = 'draft'
#        for mrp_order in self.related_production_ids:
        
        if any([x.state == 'in_production' for x in self.with_context(ctx).related_production_ids]):
            self.production_state = 'progress'
            return
        if all([x.state == 'done' for x in self.with_context(ctx).related_production_ids]):
            self.production_state = 'done'
            return
        if all([x.state in ('draft','confirmed','ready') for x in self.with_context(ctx).related_production_ids]):
            self.production_state = 'draft'
            return
        
    @api.model
    def _search_in_progress_orders(self, operator, operand):
        res = []

        ctx = self.env.context
        
        if operator not in ('in','=', 'ilike'):
            return []
        if type(operand) not in (str, int, unicode,list):
            return []
 

        orders =  self.search([('create_date','>', (datetime.now() - timedelta(hours=24)).strftime('%Y-%m-%d'))])
        order_ids =[]
        for order in orders:
            if ctx.get('routing_id',False):
                mrp_orders =  self.env['mrp.production'].search([('procurement_group_id','=',order.id),('routing_id','in',ctx.get('routing_id',[]))])
            else:
                mrp_orders =  self.env['mrp.production'].search([('procurement_group_id','=',order.id)])
            print"mrp_orders....",mrp_orders
            production_state = 'draft'
            if any([x.state == 'in_production' for x in mrp_orders]):
                production_state = 'progress'
            if all([x.state in ('done','cancelled') for x in mrp_orders]):
                production_state = 'done'
            if all([x.state in ('draft','confirmed','ready') for x in mrp_orders]):
                production_state = 'draft'
            if production_state in operand:
                order_ids.append(order.id)
        res.append(('id', 'in', order_ids))
        return res       
    
    date_order = fields.Char('order date ', compute='compute_date', store=True)
    date_order2 = fields.Char('order date2 ', default='22:00:55')
    table_id = fields.Many2one('restaurant.table', string='Table')
    routing_id = fields.Many2one('mrp.routing',related='production_ids.routing_id', string='Routing')
    group_reference = fields.Char('Receipt Ref', readonly=True, copy=False)
    production_ids = fields.One2many('mrp.production', 'procurement_group_id', 'Manufacturing Orders',)
    related_production_ids = fields.One2many('mrp.production', 'procurement_group_id', 'Manufacturing Orders',compute='_compute_related_production_orders')
    production_state =  fields.Selection(compute='_compute_related_production_orders', search=_search_in_progress_orders,  copy=False,
            selection=[
                ('draft', 'Draft'),
                ('cancel', 'Cancelled'),
                ('progress', 'Started'),
                ('ready', 'Ready'),
                ('done', 'Done'),
                ('recalled', 'Recalled'),
                ], string='Status', readonly=True, select=True, track_visibility='onchange')
        
    min_time = fields.Datetime('Minimum Time',compute ='get_min_max_time')    
    max_time = fields.Datetime('Maximum Time',compute ='get_min_max_time')  
    timestamp_min = fields.Integer('Time',compute ='get_min_max_time')  
    priority = fields.Selection(string = 'Priority',compute='get_min_max_time',selection=[('0','Not urgent'),('1','Normal'),('2','Urgent'),('3','Very Urgent'),])
    
    @api.one
    def compute_date(self):
        # self.date_order = self.create_date.strftime('%H:%M:%S')
        self.date_order = '22:55:00'
        print self.date_order

    @api.multi
    def preparation_start(self):
        self.related_production_ids.signal_workflow('button_confirm')
        self.related_production_ids.force_production()
        self.related_production_ids.signal_workflow('button_produce')
        
        return True
    
#    @api.multi
#    def preparation_done(self):
#        print"function done for group %s is called....."%(self.id)
#        for production in self.related_production_ids:
#            print"production value....",production.state,production.id
#            if production.state == 'in_production':
#                production.action_produce(production.id,production.product_qty,'consume_produce',False)
#                production.signal_workflow('button_produce_done')
#            elif production.state == 'recalled':
#                production.signal_workflow('button_recalled_done')
#        return True
    
    @api.v7
    def preparation_done_json(self, cr, uid, id, mo_ids=[], context=None):
        list_products = False
        MRP = self.pool.get('mrp.production')
        for production in MRP.browse(cr, uid,mo_ids,context):
            
            if production.state == 'in_production':
                MRP.action_produce(cr, uid, production.id,production.product_qty,'consume_produce',False)
                MRP.signal_workflow(cr, uid, [production.id],'button_produce_done')
                if not list_products:
                    list_products = ''
                list_products = list_products + _('%s %s \n')%(production.product_qty,production.product_id.name)
            elif production.state == 'recalled':
                MRP.signal_workflow(cr, uid, [production.id],'button_recalled_done')
#                To force the done from recalled state if not changed by workflow
                production.write({'state':'done'})
                if not list_products:
                    list_products = ''
                list_products = list_products + _('%s %s \n')%(production.product_qty,production.product_id.name)
            elif production.state == 'cancel':
                MRP.signal_workflow(cr, uid, [production.id],'button_cancelled')
#                To force the cancelled state if not changed by workflow
                production.write({'state':'cancelled'})
        return list_products
    
    @api.multi
    def preparation_recall(self):
        for production in self.related_production_ids:
#            production.action_produce(production.id,production.product_qty,'consume_produce',False)
            production.signal_workflow('button_recall')
#            To force the recalled state if not changed by workflow
            production.write({'state':'recalled'})

        return True

class product_template(models.Model):
    _inherit = 'product.template'
    
    @api.multi
    @api.depends('name')
    def name_get(self):
        result = []
        for product in self:
            name =  product.name
            result.append((product.id, name))
        return result
    
class product_product(models.Model):
    _inherit = 'product.product'
    
    @api.multi
    @api.depends('name')
    def name_get(self):
        result = []
        for product in self:
            name =  product.name
            result.append((product.id, name))
        return result