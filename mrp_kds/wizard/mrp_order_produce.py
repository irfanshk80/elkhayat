# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models


class wizard_produce_mrp_order(models.TransientModel):

    _name = "wizard.produce.mrp.order"

    def produce_mrp_orders(self, cr, uid, ids, context=None):
        active_ids = context.get('active_ids')
        print "mrp orders",active_ids
        from openerp import workflow
        move_obj = self.pool.get('stock.move')
        mrp_obj = self.pool.get('mrp.production')
        for order in mrp_obj.browse(cr, uid, active_ids):
            move_obj.force_assign(cr, uid, [x.id for x in order.move_lines])
            if mrp_obj.test_ready(cr, uid, [order.id]):
                workflow.trg_validate(uid, 'mrp.production', order.id, 'moves_ready', cr)
                mrp_obj.action_produce(cr, uid, order.id, order.product_qty, 'consume_produce', context=context)
                
        return True




class wizard_force_stock_picking(models.TransientModel):

    _name = "wizard.force.stock.picking"

    def make_delivery_done(self, cr, uid, ids, context=None):
        active_ids = context.get('active_ids')
        print "mrp orders",active_ids
        from openerp import workflow
        move_obj = self.pool.get('stock.move')
        picking_obj = self.pool.get('stock.picking')
        for order in picking_obj.browse(cr, uid, active_ids):

            move_ids = [x.id for x in order.move_lines]
            move_obj.force_assign(cr, uid, move_ids, context=context)
            move_obj.action_done(cr, uid, move_ids, context=context)

#            move_obj.force_assign(cr, uid, [x.id for x in order.move_lines])
#            if self.pool.get('mrp.production').test_ready(cr, uid, [order.id]):
#                workflow.trg_validate(uid, 'mrp.production', order.id, 'moves_ready', cr)
            picking_obj.do_new_transfer(cr, uid, order.id, context=context)

        return True