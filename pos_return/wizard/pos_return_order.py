# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from openerp import api, fields, models, _
from openerp.exceptions import UserError, ValidationError
from openerp import SUPERUSER_ID

class PosReturnOrder(models.TransientModel):
    _name = "pos.return.order"
    _description = "POS Return Order"

    name = fields.Char('Order Reference', required=True)
    line_ids = fields.One2many('pos.return.order.line','return_order_id','Order Lines')
    supervisor_passwrod = fields.Char('Supervisor Password')
    cancel_reason_id = fields.Many2one('cancellation.reason', string='Cancellation Reason', required=True)

    @api.multi
    def return_product(self):
        self.ensure_one()
        PosOrder = self.env['pos.order']
        #search for opened session
        current_session = self.env['pos.session'].search([('state', '!=', 'closed'), ('user_id', '=', self.env.uid)], limit=1)
        if not current_session:
            raise ValidationError(_('To return product(s), you need to open a session that will be used to register the refund.'))
        #check if password entered by supervisor is correct
        if self.supervisor_passwrod != current_session.security_pin:
            raise ValidationError(_('Invalid Supervisor Password!'))
        if not self.line_ids:
            raise ValidationError(_('Please add products to return'))
        #append Order with input string
        order_ref = self.name.strip()
        if 'Order' not in order_ref:
            order_ref = 'Order '+order_ref
        cancel_reason_id = self.cancel_reason_id.id
        order_id = PosOrder.sudo().search([('pos_reference','=',order_ref),('state','in',('paid','done','invoiced'))],limit=1)
        if not order_id:
            raise ValidationError(_('No order found with given reference.'))
        return_lines = {}
        product_dict = {}
        lines_to_copy = []
        #handling multiple lines having same products
        for line in self.line_ids:
            return_lines.update({line.product_id.id: return_lines.get(line.product_id.id,0.0)+ line.quantity})
        for line in order_id.lines:
            if line.product_id.id in product_dict:
                product_dict[line.product_id.id]['qty'] += line.qty-line.returned_qty
            else:
                product_dict.update({line.product_id.id: {'qty': line.qty-line.returned_qty,'line_id': line,'product_name': line.product_id.name}})
        for product_id,qty in return_lines.iteritems():
            if product_id not in product_dict:
                raise ValidationError(_('Product(s) not found in given order'))
            if qty > product_dict[product_id]['qty']:
                raise ValidationError(_('You can return only (%s) quantity of product (%s).')%(product_dict[product_id]['qty'],product_dict[product_id]['product_name']))
            lines_to_copy.append(product_dict[product_id]['line_id'])
         
        #create duplicate order with existing order info
        clone_id = order_id.with_context(from_return=True).copy({
            # ot used, name forced by create
            'name': order_ref + _(' REFUND'),
            'session_id': current_session.id,
            'date_order': fields.Datetime.now(),
            'pos_reference': order_id.name,
            'lines' : [],
            'order_parent_id': order_id.id,
            'emis_returnorder_id': int(self.env['ir.sequence'].get('return.sequence')),
        })
#         order_id.write({'emis_parentorder_id':order_id.id})
        for line in lines_to_copy:
            qty_return = return_lines[line.product_id.id]
            new_line = line.copy({
                'order_id': clone_id.id,
                'qty':-qty_return,
                'cancel_reason_id':cancel_reason_id
                })
            line.write({'returned_qty': line.returned_qty+qty_return})
        return {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': clone_id.id,
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        

class PosReturnOrderLine(models.TransientModel):
    _name = "pos.return.order.line"

    product_id = fields.Many2one('product.product', required=True, domain=[('available_in_pos', '=', True)])
    quantity = fields.Float('Quantity', required = True, default=1.0)
    return_order_id = fields.Many2one('pos.return.order','Return Order')


