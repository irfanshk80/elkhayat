# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name' : 'Point of Sale Return Management',
    'version' : '1.0',
    'summary': 'Manage your point of sale return with Supervisor Autherozation',
    'description': """

Refund/Return for Point of Sales orders
=====================================================

This module manages:

* Return of Paid/posted orders.
* Partial/Full return
* Supervisor Password Authentication for refund creation

    """,
    'category': 'Point of Sale',
    'sequence': 20,
    'website' : 'https://www.odoo.com/page/point-of-sale',
    'depends' : ['point_of_sale'],
    'demo' : [],
    'data' : [  'security/ir.model.access.csv',
                'wizard/pos_return_order_view.xml'],
    'test' : [],
    'auto_install': False,
    'installable': True,
}
