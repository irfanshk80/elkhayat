# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _, SUPERUSER_ID
from openerp.exceptions import UserError

class PosOrderLine(models.Model):
    
    _inherit = "pos.order.line"

    returned_qty = fields.Float('Returned Quantity')
    cancel_reason_id = fields.Many2one('cancellation.reason', string='Cancellation Reason')

class PosOrder(models.Model):

    _inherit = 'pos.order'

    order_parent_id = fields.Many2one('pos.order', "Parent ID")

    @api.multi
    def copy(self, default=None):
        context = dict(self.env.context or {})
        if context.get('from_return'):
            return super(PosOrder, self).copy(default=default)
        raise UserError(_('You are not allowed to duplicate an Order'))


class CancellationReason(models.Model):
    _name = "cancellation.reason"

    name = fields.Char('Cancellation Reason', required=True)
    