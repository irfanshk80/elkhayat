# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details

from openerp.osv import osv, fields
from openerp.tools.translate import _
from datetime import datetime,  timedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT

import csv
import cStringIO
import base64

class create_pos_report(osv.osv_memory):

    _name = 'create.pos.report'
    _columns = {
        'date_start': fields.date('Date Start', required=True)
    }

    _defaults = {
        'date_start': fields.date.context_today,
    }


    def _send_mail_notification(self, cr, uid, context=None):
        if context is None: context ={}
        date_start = (datetime.now() - timedelta(days=1)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        date_end =  datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        data = {}
        data['form'] = {'date_start' : date_start, 'date_end' : date_end,'user_ids': False }
        if context.get('supervisor_shift_id', False):
            data['form'].update({'supervisor_shift_id': context.get('supervisor_shift_id')})
        
        result, format = self.pool['report'].get_pdf(cr, uid, [], 'pos_brew92.report_detailsofsales_custom',data=data, context=context), 'pdf'
        self.send_email_attachment(cr, uid, 'pos_brew92', 'email_template_sales_report', result, format, date_start, 'Sales Report', context=context)
        #to send product mix report
#        session_obj = self.pool['pos.session']
#        #session_ids = session_obj.search(cr,uid,[('start_at','>=',date_start),('stop_at','<',date_end),('state','=','closed')])
#        cr.execute("select max(id) from pos_session where state='closed'")
#        session_ids = cr.fetchone()[0] or 0
#        if session_ids:
#            context.update({'active_id': session_ids})
#            self.generate_product_count_report(cr, uid, context=context)
        return True

    def send_email_attachment(self, cr, uid, module_name, email_template, attachment, format, date_start, subject, context=None):

        if context is None: context={}
        context = dict(context) #handling for frozendict
        partner_obj = self.pool['res.partner']
        mail_obj = self.pool['mail.mail']
        md = self.pool.get('ir.model.data')
        mail_template_id = md.get_object(cr, uid, module_name, email_template)
        to_partner_email = mail_template_id.email_to
        if to_partner_email:
            partner_emails = to_partner_email.split(",")
            partner_ids = []
            for email in partner_emails:
                partner_ids += partner_obj.search(cr, uid, [('email','like',str(email))])

            data_attach = {
                'name': subject,
                'datas': base64.b64encode(attachment),
                'datas_fname': subject+ ' '+date_start+'.'+format,
                'description': subject+ date_start,
                'res_model': 'res.partner',
                'res_id': partner_ids[0]
            }

            if partner_ids:
                context.update({'active_id': partner_ids[0],'active_model': 'res.partner','default_model': 'res.partner'})
                template_values = self.pool.get('mail.template').generate_email(cr, uid, mail_template_id.id, partner_ids[0], context=context)
                template_values['subject'] =  subject+ ' ' + date_start[0:10]
                template_values['attachment_ids'] = [(0, 0, data_attach)]
                mail_id =  mail_obj.create(cr, uid, template_values)
                mail_obj.send(cr,uid,[mail_id])
                partner_obj.message_post(cr, uid, [partner_ids[0]], body=_(template_values['body']))
        return True

  
    def generate_product_count_report(self, cr, uid, context=None):
        session_id = context.get('active_id',False)
        if session_id:
            categ_info = {}
            product_information= {}
            order_ids = self.pool['pos.session'].browse(cr, uid, session_id).order_ids
            for order in order_ids:
                for line in order.lines:
                    product_id = line.product_id.id
                    if product_id in product_information:
                        product_information[product_id]['qty'] += line.qty
#                        product_information[product_id]['price_subtotal'] += line.price_subtotal
                    else:
                        product_information.update({product_id:{'categ_name': line.product_id.categ_id.name, 'name' : line.product_id.name,'qty': line.qty}})
#                        'price_subtotal': line.price_subtotal}})
            for key,product in product_information.iteritems():
                if product['categ_name'] in categ_info:
                    categ_info[product['categ_name']].append(product)
                else:
                    categ_info[product['categ_name']] = [product]

            if categ_info:
                buf = cStringIO.StringIO()
                writer = csv.writer(buf, 'UNIX')
                writer.writerow(['Product Category','Product Name','Quantity'])
                for key, value in categ_info.iteritems():
                    writer.writerow([key,'','',''])
                    for product in value:
                        writer.writerow(['',product['name'],product['qty']])
                out = buf.getvalue()
                date_start = (datetime.now() - timedelta(days=1)).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
   #             self.send_email_attachment(cr, uid, 'pos_brew92', 'email_template_product_mix_report', out, '.csv', date_start, 'Product Mix Report', context=context)
        return True

create_pos_report()
