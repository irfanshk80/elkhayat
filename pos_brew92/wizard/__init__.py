# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import pos_details
import pos_payment
import create_pos_report
import change_pos_payment
import change_floor
import account_bank_statement
import random_check_wizard