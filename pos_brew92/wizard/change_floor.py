# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _

class ChangeFloor(models.TransientModel):
    _name = 'change.floor'

    user_id = fields.Many2one('res.users', 'Waiter', required=True,)
    floor_id = fields.Many2one('restaurant.floor', 'Floor')

    @api.onchange('user_id')
    def onchange_user_id(self):
        if self.user_id.floor_id :
            self.floor_id = self.user_id.floor_id.id
            
    @api.one
    def change_floor(self):
        self.ensure_one()
         
        if self.floor_id:
            self.user_id.write({'floor_id':self.floor_id.id})
        else:
            self.user_id.write({'floor_id':False})
         
        return True
