# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _


#class AccountBankStmtCashWizard(models.Model):
#
#    _inherit = 'account.bank.statement.cashbox'
#
#
#    @api.model
#    def default_get(self, fields):
#        print fields
#        res = super(AccountBankStmtCashWizard, self).default_get(fields)
#        res['cashbox_lines_ids'] = [[0, 0, {'coin_value' : 1.0, 'number' : 0}],
#                                    [0, 0, {'coin_value' : 5.0, 'number' : 0}],
#                                    [0, 0, {'coin_value' : 10.0, 'number' : 0}],
#                                    [0, 0, {'coin_value' : 50.0, 'number' : 0}],
#                                    [0, 0, {'coin_value' : 100.0, 'number' : 0}],
#                                    [0, 0, {'coin_value' : 500.0, 'number' : 0}],
#                                    ]
#        return res
#
#    #update real ending balance, add initial amount to match with ending balance
#    @api.multi
#    def validate(self):
#        bnk_stmt_id = self.env.context.get('bank_statement_id', False) or self.env.context.get('active_id', False)
#        bnk_stmt = self.env['account.bank.statement'].browse(bnk_stmt_id)
#        total = 0.0
#        for lines in self.cashbox_lines_ids:
#            total += lines.subtotal
#        if total > 0.0:
#            if self.env.context.get('balance', False) == 'start':
#                #starting balance
#                bnk_stmt.write({'balance_start': total, 'cashbox_start_id': self.id})
#            else:
#                #closing balance
#                bnk_stmt.write({'balance_end_real': total, 'cashbox_end_id': self.id})
#        return {'type': 'ir.actions.act_window_close'}


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    state = fields.Selection(string='Status',related='statement_id.state', readonly=True, store=True, copy=False, default='open')
    
    