# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
from openerp.exceptions import UserError, ValidationError

class random_check_wizard(models.TransientModel):

    _name = 'random.check.wizard'

    total_cash = fields.Float('Total Cash')

    @api.multi
    def validate_cash(self):
        self.ensure_one()
        PosSession = self.env['pos.session']
        active_id = self.env.context.get('active_id', False)
        PosRandom = self.env['pos.random.check']
        cr = self.env.cr

        if active_id:
            session_data = PosSession.browse(active_id)
            session_name = session_data.name
            input_amount = self.total_cash
            if self.env.user.id not in [user.id for user in session_data.config_id.allowed_users]:
                raise ValidationError(_('You are not authorize user to do random check'))

            if session_data.random_count > session_data.config_id.max_check:
                raise ValidationError(_('Maximum Random Check Limit Exceeded'))
            
            
            start_balance = session_data.cash_register_balance_start
#            total_cash = session_data.cash_register_total_entry_encoding
            total_cash = session_data.cash_register_total_entry_encoding

            total_credit_card = sum([statement.balance_end for statement in session_data.statement_ids]) - total_cash
            #get totoal_invoice paid in cash
            journal_ids = [journal.id for journal in session_data.journal_ids]
            cash_journal_id = session_data.cash_journal_id.id

            #get total Credit card orders
            cr.execute("select count(*) from (select distinct pos_statement_id from account_bank_statement_line where ref=%s and amount > 0.0 and journal_id in %s) as temp",(session_name, tuple(set(journal_ids)-set([cash_journal_id]))))
            total_credit_card_inv = cr.fetchone()[0] or 0

            #get total credit card return orders
            cr.execute("select count(*) from (select distinct pos_statement_id from account_bank_statement_line where ref=%s and amount < 0.0 and journal_id in %s) as temp",(session_name, tuple(set(journal_ids)-set([cash_journal_id]))))
            total_credit_inv_return = cr.fetchone()[0] or 0

            #total cash orders
            cr.execute("select count(*) from (select distinct pos_statement_id from account_bank_statement_line where ref=%s and amount > 0.0 and journal_id = %s) as temp",(session_name, cash_journal_id))
            total_cash_inv = cr.fetchone()[0] or 0

            cash_return_orders = session_data.order_ids.filtered(lambda order: order.amount_total < 0.0)

            retrun_amount = sum([order.amount_total for order in cash_return_orders])
        

            difference = start_balance + total_cash - input_amount
            default_company = self.env.user.company_id
            random_id = PosRandom.create({
                'session_id' : active_id,
                'pos_config_id' : session_data.config_id.id,
                'muc_bus_unit' : default_company.business_unit,
                'muc_prefix' : default_company.shift_prefix,
                'muc_shift': session_data.shift_type,
                'muc_unit': default_company.unit_id,
                'muc_date': session_data.start_at,
                'muc_cash': input_amount,
                'muc_cash_a': total_cash + start_balance,
                'muc_cashr_a': retrun_amount,
                'muc_cash_i': total_cash_inv,
                'muc_cashr_i': len(cash_return_orders),
                'muc_cr_i': total_credit_card_inv,
                'muc_crr_i': total_credit_inv_return,
                'muc_diff': -difference,
                'muc_emp': self.env.user.emis_id,
                'muc_cno' : session_data.random_count,
                'muc_cash_emp' : session_data.user_id.emis_id,
                'muc_open_amount': start_balance,
                'muc_credit_card_amount': total_credit_card
                    })
            if random_id:
                session_data.write({'random_count': session_data.random_count+1})

            context = dict(active_id=random_id.id, active_model='pos.random.check')

            random_form = self.env.ref('pos_brew92.view_pos_random_check_form', False)
            
#            return {
#                'name': _('Random Check'),
#                'type': 'ir.actions.act_window',
#                'view_type': 'form',
#                'view_mode': 'form',
#                'res_model': 'pos.random.check',
#                'res_id': random_id.id,
#                'context': context,
#                'views': [(random_form.id, 'form')],
#                'view_id': random_form.id,
#                'target': 'new',
#            }

            return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'pos.random.check',
            'target': 'current',
            'res_id': random_id.id
        }
