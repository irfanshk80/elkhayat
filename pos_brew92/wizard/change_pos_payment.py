# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _

class ChangePosPayment(models.TransientModel):
    _name = 'change.pos.payment'

    journal_id = fields.Many2one('account.journal', 'Payment Mode', required=True, domain=[('journal_user', '=', True)])

    @api.one
    def change_payment(self):
        self.ensure_one()
        active_id = self.env.context.get('active_id', False)
        if active_id:
            selected_journal = self.journal_id.id
            bank_statement = self.env['account.bank.statement']
            bank_statment_line = self.env['account.bank.statement.line']
            statement_id = bank_statement.search([('journal_id','=',selected_journal),('state','=','open')], limit=1).id
            if statement_id:
                line_data = bank_statment_line.browse(active_id).write({'journal_id' : selected_journal,'statement_id' : statement_id})
        return True

    