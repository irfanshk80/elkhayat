# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from openerp.osv import osv, fields
from openerp.tools.translate import _


from openerp import api, fields, models, _

class pos_make_payment(models.TransientModel):
    _inherit = 'pos.make.payment'

    @api.multi
    def check(self):
        """Check the order:
        if the order is not paid: continue payment,
        if the order is paid print ticket.
        """
        self.ensure_one()
        order = self.env['pos.order'].browse(self.env.context.get('active_id', False))
        amount = order.amount_total - order.amount_paid
        data = self.read()[0]
        # this is probably a problem of osv_memory as it's not compatible with normal OSV's
        data['journal'] = data['journal_id'][0]
        if amount != 0.0:
            order.add_payment(data)
        if order.test_paid():
            order.signal_workflow('paid')
            order.print_bill()
            return {'type': 'ir.actions.act_window_close'}
        return self.launch_payment()


    def _default_journal(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            session = self.env['pos.order'].browse(active_id).session_id
            if session:
                for journal in session.config_id.journal_ids:
                    if journal.type == 'cash':
                        return journal.id
#            return session.config_id.journal_ids and session.config_id.journal_ids.ids[0] or False
        return False

    journal_id = fields.Many2one('account.journal', 'Payment Mode', required=True, domain=[('journal_user', '=', True)], default=_default_journal)