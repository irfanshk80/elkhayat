# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _


class AccountBankStmtCashWizard(models.Model):

    _inherit = 'account.bank.statement.cashbox'

    management_credit = fields.Float('Management Credit')
#    customer_credit = fields.Float('Credit Card Sale')
#    total_invoice = fields.Integer('Total Invoices')


    @api.model
    def default_get(self, fields):
        res = super(AccountBankStmtCashWizard, self).default_get(fields)
        print self.env.context
        if 'session_id' in self.env.context:
            Session = self.env['pos.session']
            management_credit = Session.browse(self.env.context['session_id']).management_credit
        else:
            management_credit = 0.0
        res['cashbox_lines_ids'] = [[0, 0, {'coin_value' : 1.0,  'subtotal' : 0.0}],
                                    [0, 0, {'coin_value' : 5.0,  'subtotal' : 0.0 }],
                                    [0, 0, {'coin_value' : 10.0,  'subtotal' : 0.0}],
                                    [0, 0, {'coin_value' : 20.0,  'subtotal' : 0.0}],
                                    [0, 0, {'coin_value' : 50.0,  'subtotal' : 0.0}],
                                    [0, 0, {'coin_value' : 100.0,  'subtotal' : 0.0}],
                                    [0, 0, {'coin_value' : 500.0,  'subtotal' : 0.0}],
                                    ]
        res['management_credit'] = management_credit
        return res

    @api.multi
    def validate(self):
        print 'context - ',self.env.context
        bnk_stmt_id = self.env.context.get('bank_statement_id', False) or self.env.context.get('active_id', False)
        session_obj = self.env['pos.session']
        
        session_id = self.env.context.get('session_id', False)
        bnk_stmt = self.env['account.bank.statement'].browse(bnk_stmt_id)
        total = 0.0
        for lines in self.cashbox_lines_ids:
            total += lines.subtotal
        if self.env.context.get('balance', False) == 'start':
            #starting balance
            bnk_stmt.write({'balance_start': total, 'cashbox_start_id': self.id})
        else:
            #closing balance
            bnk_stmt.write({'balance_end_real': total, 'cashbox_end_id': self.id})
        print 'before if session_id'
        if session_id:
            pos_session = session_obj.browse(session_id)
            vals = {'management_credit': self.management_credit}
            #repeated code for setting opening and closing balance
            if self.env.context.get('balance', False) == 'start':
                vals.update({'set_opening_balance': True})
            elif self.env.context.get('balance', False) == 'end':
                vals.update({'set_closing_balance': True})
            print "vals",vals
            pos_session.write(vals)
        return {'type': 'ir.actions.act_window_close'}


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    state = fields.Selection(string='Status',related='statement_id.state', readonly=True, store=True, copy=False, default='open')
    
    