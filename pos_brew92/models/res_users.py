# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _, SUPERUSER_ID


class res_users(models.Model):
	_inherit = 'res.users'

	floor_id = fields.Many2one('restaurant.floor',string='Floor Name')

	@api.multi
	@api.onchange('pos_config')
	def onchange_pos_config(self, def_pos_config):
		floors = []
		pc_obj = self.env['pos.config']
		pc_floor = pc_obj.search([('id','=',def_pos_config)])
		for floor in pc_floor.floor_ids:
			floors.append(floor.id)
		return {'domain': {'floor_id': [('id', 'in', floors)]}}