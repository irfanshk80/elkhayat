# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import models, api, fields


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.one
    @api.depends('bom_detail')
    def _compute_choice_count(self):
        self.choice_count =  len(self.bom_detail.filtered(lambda bom: bom.has_choice==True)) or 0

    ar_display_name = fields.Char('Arabic Product Name')
    ar_display_image = fields.Binary('Arabic Product Image')
    choice_count = fields.Integer('Choice Count', compute='_compute_choice_count', store=True)