# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

#Break the structure of session restriction, which doesn't allow to open multi session
#Add number of users in pos config which can resume the same session once it's opened by cahsier
#Add configuration for Monring shift and Evening shift timing


from openerp import api, fields, models, _, SUPERUSER_ID
from openerp.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil import tz
from openerp.addons.hw_escpos.controllers.main import EscposDriver

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('Asia/Riyadh')

from openerp.http import request
import werkzeug.utils
import time

import urllib2
import json

class pos_shifts(models.Model):

    _name = 'pos.shifts'

    name = fields.Char('Shift Name')
    start_time = fields.Float('Starts At')
    end_time = fields.Float('Ends At')
    cashier_ids = fields.Many2many('res.users','shift_user_rel','shift_id','user_id',string='Cashier')
    config_id = fields.Many2one('pos.config','Pos Configuration')

class pos_config(models.Model):

    _inherit = 'pos.config'
    
    @api.depends('session_ids')
    def _get_current_session(self):
        user_ids = []
        for pos_config in self:
#            for shift in pos_config.pos_shift_ids:
#                user_ids += shift.cashier_ids.ids
#            session = pos_config.session_ids.filtered(lambda r: r.user_id.id in user_ids and not r.state == 'closed')
            session = pos_config.session_ids.filtered(lambda r:  r.state != 'closed')
            pos_config.current_session_id = session
            pos_config.current_session_state = session.state

    @api.depends('session_ids')
    def _get_group_pos_order_taker(self):
        try:
            return self.env.ref('pos_brew92.group_pos_order_taker')
        except Exception,e:
            return False

            
#    @api.depends('session_ids')
#    def _get_group_pos_order_taker(self):
##        ir_model_data = self.env['ir.model.data']
##        template_id = ir_model_data.get_object_reference('pos_brew92', 'group_pos_order_taker')
#        try:
#            group_id = self.env.ref('pos_brew92.group_pos_order_taker')
#            return group_id
#        except Exception,e:
#            return False
#        if len(template_id)>=2:
#            return template_id[1]
#        else:
#            return False
#        return self.env.ref('pos_east_coast_wings.group_pos_order_taker')
    
#    allowed_user_ids = fields.Many2many('res.users','pos_config_users','config_id','users_id',string='Allowed Users')
    pos_shift_ids = fields.One2many('pos.shifts','config_id',string='Shifts')
    current_session_id = fields.Many2one('pos.session', compute='_get_current_session', string="Current Session")
    current_session_state = fields.Char(compute='_get_current_session')
    group_pos_order_taker_id = fields.Many2one('res.groups', string='Point of Sale Order Taker', default=_get_group_pos_order_taker,
        help='This field is there to pass the id of the pos order taker to the point of sale client')


    @api.multi
    def open_ui(self):
        assert len(self.ids) == 1, "you can open only one session at a time"
        if self.session_ids.filtered(lambda r:  r.state != 'closed').active_shift_id.state !='draft':
            raise UserError(_("Cashier shift is not opened. Please open a shift before start selling."))
        return {
            'type': 'ir.actions.act_url',
            'url':   '/pos/web/',
            'target': 'self',
        }

#     @api.multi
#     def open_ui(self):
#         assert len(self.ids) == 1, "you can open only one session at a time"
#         if self.session_ids.filtered(lambda r:  r.state != 'closed').active_shift_id.state !='draft':
#             raise UserError(_("Cashier shift is not opened. Please open a shift before start selling."))
#         return {
#             'type': 'ir.actions.act_url',
#             'url':   '/pos/web/',
#             'target': 'self',
#         }



class pos_session(models.Model):

    _inherit = 'pos.session'

    session_name = fields.Char('Shift Name')
    shift_ids = fields.One2many('pos.shift.summary','session_id','Shift')
    active_shift_id = fields.Many2one('pos.shift.summary','Active Shift')
    shift_state = fields.Selection(related='active_shift_id.state', store=True, readonly=True)
#    shift_id = fields.Many2one('pos.shifts','Shift')

    to_zone = tz.gettz('Asia/Riyadh')

    #Check whether all sifts are closed
    def wkf_action_closing_control(self):
        for session in self:
            for shift in session.shift_ids:
                if shift.state == 'draft':
                    raise UserError(_("Please close the shifts before closing this session "))
        return super(pos_session,self).wkf_action_closing_control()

    @api.multi
    def open_frontend_cb(self):
        if not self.ids:
            return {}
        assert len(self.ids) == 1, "you can open only one session at a time"
        if self.active_shift_id.state !='draft':
            raise UserError(_("Cashier shift is not opened. Please open a shift before start selling."))
#        for session in self.filtered(lambda s: s.user_id.id != self.env.uid):
#            raise UserError(_("You cannot use the session of another users. This session is owned by %s. "
#                              "Please first close this one to use this point of sale.") % session.user_id.name)
        return {
            'type': 'ir.actions.act_url',
            'target': 'self',
            'url':   '/pos/web/',
        }


#    @api.multi
#    def login(self):
#        self.ensure_one()
#        if self.env.user.id not in self.shift_id.cashier_ids.ids:
#            print "loging function is getting called"
#            request.session.logout(keep_db=True)
#            return werkzeug.utils.redirect('/web', 303)
##            raise UserError(_("You are not allowed to use this session"))
#        self.write({
#            'login_number': self.login_number + 1,
#        })

#    @api.model
#    def create(self, values):
#        config_id = values.get('config_id') or self.env.context.get('default_config_id')
#        if not config_id:
#            raise UserError(_("You should assign a Point of Sale to your session."))
#
#        # journal_id is not required on the pos_config because it does not
#        # exists at the installation. If nothing is configured at the
#        # installation we do the minimal configuration. Impossible to do in
#        # the .xml files as the CoA is not yet installed.
#        pos_config = self.env['pos.config'].browse(config_id)
#        ctx = dict(self.env.context, company_id=pos_config.company_id.id)
#        to_zone = tz.gettz('Asia/Riyadh')
#        if self.env.context.get('tz', False):
#            to_zone = tz.gettz(self.env.context['tz'])
#
#        shift_creation_date = datetime.now()
#        utc = shift_creation_date.replace(tzinfo=from_zone)
#
#        # Convert time zone
#        local_time = utc.astimezone(to_zone)
#
#        start_time = str(local_time.hour) + '.' + str(local_time.minute)
#        shift_ids = pos_config.pos_shift_ids
#
#        for shift in shift_ids:
#            if round(shift.start_time, 2) <= float(start_time)< round(shift.end_time, 2):
#                values.update({'shift_id': shift.id})
#            else:
#                continue
##        if not values.get('shift_id'):
##            raise UserError(_("No shift configuration defined for current time. Please define configuration before starting session"))
#
#        return super(pos_session, self.with_context(ctx)).create(values)



class pos_order(models.Model):
    _name = 'pos.order'
    
    _inherit = ['pos.order','mail.thread']

    def _default_session(self):
        return self.env['pos.session'].search([('state', '=', 'opened')], limit=1)

    def _default_pricelist(self):
        return self._default_session().config_id.pricelist_id

    

    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=True, states={
                                   'draft': [('readonly', False)]}, readonly=True, default=_default_pricelist)

    amount_tax = fields.Float(compute='_compute_amount_all', string='Taxes', digits=0, track_visibility='always')
    amount_total = fields.Float(compute='_compute_amount_all', string='Total', digits=0, track_visibility='always')
    amount_paid = fields.Float(compute='_compute_amount_all', string='Paid', states={'draft': [('readonly', False)]}, readonly=True, digits=0, track_visibility='always')
    amount_return = fields.Float(compute='_compute_amount_all', string='Returned', digits=0, track_visibility='always')
    table_id = fields.Many2one('restaurant.table','Table', help='The table where this order was served',track_visibility='always')
    json_object =  fields.Text('Json Data')
    json_import = fields.Boolean('Cashier Import')
    order_uid =  fields.Char('POS Uid')
    shift_id = fields.Many2one('pos.shift.summary', string='Shift Name', readonly=True, states={'draft': [('readonly', False)]})
    
    

    @api.depends('statement_ids', 'lines.price_subtotal_incl', 'lines.discount')
    def _compute_amount_all(self):
        for order in self:
            order.amount_paid = order.amount_return = order.amount_tax = 0.0
            currency = order.pricelist_id.currency_id
            order.amount_paid = sum(payment.amount for payment in order.statement_ids)
            order.amount_return = sum(payment.amount < 0 and payment.amount or 0 for payment in order.statement_ids)
            order.amount_tax = currency.round(sum(self._amount_line_tax(line, order.fiscal_position_id) for line in order.lines))
            amount_untaxed = currency.round(sum(line.price_subtotal for line in order.lines))
            order.amount_total = order.amount_tax + amount_untaxed

    @api.multi
    def copy(self, default=None):
        raise UserError(_('You are not allowed to duplicate an Order'))

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(pos_order, self)._order_fields(ui_order)
        order_fields['json_object'] = ui_order
        order_fields['order_uid'] = ui_order.get('uid',False)
        order_fields['json_import'] = False
        order_fields['shift_id'] = self.env['pos.shift.summary']._get_active_shift(ui_order.get('pos_session_id', False))
        return order_fields

    def print_bill(self, cr, uid, ids, context=None):

        for order in self.browse(cr, uid, ids, context):
            proxy_ip = order.session_id.config_id.proxy_ip or False
            if proxy_ip:
                url = 'http://'+proxy_ip+':8069/hw_proxy/print_xml_receipt'
                receipt = """<receipt align="center" value-thousands-separator="" width="40">"""
               
                receipt_header = """ """
                total_discount = 0.0
                void_transaction = 0.0
                if order.company_id.logo:
                    receipt_header = """
                                        <img src= "data:image/png;base64,%s" /> <br/>""" %(order.company_id.logo)
                else:
                    receipt_header = """<h1>%s </h1> <br/>"""%(order.company_id.name)
                receipt_header = receipt_header + """
                                    <div font="b">
                                """
                if order.location_id:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.location_id.name)
                if order.company_id.street:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.company_id.street)
                if order.company_id.email:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.company_id.email)

                if order.company_id.website:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.company_id.website)
                if order.user_id:
                    receipt_header = receipt_header + """

                                        <div class="cashier">
                                            <div>--------------------------------</div>
                                            <div>Served by %s</div>"""%(order.user_id.name)
                    if order.table_id:
                        receipt_header = receipt_header + """
                                            at table %s
                                            <div>Guests: %s</div>"""%(order.table_id.name,order.customer_count)
                    receipt_header = receipt_header + """
                        </div>

                        </div>
                        <br/><br/>
                """

                receipt_line = """<div class="orderlines" line-ratio="0.6">"""
                for line in order.lines:
                    simple = True
                    if line.discount != 0.0 or line.qty != 1.000 :
                        simple = False
                    if simple:
                        receipt_line  = receipt_line + """
                                            <line>
                                                <left>%s</left>
                                                <right><value>%s</value></right>
                                            </line>"""%(line.product_id.name,line.price_subtotal_incl)
                    else:
                        total_discount += (line.price_unit*line.discount/100)*line.qty
                        
                        receipt_line  = receipt_line + """
                                            <line>
                                                <left>%s</left>
                                            </line>"""%(line.product_id.name)
                        if line.discount!=0.0:
                            receipt_line  = receipt_line + """
                                            <line indent='1'><left>Discount: %s%%</left></line>"""%(line.discount)
                        receipt_line  = receipt_line + """
                                                <line indent='1'>
                                                    <left>
                                                        <value value-decimals='3' value-autoint='on'>
                                                            %s
                                                        </value>

                                                        x
                                                        <value value-decimals='2'>
                                                            %s
                                                        </value>
                                                    </left>
                                                    <right>
                                                        <value>%s</value>
                                                    </right>
                                            </line>"""%(line.qty,line.price_unit,line.price_subtotal_incl)
                                            
                                            
                    if line.product_id.ar_display_image:
                        receipt_line  = receipt_line + """
                        <line>
                                            <left><img src= "data:image/png;base64,%s" /></left>
                                            </line>"""%(line.product_id.ar_display_image)
                   
                        
                receipt_line  = receipt_line + """</div>"""

                receipt_total = """<line><right>--------</right></line>
                                    <line class="total" size="double-height">
                                        <left><pre> TOTAL</pre></left>
                                        <right><value>%s</value></right>
                                    </line>
                                    <br/><br/>"""%(order.amount_total)
                receipt_payment =""""""
                receipt_change = """"""
                if order.statement_ids:
                    for statement in order.statement_ids:

                        if statement.amount > 0.0:
                            receipt_payment  = receipt_payment + """
                                            <line>
                                                <left>%s</left>
                                                <right><value>%s</value></right>
                                            </line>
                                            <br/>
                                            """%(statement.journal_id.name_get()[0][1],statement.amount)
                    receipt_change = """
                                        <line size="double-height">
                                            <left><pre> CHANGE</pre></left>
                                            <right><value>%s</value></right>
                                        </line>
                                        <br/>

                                        <div class="before-footer"></div>"""%(abs(order.amount_return))
                discount_info = """"""""
                if total_discount!=0.0:
                    discount_info = """"<line>
                            <left>Discounts</left>
                            <right><value>%s</value></right>
                        </line>"""%(total_discount)
                        
                footer = """
                        <br/>
                        <br/>
                        <div font='b'>
                            <div>%s</div>
                        </div>
                        <div font='b'>
                                <div>Thank You For Visiting</div>
                                <div>Please Come Again</div>
                            </div>"""%(datetime.now().strftime('%Y-%m-%d %H:%M%S'))

                receipt = receipt + receipt_header + receipt_line + receipt_total +receipt_payment+ receipt_change + discount_info + footer +"""</receipt>"""
                print"receipt......",receipt
                
                try:
                    driver = EscposDriver()
                    driver.push_task('xml_receipt',receipt,network_ip='')

                except Exception as e:
                    raise UserError(_(e))
        return True

    @api.one
    def print_kitchen_order(self):
        proxy_printers = self.session_id.config_id.printer_ids or []
        for printer in proxy_printers:
            orderlines = self.lines.filtered(lambda r: r.product_id.pos_categ_id in printer.product_categories_ids)
            print "order lines",orderlines
            network_ip = printer.network_ip
            if network_ip:
                url = 'http://'+network_ip+':8069/hw_proxy/print_xml_receipt'
                
                receipt = """<receipt
                            align='center'
                            width='40'
                            size='double-height'
                            line-ratio='0.4'
                            value-decimals='3'
                            value-thousands-separator=''
                            value-autoint='on'
                            >"""
                receipt_header = """"""
                if self.table_id:
                    receipt_header = """
                    <div size='normal' >%s</div>
                            <br />
                            <div><span>%s</span> / <span bold='on' size='double'>%s</span> </div>
                            <div>
                                <span>%s</span>
                            </div>
                        <br />
                        <br />"""%(self.name,self.table_id.floor_id.name,self.table_id.name,self.user_id and self.user_id.name or '')
                for line in orderlines:
                    receipt_header += """
                    <line >
                        <right><value>%s</value></right>
                            <right><div>%s</div></right>
                    </line>"""%(line.qty,line.product_id.name)
                    if line.note:
                        receipt_header += """
                        <line size='normal'>
                            <right>-----------NOTE----------</right>
                        </line> """
                        for note in line.note.split('\n'):
                            receipt_header += """
                        <right><span size='normal' bold='off' line-ratio='0.4' align="right" indent='1' width='30'>
                                	%s<br />
                        </span></right>
                        <line></line>"""%(note)
                    receipt_header+="""
                    <div><span> -----------------------------------------</span></div>
                    """

             

                receipt = receipt + receipt_header +"""</receipt>"""
#                print"receipt......",receipt
                data = {
                    "jsonrpc": "2.0",
#                    "params": {"receipt": u'<receipt align="center" font="a" value-thousands-separator="," width="30"><h1>Company</h1><br/><div font="a"><br/>' + \
#                                            u'</div></receipt>',
                    "params": {"receipt":  receipt,
#                                "order": order,
#                                "orderlines":order.lines
                                            },
                }
            try:
                driver = EscposDriver()
                driver.push_task('xml_receipt',receipt,network_ip='')
                time.sleep(5)

            except Exception as e:
                raise UserError(_(e))

        return True


class pos_order_line(models.Model):
    _inherit = "pos.order.line"

    line_detail = fields.One2many('pos.order.line.detail', 'line_id', 'Order Line Details', readonly=True, copy=True)

    @api.model
    def create(self, values):
        product_id = values.get('product_id',False)
        order_qty = values.get('qty',False)
        if product_id:
            product_brw = self.env['product.product'].browse(product_id)
            default_choice_items = self.env['product.bom.detail'].search([('parent_id','=',product_brw.product_tmpl_id.id),('has_choice','=',False)])
            if default_choice_items:
                values['line_detail'] = values.get('line_detail',[])
                for item in default_choice_items:
                    values['line_detail'].append([0,0,{'product_id':item.product_id.id,'qty':item.qty}])
                for line in values['line_detail']:
                    if line[2] and line[2].get('qty', False):
                        line[2]['qty']=  line[2]['qty']*order_qty
        return super(pos_order_line, self).create(values)

    @api.multi
    def write(self,values):
        if self.env.context.get('from_pos_interface', False):
            if values.get('line_detail',False):
                del values['line_detail']
        return super(pos_order_line, self).write(values)



class pos_order_line_detail(models.Model):
    _name = "pos.order.line.detail"


    product_id =  fields.Many2one('product.product', 'Product', domain=[('sale_ok', '=', True)], required=True, change_default=True)
    qty = fields.Float('Quantity', default=1)
    line_id = fields.Many2one('pos.order.line', 'Order Line Ref', ondelete='cascade')


class account_cashbox_count(models.Model):
    '''Custom class to count petty cash balance'''

    _name = 'account.cashbox.count'


    @api.one
    @api.depends('coin_count', 'coin_value')
    def _compute_subtotal(self):
        self.amount = self.coin_value * self.coin_count

    coin_value = fields.Float('Coin/Note Value')
    coin_count = fields.Float('Count')
    amount = fields.Float(string='Amount',
        store=True, readonly=True, compute='_compute_subtotal')
    shift_id = fields.Many2one('pos.shift.summary','Shift')

    


class pos_shift_summary(models.Model):

    _name = 'pos.shift.summary'

    @api.one
    @api.depends('cash_count_ids')
    def _compute_total_cash(self):
        self.total_cash = sum(line.amount for line in self.cash_count_ids)

    @api.one
    @api.depends('management_credit','initial_balance','customer_credit','total_cash')
    def _compute_total(self):
        shift_id = self.id
        self.total_sale = sum([order.amount_total for order in self.session_id.order_ids.filtered(lambda o: o.shift_id.id==shift_id and o.state=='paid')])
        self.difference = self.total_sale - (self.total_cash + self.management_credit + self.customer_credit - self.initial_balance)
        

    name = fields.Char('Shift Number', readonly=True, default=lambda self: self.env['ir.sequence'].next_by_code('pos.shift.summary'))
    shift_name = fields.Many2one('pos.shifts', string='Shift Name', readonly=True, states={'draft': [('readonly', False)]}, required=True)
    state = fields.Selection([('draft','Open'),('done','Closed')], readonly=True, default='draft')
    cash_count_ids = fields.One2many('account.cashbox.count', 'shift_id', string='Cash Summary', readonly=True, states={'draft': [('readonly', False)]})
    total_cash = fields.Float(string='Total Cash',
        store=True, readonly=True, compute='_compute_total_cash')
    management_credit = fields.Float('Management Credit', readonly=True, states={'draft': [('readonly', False)]})
    customer_credit = fields.Float('Credit Card Sale', readonly=True,states={'draft': [('readonly', False)]})
    employee_id = fields.Many2one('res.users', string="User", readonly=True, default=lambda self: self.env.user)
    initial_balance = fields.Float('Opening Balanace')
    total_sale = fields.Float('Total Sale',readonly=True, compute='_compute_total', store=True)
    difference = fields.Float('Difference', readonly=True,compute='_compute_total',store=True)
    
    session_id = fields.Many2one('pos.session', 'Session', required=True)
    

    @api.multi
    def _check_opened_shift(self):
        """ Check if there's any opened shift. User should be allowed to open two shifts simultaneously"""
        for shift in self:
            open_shift_ids = self.search([('state','=','draft'),('id','!=',shift.id),('session_id','=',shift.session_id.id)], limit=1)
            if open_shift_ids:
                return False
        return True

    _constraints = [(_check_opened_shift, 'Multipe shifts are not allowed for the same session', ['state','session_id']),]

    @api.model
    def default_get(self, fields):
        res = super(pos_shift_summary, self).default_get(fields)
        res.update({'cash_count_ids' : [
                [0, 0, {'coin_value' : 1.0, 'coin_count' : 0}],
                [0, 0, {'coin_value' : 5.0, 'coin_count' : 0}],
                [0, 0, {'coin_value' : 10.0, 'coin_count' : 0}],
                [0, 0, {'coin_value' : 50.0, 'coin_count' : 0}],
                [0, 0, {'coin_value' : 100.0, 'coin_count' : 0}],
                [0, 0, {'coin_value' : 500.0, 'coin_count' : 0}]
            ]})
        return res
    
    @api.one
    def close_shift(self):
        self.state = 'done'
        return True

    @api.model
    def _get_active_shift(self, session_id):
        active_shift = self.search([('state','=','draft'),('session_id','=',session_id)],limit=1)
        if active_shift:
            return active_shift.id
        return False

    @api.model
    def create(self, vals):
        rec = super(pos_shift_summary, self).create(vals)
        rec.session_id.active_shift_id= rec.id
#	rec.session_id.write({'active_shift_id':rec.id,'user_id':self.env.uid})
        rec.session_id.order_ids.filtered(lambda order: order.state=='draft').write({'shift_id': rec.id})
        return rec
        
    

    
#hide cash option for first floor user

    
