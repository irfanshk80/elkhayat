# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
import ast
import json

class res_partner(models.Model):
    
    _inherit = 'res.partner'
    
    @api.one
    @api.depends('last_order_id')
    def _get_most_ordered_product(self):
        if self.id:
            self._cr.execute("""select product_id,sum(qty) as tot_qty 
                                from pos_order_line 
                                where order_id in 
                                (select id from pos_order where partner_id = %s) 
                                group by pos_order_line.product_id 
                                ORDER BY tot_qty DESC 
                                LIMIT 1 ;
                     """%(self.id,))

            res = self._cr.fetchone()
            if res:
                self.most_order_product_id = res[0]
                self.max_order_product_count = res[1]

    

#    def get_customer_order_history(self, cr, uid, partner_id, context=None):
    @api.multi
    def get_customer_order_history(self):
        """ create or modify a partner from the point of sale ui.
            partner contains the partner's fields. """
#        orders = self.env['pos.order'].search_read([('partner_id','=',partner_id)],fields=['id', 'name', 'date_order', 'lines'],order='date_order', limit=5)
        orders = self.env['pos.order'].search_read([('partner_id','=',self.id)],fields=['id', 'name', 'date_order', 'lines'],order='date_order desc', limit=5)
        order_history ={self.id:orders}
        i=0
        for order in orders:
            lines = self.env['pos.order.line'].browse(order['lines'])
            line_detail = []
            for line in lines:
                line_detail.append({'product_id':line.product_id.id,'product_name':line.product_id.name,'product_qty':line.qty,'note':line.note})
            
            order_history[self.id][i].update({'lines':line_detail}) 
            i+=1
            
#        #image is a dataurl, get the data after the comma
##        if partner.get('image',False):
##            img =  partner['image'].split(',')[1]
##            partner['image'] = img
##
##        if partner.get('id',False):  # Modifying existing partner
##            partner_id = partner['id']
##            del partner['id']
##            self.write(cr, uid, [partner_id], partner, context=context)
##        else:
##            partner_id = self.create(cr, uid, partner, context=context)
#        json_order_history = ast.literal_eval(order_history)
        json_order_history = json.dumps(order_history)
        return json_order_history
            
            
    management_credit = fields.Boolean('Management Credit')
    security_pin = fields.Char('Security Pin')
    date_of_birth = fields.Date('Date of Birth')
    organization_id = fields.Many2one('organization.name','Organization')
    last_order_id = fields.Many2one('pos.order','Last Order')
    last_visit_date = fields.Datetime('Last Visited')
    total_visits = fields.Integer('Total Visits')
    most_order_product_id = fields.Many2one('product.product',compute='_get_most_ordered_product', string='Favorite Item')
    max_order_product_count = fields.Float(compute='_get_most_ordered_product', string='Favorite Item Qty')


class organization_name(models.Model):
    _name = 'organization.name'

    name = fields.Char('Organization Name')
    discount = fields.Float('Discount')