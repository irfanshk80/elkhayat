from openerp import api, fields, models, _
import openerp.addons.decimal_precision as dp

class res_company(models.Model):
	_inherit = 'res.company'

	vat_ar_image = fields.Binary('VAT Arabic Image')
	total_vat_ar = fields.Binary('Total with VAT Arabic Image')