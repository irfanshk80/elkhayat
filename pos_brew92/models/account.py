# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _
import openerp.addons.decimal_precision as dp

from openerp.http import request

import math

from openerp.tools import amount_to_text_en
import base64

#class AccountBankStmtCashWizard(models.Model):
#    _inherit = 'account.bank.statement'
#
#    @api.multi
#    def _get_opening_balance(self, journal_id):
#        last_bnk_stmt = self.search([('journal_id', '=', journal_id)], limit=1)
#
#        if last_bnk_stmt:
#            if last_bnk_stmt.journal_id.with_last_closing_balance:
#                return last_bnk_stmt.balance_end
#            else:
#                return 0
#        return 0
#
#class account_journal(models.Model):
#
#    _inherit  = 'account.journal'
#
#    with_last_closing_balance = fields.Boolean('Opening With Last Closing Balance', default=False, help="For cash or bank journal, this option should be unchecked when the starting balance should always set to 0 for new documents.")
   
   
class account_invoice(models.Model):

    _inherit  = 'account.invoice'

    pos_order_id = fields.Many2one('pos.order', string='Related Order')
    
    @api.multi
    def send_invoice(self):
        template = self.env.ref('account.email_template_edi_invoice', False)
        ctx = dict(
            default_model='account.invoice',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='comment',
            mark_invoice_as_sent=True,
        )

        template.with_context(ctx).send_mail(self.id, force_send=True)
        return True

    @api.multi
    def amount_to_text(self, amount, currency=''):
        convert_amount_in_words = amount_to_text_en.amount_to_text(int(math.floor(amount)), lang='en', currency=currency)
        convert_amount_in_words = convert_amount_in_words.replace(' and Zero Cent', ' Only ')
        return convert_amount_in_words
        
