##############################################################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
##############################################################################
from openerp import models, fields, api, SUPERUSER_ID, _
from openerp.exceptions import UserError
from datetime import datetime
import random
import collections


class supervisor_shift(models.Model):
    _name = "supervisor.shift"

    _order = 'id desc'

    SUP_UNIT_STATE = [
        ('draft', 'New'),
        ('opened', 'Opened'),
        ('closed', 'Closed'),
    ]

    # @api.model
    # def default_get(self, fields):
    #     res = super(supervisor_shift, self).default_get(fields)
    #     res['transaction_line_ids'] = [[0, 0, {'coin_value' : 1.0,  'subtotal' : 0.0}],
    #                                 [0, 0, {'coin_value' : 5.0,  'subtotal' : 0.0 }],
    #                                 [0, 0, {'coin_value' : 10.0,  'subtotal' : 0.0}],
    #                                 [0, 0, {'coin_value' : 20.0,  'subtotal' : 0.0}],
    #                                 [0, 0, {'coin_value' : 50.0,  'subtotal' : 0.0}],
    #                                 [0, 0, {'coin_value' : 100.0,  'subtotal' : 0.0}],
    #                                 [0, 0, {'coin_value' : 500.0,  'subtotal' : 0.0}],
    #                                 ]
    #     return res
    
    @api.one
    @api.depends('transaction_line_ids','transaction_line_ids.subtotal')
    def _compute_amount(self):
        self.total_cash = sum(line.subtotal for line in self.transaction_line_ids)

    state = fields.Selection(SUP_UNIT_STATE, 'state', default='draft', readonly=True, copy=False, index=True, store=True)
    name = fields.Char('Shift No', readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]}, required=True, default=lambda self: self.env['ir.sequence'].next_by_code('pos.supervisor.data'))

    shift_start_date = fields.Datetime('Start Date', readonly=True, default=fields.Datetime.now)
    shift_end_date = fields.Datetime('End Date', readonly=True)
    sync_supshft = fields.Boolean('Sync Supervisor shift', readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})
    user_id  = fields.Many2one('res.users',"Supervisor", default=lambda self: self.env.user)

    total_cash = fields.Float('Total Cash', store=True, compute='_compute_amount')
    total_left_change = fields.Float("Total Left Change", readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})
    total_airline_cr = fields.Float("Airline Credit", readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})
    total_mgt_cr = fields.Float("Management Credit", readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})

    trans_no = fields.Float('Trans No ', readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})
    total_amount = fields.Float('Total Amount', readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})
    transaction_line_ids = fields.One2many('cash.transaction.line','shift_id','Cash Summary',readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})


    card_transaction_lines = fields.One2many('card.transaction.line','super_shift_id',"CC Transactions", readonly=True, states={'draft': [('readonly', False)],'opened': [('readonly', False)]})

    total_cash_random = fields.Float('Total Cash Sale ', readonly=True)
    total_random_count = fields.Float('Total Random Check', readonly=True)

    #Supervisor password for void and discount
    security_pin = fields.Char('Supervisor PIN', required=True)

    @api.multi
    def _check_opened_shift(self):
        """ Check if there's any opened shift. User should be allowed to open two shifts simultaneously"""
        for shift in self:
            open_shift_ids = self.search([('state','=','opened'),('id','!=',shift.id)], limit=1)
            if open_shift_ids:
                return False
        return True

    _constraints = [(_check_opened_shift, 'Multipe shifts are not allowed', ['state','session_id']),]

    @api.multi
    def write(self, values):
        
        if self._uid != SUPERUSER_ID:
             values.update({'user_id':self._uid})
             
        return super(supervisor_shift, self).write(values)
        
    @api.one
    def close_sup_shift(self):
        session_ids = self.env['pos.session'].search([('supervisor_shift_id','=',self.id)])
        for session in session_ids:
            if session.state != 'closed':
                raise UserError(_("Cashier shift is not closed. Please close all cashier shifts before closing supervisor shift"))
        self.state = 'closed'
        self.shift_end_date = datetime.now()
        next_shift = self.create({'user_id':self._uid, 'security_pin': self.security_pin})
        next_shift.open_sup_shift()
        return True
#        try:
#            self.env['create.pos.report'].with_context({'supervisor_shift_id': self.id})._send_mail_notification()
#        except Exception, e:
#            print e
#        return True


    @api.one
    def open_sup_shift(self):
        self.state = 'opened'
        
    @api.one
    def get_cashier_data(self):
        session_ids = self.env['pos.session'].search([('supervisor_shift_id','=',self.id)], order = 'start_at asc')
        
        card_detail = {}
        cash_detail = {}
        cash_merge = {}
        management_credit = 0.0
        total_cash = 0.0
        transaction_lines = []
        card_transaction_lines = []
        self.transaction_line_ids.unlink()
        self.card_transaction_lines.unlink()
#        self.write({'total_mgt_cr':management_credit,'transaction_line_ids': transaction_lines,'card_transaction_lines':card_transaction_lines})
        for session in session_ids:
            # cash_detail = {}
            cashbox_start = {}
            cashbox_end = {}
            management_credit += session.management_credit

            for line in session.cash_register_id.cashbox_start_id.cashbox_lines_ids:
                cashbox_start.update({line.coin_value:line.number})

            for line in session.cash_register_id.cashbox_end_id.cashbox_lines_ids:
                cashbox_end.update({line.coin_value:line.number})

            #Substracting opening bal from end balance
            cash_trans = {key: cashbox_end.get(key,0) - cashbox_start.get(key,0) for key in cashbox_end.keys()}
            if cash_detail:
                #Sum values and assign
                cash_merge = {k: cash_trans.get(k, 0) + cash_detail.get(k, 0) for k in cash_trans.keys()}
                cash_detail = cash_merge
            else:
                cash_detail = cash_trans

#            To get the card transaction summary from all session of this Supervisor shift
            for statement in session.statement_ids:
                print 'journal ID ',statement.journal_id
                if statement.journal_id.card_type:
                    if statement.journal_id.id in card_detail:
                        card_detail[statement.journal_id.id]['no_of_trans'] +=len(statement.line_ids)
                        card_detail[statement.journal_id.id]['amount'] += statement.balance_end
                    else:
                        card_detail.update({statement.journal_id.id:{'no_of_trans': len(statement.line_ids),'amount' : statement.balance_end }})

        od = collections.OrderedDict(sorted(cash_detail.iteritems()))

        for k,v in od.iteritems():
            transaction_lines.append([0,0,{'coin_value' : k, 'number': v}])

        for k,v in card_detail.iteritems():
            card_transaction_lines.append([0, 0, {'card_type' : k,  'amount' : v['amount'],'no_of_trans':v['no_of_trans']}])
#        To update the card transaction summary from all session of this Supervisor shift
        self.write({'total_mgt_cr':management_credit, 'card_transaction_lines':card_transaction_lines,'transaction_line_ids':transaction_lines})
        self.get_total()
        return True
            
    @api.one
    def get_total(self):
        total_transaction = 0.0
        total_amount = 0.0
        for card_transaction_line in self.card_transaction_lines:
            total_transaction += card_transaction_line.no_of_trans
            total_amount += card_transaction_line.amount
        self.write({'trans_no': total_transaction,'total_amount':total_amount})

    @api.one
    def get_total_random(self):
        session_ids = self.env['pos.session'].search([('supervisor_shift_id','=',self.id)])
        cash_total = sum([session.cash_register_total_entry_encoding for session in session_ids])
        self.write({'total_cash_random': cash_total,'total_random_count':self.total_random_count+1})


class card_transaction_line(models.Model):

    _name = "card.transaction.line"

    amount              = fields.Float('Amount')
    no_of_trans         = fields.Float("No of Transactions")
    description         = fields.Text("Description")
    super_shift_id      = fields.Many2one('supervisor.shift',"Super Shift")
    card_type           = fields.Many2one('account.journal',"Card Type",required = True)


class cash_transaction_line(models.Model):

    _name = 'cash.transaction.line'

    @api.one
    @api.depends('coin_value', 'number')
    def _sub_total(self):
        """ Calculates Sub total"""
        self.subtotal = self.coin_value * self.number

    coin_value = fields.Float(string='Coin/Bill Value', required=True, digits=0)
    number = fields.Integer(string='Number of Coins/Bills', help='Opening Unit Numbers')
    subtotal = fields.Float(compute='_sub_total', string='Subtotal', digits=0, readonly=True)
    shift_id = fields.Many2one('supervisor.shift','Supervisor Shift')

    
    
class pos_random_check(models.Model):
    
    _name = 'pos.random.check'

    _rec_name = 'session_id'
    _order = 'id desc'

    #muc_bus_unit = fields.Char()
    
    session_id = fields.Many2one('pos.session', string='Pos Session',required=True, readonly=True)
    pos_config_id = fields.Many2one('pos.config', string='Config ID',readonly=True)

    #muc_no
    #muc_shift: will get from session
    
    muc_bus_unit = fields.Char('Business Unit', readonly=True)
    muc_prefix= fields.Char('Prefix', readonly=True)#supervisor shit
    muc_shift = fields.Char('Shift', readonly=True) 
    muc_unit = fields.Char('Unit', readonly=True)#unit codfe
    muc_date = fields.Datetime('Shift Date', readonly=True)#shift date
    
    muc_cash = fields.Float('Total Cash',readonly=True) #cash entered
    muc_cash_a = fields.Float('Cash Available', readonly=True) #pos cash
    muc_cashr_a = fields.Float('Cash Return', readonly=True)#pos cash retur
    
    muc_cr_a = fields.Float('Credit Amount', readonly=True)
    muc_crr_a = fields.Float('Credit Rreturn Amount', readonly=True)
    
    muc_cash_i = fields.Integer('Total Cash Invoice', readonly=True) #no invoivces
    muc_cashr_i = fields.Integer('Total Cash Return Invoice', readonly=True) #
    
    muc_cr_i = fields.Integer('Credit Card Invoice', readonly=True)
    muc_crr_i = fields.Integer('Credit Return Invoice', readonly=True)

    muc_diff = fields.Float('Difference', readonly=True)
    muc_cdate = fields.Datetime('Random Check Date', readonly=True, default=fields.Datetime.now) #rac date
    muc_emp = fields.Char(string="Employee", readonly=True)#cashier

    muc_cno = fields.Integer('Transaction Number', readonly=True) #number of times
    muc_cash_emp = fields.Char(string='Cashier', readonly=True)#superviosor
    muc_open_amount = fields.Float('Opening Amount', readonly=True)
    
    muc_credit_card_amount = fields.Float('Credit Card Amount', readonly=True)
