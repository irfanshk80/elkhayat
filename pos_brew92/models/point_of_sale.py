# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

#Break the structure of session restriction, which doesn't allow to open multi session
#Add number of users in pos config which can resume the same session once it's opened by cahsier
#Add configuration for Monring shift and Evening shift timing


from openerp import api, fields, models, _, SUPERUSER_ID, tools,  registry
from openerp.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil import tz
from openerp.tools import float_is_zero
from openerp.addons.hw_escpos.controllers.main import EscposDriver
from openerp.tools import float_is_zero
from_zone = tz.gettz('UTC')
to_zone = tz.gettz('Asia/Riyadh')

import time
import openerp.addons.decimal_precision as dp

import psycopg2
import logging
_logger = logging.getLogger(__name__)

import threading
import Queue
threads = []
lock = threading.Lock()

#class pos_shifts(models.Model):
#
#    _name = 'pos.shifts'
#
#    name = fields.Char('Shift Name')
#    start_time = fields.Float('Starts At')
#    end_time = fields.Float('Ends At')
#    cashier_ids = fields.Many2many('res.users','shift_user_rel','shift_id','user_id',string='Cashier')
#    config_id = fields.Many2one('pos.config','Pos Configuration')

class pos_config(models.Model):

    _inherit = 'pos.config'
    
    @api.depends('session_ids')
    def _get_current_session(self):
        #order taker can continue session
        order_taker =  (self.user_has_groups('pos_brew92.group_pos_order_taker') and \
           not self.user_has_groups('point_of_sale.group_pos_user') and
           not self.user_has_groups('pos_brew92.group_pos_supervisor') and \
           not self.user_has_groups('point_of_sale.group_pos_manager'))
        for pos_config in self:
            session = pos_config.session_ids.filtered(lambda r: (r.user_id.id == self.env.uid or order_taker) and not r.state == 'closed')
            pos_config.current_session_id = session
            pos_config.current_session_state = session.state
            
    @api.depends('session_ids')
    def _get_group_pos_order_taker(self):
        ir_model_data = self.env['ir.model.data']

        template_id = ir_model_data.get_object_reference('pos_brew92', 'group_pos_order_taker')
        if len(template_id)>=2:
            return template_id[1]
        else:
            return False
#        return self.env.ref('pos_brew92.group_pos_order_taker')
    
    current_session_id = fields.Many2one('pos.session', compute='_get_current_session', string="Current Session")
    current_session_state = fields.Char(compute='_get_current_session')
    group_pos_order_taker_id = fields.Many2one('res.groups', string='Point of Sale Order Taker',default=_get_group_pos_order_taker,
         help='This field is there to pass the id of the pos order taker to the point of sale client')

    #random check fields
    min_check = fields.Integer('Minimum Random Check')
    max_check = fields.Integer('Maximum Random Check')
    allowed_users = fields.Many2many('res.users','rand_check_user_rel','config_id','user_id', string='Allowed Users')
    cashier_printer_ip = fields.Char('cashier printer IP')


    #open pos session on clik of close button
    #To allow entering ending balance
    @api.multi
    def open_existing_session_cb_close(self):
        assert len(self.ids) == 1, "you can open only one session at a time"
        return self._open_session(self.current_session_id.id)

class pos_session(models.Model):
    _inherit = 'pos.session'

    shift_type = fields.Selection([
        ('morning', 'Morning Shift'),
        ('evening', 'Evening Shift'),
        ],'Shift Type',default="morning", required=True)
    supervisor_shift_id = fields.Many2one('supervisor.shift','Supervisor Shift')
    management_credit = fields.Float('Management Credit', readonly=True)
    customer_credit = fields.Float('Credit Card Sale', readonly=True)
    total_invoice = fields.Integer('Total Invoices', readonly=True)
    set_opening_balance = fields.Boolean('Set Opening Balance')
    set_closing_balance = fields.Boolean('Set Closing Balance')
    random_count = fields.Integer('Maximum Random Check', default=1)
    security_pin = fields.Char(string='Supervisor Password', related='supervisor_shift_id.security_pin', store=True)

    
    @api.multi
    def _get_pos_config_domain(self):
        user_id = self.env['res.users'].search([('id','=',self.env.user.id)])
        pos_config_ids = self.env['pos.config'].search([('id', '=', user_id.pos_config.id)])
        domain = [('state', '=', 'active'), ('id', 'in', pos_config_ids.id)]
        return domain

    config_id = fields.Many2one('pos.config', 'Point of Sale',
                                  help="The physical point of sale you   will use.",
                                  required=True,
                                  select=1,
                                  )

    @api.multi
    def _confirm_orders(self):
        for session in self:
            company_id = session.config_id.journal_id.company_id.id
            orders = session.order_ids.filtered(lambda order: order.state == 'paid')
            move = self.env['pos.order'].with_context(force_company=company_id)._create_account_move(session.start_at, session.name, session.config_id.journal_id.id, company_id)
            orders.with_context(force_company=company_id)._create_account_move_line(session, move)
            for order in session.order_ids.filtered(lambda o: o.state in ('paid', 'invoiced')):
                #allow user to close to session at time of shift handover
                
#                if order.state not in ('paid', 'invoiced'):
#                    raise UserError(_("You cannot confirm all orders of this session, because they have not the 'paid' status"))
                order.signal_workflow('done')

    def wkf_action_open(self):
        # second browse because we need to refetch the data from the DB for cash_register_id
        for session in self:
            if not session.set_opening_balance:
                raise UserError(_('Please set Openening Balance'))
            values = {}
            if not session.start_at:
                values['start_at'] = fields.Datetime.now()
            values['state'] = 'opened'
            session.write(values)
            session.statement_ids.button_open()
        return True

    def wkf_action_closing_control(self):
        for session in self:
            if not session.set_closing_balance:
                raise UserError(_('Please set Closing Balance'))
            orders = session.order_ids.filtered(lambda order: order.state == 'draft')
            if orders and session.shift_type=='evening':
                    raise UserError(_("There are still some unpaid orders. \n Please make payment for those orders as this is Evening Shift."))
            for statement in session.statement_ids:
                if (statement != session.cash_register_id) and (statement.balance_end != statement.balance_end_real):
                    statement.write({'balance_end_real': statement.balance_end})
            session.write({'state': 'closing_control', 'stop_at': fields.Datetime.now()})

    #Assign newly created session to draft orders
    @api.model
    def create(self, values):
        config_id = values.get('config_id') or self.env.context.get('default_config_id')
        pos_config = self.env['pos.config'].browse(config_id)
        ctx = dict(self.env.context, company_id=pos_config.company_id.id)
        uid = SUPERUSER_ID if self.env.user.has_group('point_of_sale.group_pos_user') else self.env.user.id
        #Code to check and assign open supervisor shift to this session
        supervisor_shift = self.env['supervisor.shift']
        open_shift_id = supervisor_shift.search([('state','=','opened')], limit=1)
        
        if not open_shift_id:
            raise UserError(_("Supervisor Shift is not opened. Please open supervisor shift before start sellling."))
        

        sessions = self.search([('supervisor_shift_id','=', open_shift_id.id),('config_id', '=', config_id)])
        curr_datetime = datetime.now()
        sup_shift_start = datetime.strptime(open_shift_id.shift_start_date, '%Y-%m-%d %H:%M:%S')

        diff = (curr_datetime - sup_shift_start).total_seconds()//3600
        if  diff >= 23:
            raise UserError(_("Please close yesterday's supervisor shift"))

        if sessions:
            shift_type = 'evening'
        else:
            shift_type = 'morning'
        values.update({
                 
                'supervisor_shift_id' : open_shift_id.id,
                'shift_type':shift_type
            })
        #End 
        session_id = super(pos_session, self.with_context(ctx).sudo(uid)).create(values)

        pos_order_obj = self.env['pos.order']
#         draft_order_ids = pos_order_obj.search([('state','=','draft'),('config_id','=',config_id)])
        draft_order_ids = pos_order_obj.sudo(SUPERUSER_ID).search([('state','=','draft'),('config_id','=',config_id)])
        for order in draft_order_ids:
#             order.session_id = session_id
            order.sudo(SUPERUSER_ID).write({'json_import':False,'session_id':session_id.id})
        return session_id


class pos_order(models.Model):
    _name = 'pos.order'
    
    _inherit = ['pos.order','mail.thread']

    def _default_session(self):
        return self.env['pos.session'].search([('state', '=', 'opened')], limit=1)

    def _default_pricelist(self):
        return self._default_session().config_id.pricelist_id

    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist', required=True, states={
                                   'draft': [('readonly', False)]}, readonly=True, default=_default_pricelist)
    amount_tax = fields.Float(compute='_compute_amount_all', string='Taxes', digits=0, track_visibility='always')
    amount_total = fields.Float(compute='_compute_amount_all', string='Total', digits=0, track_visibility='always')
    amount_paid = fields.Float(compute='_compute_amount_all', string='Paid', states={'draft': [('readonly', False)]}, readonly=True, digits=0, track_visibility='always')
    amount_return = fields.Float(compute='_compute_amount_all', string='Returned', digits=0, track_visibility='always')
    table_id = fields.Many2one('restaurant.table','Table', help='The table where this order was served',track_visibility='always')
    config_id = fields.Many2one('pos.config',related='session_id.config_id',store=True,string='Point of Sale', help='The table where this order was served',track_visibility='always')
#    config_id = fields.Many2one('pos.config',string='Point of Sale', help='The table where this order was served',track_visibility='always')
    json_object =  fields.Text('Json Data')
    json_import = fields.Boolean('Cashier Import')
    order_uid =  fields.Char('POS Uid')
    supervisor_shift_id = fields.Many2one('supervisor.shift',related='session_id.supervisor_shift_id', string='Supervisor Shift', store=True)

#    _sql_constraints = [('pos_reference_uniq', 'unique (pos_reference,user_id)', 'Order Reference Must be Unique !')]

#    Overriden to remove the user validation
     # This deals with orders that belong to a closed session. In order
    # to recover from this we:
    # - assign the order to another compatible open session
    # - if that doesn't exist, create a new one
    def _get_valid_session(self, order):
        PosSession = self.env['pos.session']
        closed_session = PosSession.browse(order['pos_session_id'])
        open_session = PosSession.search(
            [('state', '=', 'opened'),
             ('config_id', '=', closed_session.config_id.id)],
            limit=1, order="start_at DESC")

        _logger.warning('session %s (ID: %s) was closed but received order %s (total: %s) belonging to it',
                        closed_session.name,
                        closed_session.id,
                        order['name'],
                        order['amount_total'])

        if open_session:
            _logger.warning('using session %s (ID: %s) for order %s instead',
                            open_session.name,
                            open_session.id,
                            order['name'])
            return open_session
        else:
            return False

 

    @api.depends('statement_ids', 'lines.price_subtotal_incl', 'lines.discount')
    def _compute_amount_all(self):
        cur_obj = self.pool.get('res.currency')
        for order in self:
            order.amount_paid = order.amount_return = order.amount_tax = 0.0
            currency = order.pricelist_id.currency_id
            order.amount_paid = sum(payment.amount for payment in order.statement_ids)
            order.amount_return = sum(payment.amount < 0 and payment.amount or 0 for payment in order.statement_ids)
            order.amount_tax = currency.round(sum(self._amount_line_tax(line, order.fiscal_position_id) for line in order.lines))
            amount_untaxed = currency.round(sum(line.price_subtotal for line in order.lines))
            #Rounding for total (discounted values)
			# for line in order.lines:
            #     discount = True
            #     break
            # print 'discount', discount
            # if discount:
            order.amount_total = round(sum(line.price_subtotal_incl for line in order.lines))
            # else:
            # order.amount_total = order.amount_tax + amount_untaxed

#    @api.multi
#    def copy(self, default=None):
#        raise UserError(_('You are not allowed to duplicate an Order'))

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(pos_order, self)._order_fields(ui_order)
        order_fields['json_object'] = ui_order
        order_fields['order_uid'] = ui_order.get('uid',False)
        if self.user_has_groups('point_of_sale.group_pos_user'):
            order_fields['json_import'] = True
        else:
            order_fields['json_import'] = False

        order_fields['note'] = ui_order.get('note','')
        return order_fields

    def action_paid(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'paid'}, context=context)
        self.create_picking(cr, SUPERUSER_ID, ids, context=context)
        inv_ref = self.pool.get('account.invoice')
        for order in self.browse(cr, uid, ids, context):
            if order.partner_id:
                ctx = context.copy()
                ctx.update({'auto_invoice':True})
                inv_ids = self.action_invoice(cr, uid, order.id, context=ctx)
                inv_ref.signal_workflow(cr, SUPERUSER_ID, inv_ids, 'invoice_open')
                if order.partner_id.email:
                    inv_ref.send_invoice(cr, SUPERUSER_ID, inv_ids,ctx)
                order.partner_id.write({'last_order_id': order.id,'last_visit_date': order.date_order,'total_visits': order.partner_id.total_visits+1})
        return True

#    @api.multi
#    def action_paid(self):
#        super(pos_order,self).action_paid()
#        for order in self:
#            if self.partner_id:
#                order_cxt = dict(self.env.context, auto_invoice=True)
#                inv_ids = self.action_invoice()
#                inv_ids.signal_workflow('invoice_open')
#                if self.partner_id.email:
#                    inv_ids.with_context(order_cxt).send_invoice()
#                self.partner_id.write({'last_order_id': self.id,'last_visit_date': self.date_order,'total_visits': self.partner_id.total_visits+1})
#        return True
    
    def update_partner_profile(self, cr, uid, id):
        partner_ref = self.pool.get('res.partner')
        order_brw = self.browse(cr, uid, id)
        partner_ref.write(cr, uid, order_brw.partner_id.id, {'last_order_id':id,'last_visit_date':order_brw.date_order,'total_visits':order_brw.partner_id.total_visits+1})
        return True
    
    def action_invoice(self, cr, uid, ids, context=None):
        inv_ref = self.pool.get('account.invoice')
        inv_line_ref = self.pool.get('account.invoice.line')
        product_obj = self.pool.get('product.product')
        inv_ids = []

        for order in self.pool.get('pos.order').browse(cr, uid, ids, context=context):
            # Force company for all SUPERUSER_ID action
            company_id = order.company_id.id
            local_context = dict(context or {}, force_company=company_id, company_id=company_id)
            if order.invoice_id:
                inv_ids.append(order.invoice_id.id)
                continue

            if not order.partner_id:
                raise UserError(_('Please provide a partner for the sale.'))

            acc = order.partner_id.property_account_receivable_id.id
            #pass pos order reference in account_invoice
            inv = {
                'name': order.name,
                'origin': order.name,
                'account_id': acc,
                'journal_id': order.sale_journal.id or None,
                'type': 'out_invoice',
                'reference': order.pos_reference,
                'partner_id': order.partner_id.id,
#                'comment': order.note or '',
                'currency_id': order.pricelist_id.currency_id.id, # considering partner's sale pricelist's currency
                'company_id': company_id,
                'user_id': uid,
                'pos_order_id' : order.id,
            }
            invoice = inv_ref.new(cr, uid, inv)
            invoice._onchange_partner_id()
            invoice.fiscal_position_id = order.fiscal_position_id

            inv = invoice._convert_to_write(invoice._cache)
            if not inv.get('account_id', None):
                inv['account_id'] = acc
            inv_id = inv_ref.create(cr, SUPERUSER_ID, inv, context=local_context)

            self.write(cr, uid, [order.id], {'invoice_id': inv_id, 'state': 'invoiced'}, context=local_context)
            inv_ids.append(inv_id)
            for line in order.lines:
                inv_name = product_obj.name_get(cr, uid, [line.product_id.id], context=local_context)[0][1]
                inv_line = {
                    'invoice_id': inv_id,
                    'product_id': line.product_id.id,
                    'quantity': line.qty,
                    'account_analytic_id': self._prepare_analytic_account(cr, uid, line, context=local_context),
                    'name': inv_name,
                }

                #Oldlin trick
                invoice_line = inv_line_ref.new(cr, SUPERUSER_ID, inv_line, context=local_context)
                invoice_line._onchange_product_id()
                invoice_line.invoice_line_tax_ids = [tax.id for tax in invoice_line.invoice_line_tax_ids if tax.company_id.id == company_id]
                fiscal_position_id = line.order_id.fiscal_position_id
                if fiscal_position_id:
                    invoice_line.invoice_line_tax_ids = fiscal_position_id.map_tax(invoice_line.invoice_line_tax_ids)
                invoice_line.invoice_line_tax_ids = [tax.id for tax in invoice_line.invoice_line_tax_ids]
                # We convert a new id object back to a dictionary to write to bridge between old and new api
                inv_line = invoice_line._convert_to_write(invoice_line._cache)
                inv_line.update(price_unit=line.price_unit, discount=line.discount)
                inv_line_ref.create(cr, SUPERUSER_ID, inv_line, context=local_context)
            inv_ref.compute_taxes(cr, SUPERUSER_ID, [inv_id], context=local_context)
            self.signal_workflow(cr, uid, [order.id], 'invoice')
            inv_ref.signal_workflow(cr, SUPERUSER_ID, [inv_id], 'validate')

        if not inv_ids: return {}
        
        if context.get('auto_invoice',False):
            return inv_ids
        
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
        res_id = res and res[1] or False
        return {
            'name': _('Customer Invoice'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'account.invoice',
            'context': "{'type':'out_invoice'}",
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_id': inv_ids and inv_ids[0] or False,
        }

    #moving customized code from pos_restaurant
    @api.model
    def _process_order(self, pos_order,to_invoice=False):
        prec_acc = self.env['decimal.precision'].precision_get('Account')
        pos_session = self.env['pos.session'].browse(pos_order['pos_session_id'])
        if pos_session.state == 'closing_control' or pos_session.state == 'closed':
            pos_order['pos_session_id'] = self._get_valid_session(pos_order).id

        if not self.env.context.get('existing_order_id'):
            order = self.create(self._order_fields(pos_order))
        else:
            order = self.env.context.get('existing_order_id')
            order_cxt = dict(self.env.context, from_pos_interface=True)
            order.with_context(order_cxt).write(self._order_fields(pos_order))
#            return order_id
        journal_ids = set()
        #handling for zero amount
        #to be processed from cashier screen
        if pos_order.get('order_paid',False):

            if pos_order['statement_ids']:
                for payments in pos_order['statement_ids']:
                    if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
                        order.add_payment(self._payment_fields(payments[2]))
                    journal_ids.add(payments[2]['journal_id'])

                if pos_session.sequence_number <= pos_order['sequence_number']:
                    pos_session.write({'sequence_number': pos_order['sequence_number'] + 1})
                    pos_session.refresh()

                if not float_is_zero(pos_order['amount_return'], prec_acc):
                    cash_journal_id = pos_session.cash_journal_id.id
                    if not cash_journal_id:
                        # Select for change one of the cash journals used in this
                        # payment
                        cash_journal = self.env['account.journal'].search([
                            ('type', '=', 'cash'),
                            ('id', 'in', list(journal_ids)),
                        ], limit=1)
                        if not cash_journal:
                            # If none, select for change one of the cash journals of the POS
                            # This is used for example when a customer pays by credit card
                            # an amount higher than total amount of the order and gets cash back
                            cash_journal = [statement.journal_id for statement in pos_session.statement_ids if statement.journal_id.type == 'cash']
                            if not cash_journal:
                                raise UserError(_("No cash statement found for this session. Unable to record returned cash."))
                        cash_journal_id = cash_journal[0].id
                    order.add_payment({
                        'amount': -pos_order['amount_return'],
                        'payment_date': fields.Datetime.now(),
                        'payment_name': _('return'),
                        'journal': cash_journal_id,
                    })
            try:
                order.signal_workflow('paid')
#                order.state = 'paid'
            except psycopg2.OperationalError:
                # do not hide transactional errors, the order(s) won't be saved!
                raise
            except Exception as e:
                _logger.error('Could not fully process the POS Order: %s', tools.ustr(e))

            if to_invoice:
                order.action_invoice()
                order.invoice_id.sudo().signal_workflow('invoice_open')

        return order

    def add_payment(self, data):
        """Create a new payment for the order"""
        args = {
            'amount': data['amount'],
            'date': data.get('payment_date', fields.Date.today()),
            'name': self.name + ': ' + (data.get('payment_name', '') or ''),
            'partner_id': self.env["res.partner"]._find_accounting_partner(self.partner_id).id or False,
        }

        journal_id = data.get('journal', False)
        statement_id = data.get('statement_id', False)
        assert journal_id or statement_id, "No statement_id or journal_id passed to the method!"

        journal = self.env['account.journal'].browse(journal_id)
        # use the company of the journal and not of the current user
        company_cxt = dict(self.env.context, force_company=journal.company_id.id)
        account_def = self.env['ir.property'].with_context(company_cxt).get('property_account_receivable_id', 'res.partner')
        args['account_id'] = (self.partner_id.property_account_receivable_id.id) or (account_def and account_def.id) or False

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (
                    self.partner_id.name, self.partner_id.id,)
            raise UserError(msg)

        context = dict(self.env.context)
        context.pop('pos_session_id', False)
        for statement in self.session_id.statement_ids:
            if statement.id == statement_id:
                journal_id = statement.journal_id.id
                break
            elif statement.journal_id.id == journal_id:
                statement_id = statement.id
                break
        if not statement_id:
            raise UserError(_('You have to open at least one cashbox.'))

        args.update({
            'statement_id': statement_id,
            'pos_statement_id': self.id,
            'journal_id': journal_id,
            'ref': self.session_id.name,
        })
        self.env['account.bank.statement.line'].with_context(context).create(args)
        return statement_id

    #trying to process order in multi threaded environment
    @api.multi
    def process_order_thread(self, tmp_order_update, lock, queue):
        with api.Environment.manage():
            to_invoice = tmp_order_update['to_invoice']
            order = tmp_order_update['data']
            new_cr = registry(self._cr.dbname).cursor()
            self = self.with_env(self.env(cr=new_cr))
            lock.acquire()
            try:
                if to_invoice:
                    self._match_payment_to_invoice(order)
                #Search Failed: when multiple request processed simultanously
                #For processing one it takes time.
                #Till that time another request of same order is made, it doesn't give valid result
                update_orderid = self.search([('pos_reference','=',order['name'])], limit=1)

                if not update_orderid:
                    update_orderid = self._process_order(order, to_invoice=to_invoice)
                else:
                    if update_orderid.state not in ('paid','done','invoiced'):
                        order_cxt = dict(self.env.context, existing_order_id=update_orderid)
                        self.with_context(order_cxt)._process_order(order)
                new_cr.commit()
                queue.put(update_orderid.id)
                return update_orderid
            except Exception, e:
                _logger.info('Attempt to create POS order is aborted: %s', str(e))
                new_cr.rollback()
            finally:
                new_cr.close()
                lock.release()
            return False
    
    
    #function is modified for handling multiple threads
    #create_from_ui was creating duplicate orders as multiple requests were processed simultaneously
    #At an instance, this processes only one
    @api.model
    def create_from_ui(self, orders):
        order_ids = []

        for tmp_order_update in orders:
            
            my_queue = Queue.Queue()
#            t = threading.Thread(target=process_order_thread, args=(tmp_order_update))
            threaded_calculation = threading.Thread(target=self.process_order_thread, args=(tmp_order_update,lock,my_queue))
            threaded_calculation.start()
            threaded_calculation.join()
            update_orderid = my_queue.get()
            order_ids.append(update_orderid)
        return order_ids


    @api.multi
    def write(self, vals):
        if self.env.context.get('from_pos_interface', False):
            #commented code was put to handle bill splitting
            new_lines = []
#            older_lines = self.read(cr, uid, ids, ['lines'], context=context)[0]['lines']
            if vals.get('name',False):
                del vals['name']

            order_line_obj = self.env['pos.order.line']
            if vals.get('lines',False):
                for line in vals['lines']:
                    if line[2] and line[2].get('pos_id',False):
                        existing_line = order_line_obj.search([('order_id','=',self.id),('pos_id','=',line[2]['pos_id'])])
                        if existing_line:
                            existing_line.with_context(self.env.context).write(line[2])
#                            if existing_lines[0] in older_lines:
#                                older_lines.remove(existing_lines[0])
                        else:
                            new_lines.append(line)
                    else:
                        new_lines.append(line)
            vals['lines'] = new_lines
        res = super(pos_order, self).write(vals)
        Partner = self.env['res.partner']
        # If you change the partner of the PoS order, change also the partner of the associated bank statement lines
        if 'partner_id' in vals:
            for order in self:
                partner_id = False
                if order.invoice_id:
                    raise UserError(_("You cannot change the partner of a POS order for which an invoice has already been issued."))
                if vals['partner_id']:
                    partner = Partner.browse(vals['partner_id'])
                    partner_id = Partner._find_accounting_partner(partner).id
                order.statement_ids.write({'partner_id': partner_id})
        return res

    def print_bill(self, cr, uid, ids, context=None):

        for order in self.browse(cr, uid, ids, context):
            proxy_ip = order.session_id.config_id.proxy_ip or False
            if proxy_ip:
                url = 'http://'+proxy_ip+':8069/hw_proxy/print_xml_receipt'
                receipt = """<receipt align="center" value-thousands-separator="" width="40">"""
               
                receipt_header = """ """
                total_discount = 0.0
                void_transaction = 0.0
                #logo is not getting printed
#                if order.company_id.logo:
#                    receipt_header = """
#                                        <img src= "data:image/png;base64,%s" /> <br/>""" %(order.company_id.logo)
#                else:
                receipt_header = """<h1>%s </h1> <br/>"""%(order.company_id.name)
                receipt_header = receipt_header + """
                                    <div font="b">
                                """
#                if order.location_id:
#                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.location_id.name)
                if order.company_id.street:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.company_id.street)
                if order.company_id.vat:
                    receipt_header = receipt_header + """ <div>VAT: %s</div>"""%(order.company_id.vat)
                if order.company_id.email:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.company_id.email)

                if order.company_id.website:
                    receipt_header = receipt_header + """ <div>%s</div>"""%(order.company_id.website)
                if order.user_id:
                    receipt_header = receipt_header + """

                                        <div class="cashier">
                                            <div>--------------------------------</div>
                                            <div>Served by %s</div>"""%(order.user_id.name)
                    if order.table_id:
                        receipt_header = receipt_header + """
                                            at table %s
                                            <div>Guests: %s</div>"""%(order.table_id.name,order.customer_count)
                    receipt_header = receipt_header + """
                        </div>

                        </div>
                        <br/><br/>
                """

                receipt_line = """<div class="orderlines" line-ratio="0.6">"""
                for line in order.lines:
                    simple = True
                    if line.discount != 0.0 or line.qty != 1.000 :
                        simple = False
                    if simple:
                        #replaced price_subtotal with price_subtotal_incl to print price without tax @Irfan 12dec17
                        receipt_line  = receipt_line + """
                                            <line>
                                                <left>%s</left>
                                                <right><value>%s</value></right>
                                            </line>"""%(line.product_id.name,line.price_subtotal_incl)
                    else:
                        total_discount += (line.price_unit*line.discount/100)*line.qty
                        # total_discount += (line.price_unit*line.qty) - round(line.price_subtotal_incl)
                        print total_discount
                        
                        receipt_line  = receipt_line + """
                                            <line>
                                                <left>%s</left>
                                            </line>"""%(line.product_id.name)
                        if line.discount!=0.0:
                            receipt_line  = receipt_line + """
                                            <line indent='1'><left>Discount: %s%%</left></line>"""%(line.discount)
                        receipt_line  = receipt_line + """
                                                <line indent='1'>
                                                    <left>
                                                        <value value-decimals='3' value-autoint='on'>
                                                            %s
                                                        </value>

                                                        x
                                                        <value value-decimals='2'>
                                                            %s
                                                        </value>
                                                    </left>
                                                    <right>
                                                        <value>%s</value>
                                                    </right>
                                            </line>"""%(line.qty,line.price_unit,line.price_subtotal_incl)
                                            
                                            
                    if line.product_id.ar_display_image and line.tax_ids:
                        receipt_line  = receipt_line + """
                        <line>
                                            <left><img src= "data:image/png;base64,%s" /></left>
                                            <right>VAT(%.0f%%)</right>
                                            </line>"""%(line.product_id.ar_display_image,line.tax_ids.amount)
                   
                        
                receipt_line  = receipt_line + """</div>"""

                receipt_total = """ <line><right>--------</right></line>
                                    <line class="total">
                                        <left>TOTAL Excl. VAT </left>
                                        <right><value>%s</value></right>
                                    </line>
                                    <line><left><img src="data:image/png;base64,%s" /></left></line>
                                    """%((order.amount_total - order.amount_tax),order.company_id.vat_ar_image)

                receipt_total += """ <line class="total">
                                        <left>VAT </left>
                                        <right><value>%s</value></right>
                                    </line>
                                    
                                    """%(order.amount_tax)

                receipt_total += """<line><right>--------</right></line>
                                    <line class="total" size="double-height">
                                        <left><pre>TOTAL Incl. VAT </pre></left>
                                        <right><value>%s</value></right>
                                    </line>
                                    <line>
                                        <left><img t-att-src="'data:image/png;base64,' + receipt.total_vat_ar"  t-att-alt="total_with_vat" /></left>
                                    </line>
                                    <br/>"""%(order.amount_total)
                receipt_payment =""""""
                receipt_change = """"""
                if order.statement_ids:
                    for statement in order.statement_ids:

                        if statement.amount > 0.0:
                            receipt_payment  = receipt_payment + """
                                            <line>
                                                <left>%s</left>
                                                <right><value>%s</value></right>
                                            </line>
                                            <br/>
                                            """%(statement.journal_id.name_get()[0][1],statement.amount)
                    receipt_change = """
                                        <line size="double-height">
                                            <left><pre> CHANGE</pre></left>
                                            <right><value>%s</value></right>
                                        </line>
                                        <br/>

                                        <div class="before-footer"></div>"""%(abs(order.amount_return))
                discount_info = """"""""
                if total_discount!=0.0:
                    discount_info = """"<line>
                            <left>Discounts</left>
                            <right><value>%s</value></right>
                        </line>"""%(total_discount)
                        
                footer = """
                        <br/>
                        <br/>
                        <div font='b'>
                            <div>%s</div>
                        </div>
                        <div font='b'>
                                <div>Thank You For Visiting</div>
                                <div>Please Come Again</div>
                            </div>"""%(datetime.now().strftime('%Y-%m-%d %H:%M%S'))

                receipt = receipt + receipt_header + receipt_line + receipt_total +receipt_payment+ receipt_change + discount_info + footer +"""</receipt>"""
#                print"receipt......",receipt
                
                try:
                    driver = EscposDriver()
                    driver.push_task('xml_receipt',receipt,network_ip='')

                except Exception as e:
                    raise UserError(_(e))
        return True

    @api.one
    def print_kitchen_order(self):
        proxy_printers = self.session_id.config_id.printer_ids or []
        for printer in proxy_printers:
            orderlines = self.lines.filtered(lambda r: r.product_id.pos_categ_id in printer.product_categories_ids)
            network_ip = printer.network_ip
            if network_ip:
                url = 'http://'+network_ip+':8069/hw_proxy/print_xml_receipt'
                
                receipt = """<receipt
                            align='center'
                            width='40'
                            size='double-height'
                            line-ratio='0.4'
                            value-decimals='3'
                            value-thousands-separator=''
                            value-autoint='on'
                            >"""
                receipt_header = """"""
                if self.table_id:
                    receipt_header = """
                    <div size='normal' >%s</div>
                            <br />
                            <div><span>%s</span> / <span bold='on' size='double'>%s</span> </div>
                            <div>
                                <span>%s</span>
                            </div>
                            <div><span>%s</span></div>
                        <br />
                        <br />"""%(self.name,self.table_id.floor_id.name,self.table_id.name,self.user_id and self.user_id.name or '',self.partner_id.name or '')
                for line in orderlines:
                    receipt_header += """
                    <line >
                        <right><value>%s</value></right>
                            <right><div>%s</div></right>
                    </line>"""%(line.qty,line.product_id.name)
                    if line.note:
                        receipt_header += """
                        <line size='normal'>
                            <right>-----------NOTE----------</right>
                        </line> """
                        for note in line.note.split('\n'):
                            receipt_header += """
                        <right><span size='normal' bold='off' line-ratio='0.4' align="right" indent='1' width='30'>
                                	%s<br />
                        </span></right>
                        <line></line>"""%(note)
                    receipt_header+="""
                    <div><span> -----------------------------------------</span></div>
                    """

             

                receipt = receipt + receipt_header +"""</receipt>"""
#                print"receipt......",receipt
                data = {
                    "jsonrpc": "2.0",
#                    "params": {"receipt": u'<receipt align="center" font="a" value-thousands-separator="," width="30"><h1>Company</h1><br/><div font="a"><br/>' + \
#                                            u'</div></receipt>',
                    "params": {"receipt":  receipt,
#                                "order": order,
#                                "orderlines":order.lines
                                            },
                }
            try:
                driver = EscposDriver()
                driver.push_task('xml_receipt',receipt,network_ip='')
                time.sleep(5)
            except Exception as e:
                raise UserError(_(e))
        return True


class pos_order_line(models.Model):
    _inherit = "pos.order.line"

    line_detail = fields.One2many('pos.order.line.detail', 'line_id', 'Order Line Details', readonly=True, copy=True)
    void_reason = fields.Char('Void Reason')

    @api.model
    def create(self, values):
        product_id = values.get('product_id',False)
        order_qty = values.get('qty',False)
        if product_id:
            product_brw = self.env['product.product'].browse(product_id)
            default_choice_items = self.env['product.bom.detail'].search([('parent_id','=',product_brw.product_tmpl_id.id),('has_choice','=',False)])
            if default_choice_items:
                values['line_detail'] = values.get('line_detail',[])
                for item in default_choice_items:
                    values['line_detail'].append([0,0,{'product_id':item.product_id.id,'qty':item.qty}])
#                 for line in values['line_detail']:
#                     if line[2] and line[2].get('qty', False):
#                         line[2]['qty']=  line[2]['qty']*order_qty
        return super(pos_order_line, self).create(values)

    @api.multi
    def write(self,values):
        if self.env.context.get('from_pos_interface', False):
            if values.get('line_detail',False):
                del values['line_detail']
        return super(pos_order_line, self).write(values)

    @api.depends('price_unit', 'tax_ids', 'qty', 'discount', 'product_id')
    def _compute_amount_line_all(self):
        for line in self:
            currency = line.order_id.pricelist_id.currency_id
            taxes = line.tax_ids.filtered(lambda tax: tax.company_id.id == line.order_id.company_id.id)
            fiscal_position_id = line.order_id.fiscal_position_id
            if fiscal_position_id:
                taxes = fiscal_position_id.map_tax(taxes)
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            line.price_subtotal = line.price_subtotal_incl = price * line.qty
            if taxes:
                taxes = taxes.compute_all(price, currency, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)
                line.price_subtotal = taxes['total_excluded']
                line.price_subtotal_incl = taxes['total_included']

            line.price_subtotal = currency.round(line.price_subtotal)
            line.price_subtotal_incl = round(line.price_subtotal_incl)

class pos_order_line_detail(models.Model):
    _name = "pos.order.line.detail"

    product_id =  fields.Many2one('product.template', 'Product', change_default=True)
    qty = fields.Float('Quantity')
    line_id = fields.Many2one('pos.order.line', 'Order Line Ref', ondelete='cascade')    
