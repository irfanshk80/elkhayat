# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp import api, fields, models, _, SUPERUSER_ID

class restaurant_floor(models.Model):
    _inherit = 'restaurant.floor'

    network_ip = fields.Char('Cash Printer IP')
    no_cash = fields.Boolean('No Cash Payment')

