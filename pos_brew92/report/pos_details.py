# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta
import pytz
import time
from openerp import tools
from openerp.osv import osv
from openerp.report import report_sxw
import collections
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class pos_details_inherit(report_sxw.rml_parse):

    def _get_invoice(self, inv_id):
        res = {}
        if inv_id:
            self.cr.execute("select number from account_invoice as ac where id = %s", (inv_id,))
            res = self.cr.fetchone()
            return res[0] or 'Draft'
        else:
            return ''

    def _get_all_users(self):
        user_obj = self.pool.get('res.users')
        return user_obj.search(self.cr, self.uid, [])

    def append_zero(self, hour):
        if len(hour) == 1:
            return '0' + '' + hour
        return hour

    def get_time_slot(self, hour):
        star_time = hour
        end_time = hour + 1
        if end_time == 24:
            end_time = 0
        return self.append_zero(str(star_time)) + '-' + self.append_zero(str(end_time))

    def _get_utc_time_range(self, form):
        print "form", form
        user = self.pool['res.users'].browse(self.cr, self.uid, self.uid)
        tz_name = user.tz or self.localcontext.get('tz') or 'UTC'
        user_tz = pytz.timezone(tz_name)
        between_dates = {}
        #        for date_field, delta in {'date_start': {'days': 0}, 'date_end': {'days': 1}}.items():
        #            timestamp = datetime.datetime.strptime(form[date_field] , tools.DEFAULT_SERVER_DATETIME_FORMAT) #+ datetime.timedelta(**delta)
        ##            timestamp = user_tz.localize(timestamp).astimezone(pytz.utc)
        #            between_dates[date_field] = timestamp.strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)
        return form['date_start'], form['date_end']

    def _pos_sales_details(self, form, supervisor_shift_id=0, context={}):
        cr, uid = self.cr, self.uid
        pos_obj = self.pool.get('pos.order')
        user_obj = self.pool.get('res.users')

        user = self.pool['res.users'].browse(cr, uid, uid)
        tz_name = user.tz or self.localcontext.get('tz') or 'UTC'
        user_tz = pytz.timezone(tz_name)
        data = []
        result = {}
        hourly_data = {}
        user_ids = form['user_ids'] or self._get_all_users()
        print "super_visor shift", supervisor_shift_id
        company_id = user_obj.browse(cr, uid, uid).company_id.id
        date_start, date_end = self._get_utc_time_range(form)
        order_count = 0
        if supervisor_shift_id:
            pos_ids = pos_obj.search(cr, uid, [
                ('supervisor_shift_id', '=', supervisor_shift_id),
                ('state', 'in', ['paid', 'invoiced', 'done'])
            ])
        else:
            pos_ids = pos_obj.search(cr, uid, [
                ('date_order', '>=', date_start),
                ('date_order', '<', date_end),
                ('user_id', 'in', user_ids),
                ('state', 'in', ['done', 'paid', 'invoiced']),
                ('company_id', '=', company_id)
            ])
        for pos in pos_obj.browse(cr, uid, pos_ids, context=self.localcontext):
            for pol in pos.lines:
                result = {
                    'code': pol.product_id.default_code,
                    'name': pol.product_id.name,
                    'invoice_id': pos.invoice_id.id,
                    'price_unit': pol.price_unit,
                    'qty': pol.qty,
                    'discount': pol.discount,
                    'total': (pol.price_unit * pol.qty * (1 - (pol.discount) / 100.0)),
                    'date_order': pos.date_order,
                    'pos_name': pos.name,
                    'uom': pol.product_id.uom_id.name
                }
                data.append(result)
                self.qty += result['qty']
                self.discount += result['discount']
                self.total_discount += ((pol.price_unit * pol.qty) * (pol.discount / 100))
                if pol.qty == 0.0:
                    self.total_void += pol.price_unit
            self.total += pos.amount_total
            self.total_guests += pos.customer_count
            order_count += 1
            # get hourly data
            timestamp = datetime.strptime(pos.date_order, tools.DEFAULT_SERVER_DATETIME_FORMAT)
            timestamp = pytz.utc.localize(timestamp).astimezone(user_tz)
            order_date = timestamp.strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)

            order_hour = int(order_date[11:13])
            time_slot = self.get_time_slot(order_hour)
            if time_slot in hourly_data:
                hourly_data[time_slot] += pos.amount_total
            else:
                hourly_data.update({time_slot: pos.amount_total})
        #            print "hourly_data",hourly_data
        self.total_orders = order_count
        # deduct management credit from net sale total
        mgmt_total = 0.0
        for payment in self._get_payments(form, supervisor_shift_id):
            if payment['name'] == 'Credit Management':
                mgmt_total += payment['sum']
        self.net_total['net_total'] = self.total - mgmt_total
        self.net_total['mgmt'] = mgmt_total
        # process hourly data
        data = sorted(hourly_data.items())

        # get monthly sales
        date_start = date_start[:8] + '01' + date_start[10:]
        form_my = form.copy()
        form_my.update({'date_start': date_start})
        mgmt_total = 0.0
        for payment in self._get_payments(form_my, supervisor_shift_id):
            if payment['name'] == 'Credit Management':
                mgmt_total += payment['sum']

        args = {'date_start': date_start, 'date_end': date_end}
        query_month = """select sum((1-line.discount/100) * line.price_unit * line.qty) as disc
                        from pos_order o, pos_order_line line
                        where line.order_id = o.id and o.supervisor_shift_id>5 and o.date_order >= %(date_start)s and o.date_order <= %(date_end)s"""
        cr.execute(query_month, args)
        self.monthly_sale = cr.fetchone()[0] or 0
        self.monthly_sale = round(self.monthly_sale) - mgmt_total or 0

        # get accumulated sales
        form_my['form_year'] = True
        mgmt_total = 0.0
        for payment in self._get_payments(form_my, supervisor_shift_id):
            if payment['name'] == 'Credit Management':
                mgmt_total += payment['sum']
        args = {'end_date': date_end}
        accumulated_query = """select sum((1-line.discount/100) * line.price_unit * line.qty) as disc
                        from pos_order o, pos_order_line line
                        where line.order_id = o.id and o.supervisor_shift_id>5 and o.date_order <= %(end_date)s"""
        cr.execute(accumulated_query, args)
        self.accumulated_sale = cr.fetchone()[0] or 0
        self.accumulated_sale = round(self.accumulated_sale) - mgmt_total or 0

        #        month_filter = [('date_order', '>=', date_start),('date_order', '<', date_end),('user_id', 'in', user_ids),('state', 'in', ['done', 'paid', 'invoiced']),
        #            ('company_id', '=', company_id)
        #        ]
        #        pos_ids = pos_obj.search(cr, uid, month_filter)
        #        self.monthly_sale = sum([order.amount_total for order in pos_obj.browse(cr, uid, pos_ids)])

        # get total sale since begining
        #        total_filter = [('date_order', '<', date_end),('user_id', 'in', user_ids),('state', 'in', ['done', 'paid', 'invoiced']),
        #            ('company_id', '=', company_id)
        #        ]
        #        pos_ids = pos_obj.search(cr, uid, total_filter)
        #        self.accumulated_sale = sum([order.amount_total for order in pos_obj.browse(cr, uid, pos_ids)])
        if data:
            return data
        else:
            return {}

    def _get_qty_total_2(self):
        return self.total_orders

    def _get_sales_total_2(self):
        return self.total

    def _get_net_total(self):
        return self.net_total

    def _get_total_guests(self):
        return self.total_guests

    def _get_monthly_sales(self):
        return self.monthly_sale

    def _get_accumulated_sales(self):
        return self.accumulated_sale

    def _get_sum_invoice_2(self, form, supervisor_shift_id=0):
        pos_obj = self.pool.get('pos.order')
        user_obj = self.pool.get('res.users')
        user_ids = form['user_ids'] or self._get_all_users()
        company_id = user_obj.browse(self.cr, self.uid, self.uid).company_id.id
        date_start, date_end = self._get_utc_time_range(form)
        if supervisor_shift_id:

            pos_ids = pos_obj.search(self.cr, self.uid,
                                     [('supervisor_shift_id', '=', supervisor_shift_id), ('invoice_id', '<>', False)])
        else:
            pos_ids = pos_obj.search(self.cr, self.uid,
                                     [('date_order', '>=', date_start), ('date_order', '<=', date_end),
                                      ('user_id', 'in', user_ids), ('company_id', '=', company_id),
                                      ('invoice_id', '<>', False)])

        for pos in pos_obj.browse(self.cr, self.uid, pos_ids):
            for pol in pos.lines:
                self.total_invoiced += (pol.price_unit * pol.qty * (1 - (pol.discount) / 100.0))
        return self.total_invoiced or False

    def _paid_total_2(self):
        return self.total or 0.0

    def _get_sum_dis_2(self):
        return self.discount or 0.0

    def _get_void_total(self):
        return self.total_void or 0.0

    def _get_sum_discount(self):
        # code for the sum of discount value
        #        pos_obj = self.pool.get('pos.order')
        #        user_obj = self.pool.get('res.users')
        #        user_ids = form['user_ids'] or self._get_all_users()
        #        company_id = user_obj.browse(self.cr, self.uid, self.uid).company_id.id
        #        date_start, date_end = self._get_utc_time_range(form)
        #        pos_ids = pos_obj.search(self.cr, self.uid, [('date_order','>=',date_start),('date_order','<=',date_end),('user_id','in',user_ids),('company_id','=',company_id)])
        #        for pos in pos_obj.browse(self.cr, self.uid, pos_ids):
        #            print "Order Name",pos.name
        #            for pol in pos.lines:
        #                self.total_discount += ((pol.price_unit * pol.qty) * (pol.discount / 100))
        #                print "self.total_discount",self.total_discount
        return self.total_discount or False

    def _get_payments(self, form, supervisor_shift_id=0):
        statement_line_obj = self.pool.get("account.bank.statement.line")
        pos_order_obj = self.pool.get("pos.order")
        user_ids = form['user_ids'] or self._get_all_users()
        company_id = self.pool['res.users'].browse(self.cr, self.uid, self.uid).company_id.id
        date_start, date_end = self._get_utc_time_range(form)
        if supervisor_shift_id:
            pos_ids = pos_order_obj.search(self.cr, self.uid, [('supervisor_shift_id', '=', supervisor_shift_id),
                                                               ('state', 'in', ['paid', 'invoiced', 'done'])])
        else:
            if form.has_key('form_year'):
                pos_ids = pos_order_obj.search(self.cr, self.uid, [('date_order', '<=', date_end),
                                                                   ('state', 'in', ['paid', 'invoiced', 'done']),
                                                                   ('user_id', 'in', user_ids),
                                                                   ('company_id', '=', company_id)])
            else:
                pos_ids = pos_order_obj.search(self.cr, self.uid,
                                               [('date_order', '>=', date_start), ('date_order', '<=', date_end),
                                                ('state', 'in', ['paid', 'invoiced', 'done']),
                                                ('user_id', 'in', user_ids), ('company_id', '=', company_id)])

        data = {}
        if pos_ids:
            st_line_ids = statement_line_obj.search(self.cr, self.uid, [('pos_statement_id', 'in', pos_ids)])
            if st_line_ids:
                st_id = statement_line_obj.browse(self.cr, self.uid, st_line_ids)
                a_l = []
                for r in st_id:
                    a_l.append(r['id'])
                self.cr.execute(
                    "select aj.name,sum(amount) from account_bank_statement_line as absl,account_bank_statement as abs,account_journal as aj " \
                    "where absl.statement_id = abs.id and abs.journal_id = aj.id  and absl.id IN %s " \
                    "group by aj.name ", (tuple(a_l),))

                data = self.cr.dictfetchall()
                return data
        else:
            return {}

    def _total_of_the_day(self, objects):
        return self.total or 0.00

    def _sum_invoice(self, objects):
        return reduce(lambda acc, obj:
                      acc + obj.invoice_id.amount_total,
                      [o for o in objects if o.invoice_id and o.invoice_id.number],
                      0.0)

    def _ellipsis(self, orig_str, maxlen=100, ellipsis='...'):
        maxlen = maxlen - len(ellipsis)
        if maxlen <= 0:
            maxlen = 1
        new_str = orig_str[:maxlen]
        return new_str

    def _strip_name(self, name, maxlen=50):
        return self._ellipsis(name, maxlen, ' ...')

    def _get_tax_amount(self, form, supervisor_shift_id=0):
        taxes = {}
        account_tax_obj = self.pool.get('account.tax')
        user_ids = form['user_ids'] or self._get_all_users()
        pos_order_obj = self.pool.get('pos.order')
        company_id = self.pool['res.users'].browse(self.cr, self.uid, self.uid).company_id.id
        date_start, date_end = self._get_utc_time_range(form)
        if supervisor_shift_id:
            pos_ids = pos_order_obj.search(self.cr, self.uid, [('supervisor_shift_id', '=', supervisor_shift_id),
                                                               ('state', 'in', ['paid', 'invoiced', 'done'])])
        else:
            pos_ids = pos_order_obj.search(self.cr, self.uid,
                                           [('date_order', '>=', date_start), ('date_order', '<=', date_end),
                                            ('state', 'in', ['paid', 'invoiced', 'done']), ('user_id', 'in', user_ids),
                                            ('company_id', '=', company_id)])
        for order in pos_order_obj.browse(self.cr, self.uid, pos_ids):
            currency = order.session_id.currency_id
            for line in order.lines:
                if line.tax_ids_after_fiscal_position:
                    line_taxes = line.tax_ids_after_fiscal_position.compute_all(
                        line.price_unit * (1 - (line.discount or 0.0) / 100.0), currency, line.qty,
                        product=line.product_id, partner=line.order_id.partner_id or False)
                    for tax in line_taxes['taxes']:
                        taxes.setdefault(tax['id'], {'name': tax['name'], 'amount': 0.0})
                        taxes[tax['id']]['amount'] += tax['amount']
        return taxes.values()

    def _get_user_names(self, user_ids):
        user_obj = self.pool.get('res.users')
        return ', '.join(map(lambda x: x.name, user_obj.browse(self.cr, self.uid, user_ids)))

    def __init__(self, cr, uid, name, context):
        super(pos_details_inherit, self).__init__(cr, uid, name, context=context)
        self.net_total = {}
        self.total = 0.0
        self.qty = 0.0
        self.total_invoiced = 0.0
        self.total_void = 0.0
        self.discount = 0.0
        self.total_discount = 0.0
        self.total_guests = 0
        self.total_orders = 0
        self.monthly_sale = 0
        self.accumulated_sale = 0
        self.localcontext.update({
            'time': time,
            'strip_name': self._strip_name,
            'getpayments': self._get_payments,
            'getsumdisc': self._get_sum_discount,
            'gettotaloftheday': self._total_of_the_day,
            'gettaxamount': self._get_tax_amount,
            'pos_sales_details': self._pos_sales_details,
            'getqtytotal2': self._get_qty_total_2,
            'getvoidtotal': self._get_void_total,
            'getsalestotal2': self._get_sales_total_2,
            'get_net_total': self._get_net_total,
            'getsuminvoice2': self._get_sum_invoice_2,
            'getpaidtotal2': self._paid_total_2,
            'getinvoice': self._get_invoice,
            'get_user_names': self._get_user_names,
            'get_total_guests': self._get_total_guests,
            'get_monthly_sales': self._get_monthly_sales,
            'get_accumulated_sales': self._get_accumulated_sales,
        })


class report_pos_details(osv.AbstractModel):
    _name = 'report.pos_brew92.report_detailsofsales_custom'
    _inherit = 'report.abstract_report'
    _template = 'pos_brew92.report_detailsofsales_custom'
    _wrapped_report_class = pos_details_inherit