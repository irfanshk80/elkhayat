# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from openerp.osv import osv
from openerp.report import report_sxw

class supervisor_report(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(supervisor_report, self).__init__(cr, uid, name, context=context)


class report_supervisor_receipt(osv.AbstractModel):
    _name = 'report.pos_brew92.report_supervisor_receipt'
    _inherit = 'report.abstract_report'
    _template = 'pos_brew92.report_supervisor_receipt'
    _wrapped_report_class = supervisor_report
