# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Restaurant Brew92',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 6,
    'summary': 'Restaurant extensions for the Point of Sale ',
    'description': """
Quick and Easy Restaurant Management.
=====================================

This module is developed to provide additional features for restaturants module. 

It supports following feautres:
-------------------------------
    * Choice Item selection
    * Bill Printing based on Floor
    * Waiter Cashier and Supervisor hierarchy

End of module description

    """,
    'depends': ['pos_restaurant','pos_connect_ecw'],
    'website': 'https://www.odoo.com/page/point-of-sale',
    'data': [
        'security/point_of_sale_security.xml',
        'security/ir.model.access.csv',
        'views/point_of_sale_dashboard.xml',
        'views/change_pos_payment_view.xml',
        'wizard/random_check_wizard_view.xml',
        'views/point_of_sale_view.xml',
        'views/templates.xml',
        'views/product_view.xml',
        'views/report_detailsofsales.xml',
        'views/report_pos.xml',
        'views/point_of_sale_report.xml',
        'views/send_mail_data.xml',
        'views/ir_sequence_data.xml',
        'views/res_users_view.xml',
        'views/supervisor_shift_view.xml',
        'views/supervisor_shift_data.xml',
#        'views/point_of_sale_view.xml',
#        'wizard/pos_box.xml',
        'views/res_partner_view.xml',
        'views/account_bank_statement.xml',
        'views/report_supervisor_receipt.xml',
        'views/report_random_check.xml',
        'views/report_invoice.xml',
        'views/company_vat_view.xml'
#        'data/account_data.xml'
    ],
    'qweb':[
        'static/src/xml/pos.xml'
    ],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
}
