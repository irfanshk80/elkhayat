# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

import logging
import werkzeug.utils

from openerp.addons.point_of_sale.controllers.main import PosController

from openerp import http
from openerp.http import request
from openerp.tools.translate import _

from openerp import SUPERUSER_ID

import ast

_logger = logging.getLogger(__name__)

class PosController(PosController):

    @http.route('/pos/web', type='http', auth='user')
    def a(self, debug=False, **k):
        cr, uid, context, session = request.cr, request.uid, request.context, request.session

        # if user not logged in, log him in
        PosSession = request.registry['pos.session']
        #Remove restriction of sinlge session per user
        pos_session_ids = PosSession.search(cr, uid, [('state','=','opened')], context=context)

        if not pos_session_ids:
            return werkzeug.utils.redirect('/web#action=point_of_sale.action_client_pos_menu')
        PosSession.login(cr, uid, pos_session_ids, context=context)

        return request.render('point_of_sale.index')

    @http.route('/pos_brew92/check_payment_status', type='json', auth='user', cors='*')
    def check_payment_status(self, **k):
        cr, uid, context, session = request.cr, request.uid, request.context, request.session
        
        PosOrder = request.registry['pos.order']
        pos_order_ids = PosOrder.search(cr, uid, [('pos_reference','=',k.get('name'))], context=context)
        if pos_order_ids:
            order_data = PosOrder.browse(cr, uid, pos_order_ids[0])
            if order_data.state in ('paid', 'invoiced','done'):
                return True
            else:
                return False
        return False


    @http.route('/pos_brew92/import_unpaid_order', type='json', auth='user', cors='*')
    def import_unpaid_order(self, **k):
        cr, uid, context, session = request.cr, request.uid, request.context, request.session
        PosOrder = request.registry['pos.order']
        #need to pass configuration id from POS
#        pos_order_ids = PosOrder.search(cr, uid, [('state','=','draft'),('json_import','=',False),('config_id','=',k.get('config_id',False))], context=context)

        pos_order_ids = PosOrder.search(cr, SUPERUSER_ID, [('state','=','draft'),('json_import','=',False),('config_id','=',k.get('config_id',False))], context=context)
        _logger.info('draft orders : %s', str(pos_order_ids))
        unpaid_orders = []
#        for order in PosOrder.browse(cr, uid, pos_order_ids):
        for order in PosOrder.browse(cr, SUPERUSER_ID, pos_order_ids):
            if order.json_object:
                unpaid_orders.append(ast.literal_eval(order.json_object))
                order.write({'json_import': True})
        _logger.info('unpaid orders to import : %s', str(unpaid_orders))
        return unpaid_orders



