# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

import openerp

import logging
import werkzeug.utils


from openerp.addons.web.controllers.main import Home
from openerp.addons.web.controllers.main import ensure_db

from openerp.addons.point_of_sale.controllers.main import PosController

from openerp import http
from openerp.http import request
from openerp.tools.translate import _

import json
import ast

_logger = logging.getLogger(__name__)

from openerp.exceptions import UserError

class Home(Home):
    
    @http.route('/web/login', type='http', auth="none")
    def web_login(self, redirect=None, **kw):
        ensure_db()
        request.params['login_success'] = False
        if request.httprequest.method == 'GET' and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)

        if not request.uid:
            request.uid = openerp.SUPERUSER_ID

        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except openerp.exceptions.AccessDenied:
            values['databases'] = None

        if request.httprequest.method == 'POST':
            old_uid = request.uid
            uid = request.session.authenticate(request.session.db, request.params['login'], request.params['password'])
            
            if uid is not False:
                request.params['login_success'] = True
                if not request.registry['res.users'].has_group(request.cr, request.uid, 'point_of_sale.group_pos_user') \
                and not request.registry['res.users'].has_group(request.cr, request.uid, 'point_of_sale.group_pos_manager'):
                    redirect = '/pos/web'
                    
                if not redirect:
                    redirect = '/web'

                return http.redirect_with_hash(redirect)
            request.uid = old_uid
            values['error'] = _("Wrong login/password")
        return request.render('web.login', values)


class PosController(PosController):

    @http.route('/pos/web', type='http', auth='user')
    def a(self, debug=False, **k):
        cr, uid, context, session = request.cr, request.uid, request.context, request.session

        # if user not logged in, log him in
        PosSession = request.registry['pos.session']
        #Remove restriction of sinlge session per user
        pos_session_ids = PosSession.search(cr, uid, [('state','=','opened'),('shift_state','=','draft')], context=context)
        
        if not pos_session_ids:
            return werkzeug.utils.redirect('/web#action=point_of_sale.action_client_pos_menu')
        PosSession.login(cr, uid, pos_session_ids, context=context)

        return request.render('point_of_sale.index')

    @http.route('/pos_brew92/check_payment_status', type='json', auth='user', cors='*')
    def check_payment_status(self, **k):
        cr, uid, context, session = request.cr, request.uid, request.context, request.session
        
        PosOrder = request.registry['pos.order']
        pos_order_ids = PosOrder.search(cr, uid, [('pos_reference','=',k.get('name'))], context=context)
        print pos_order_ids
        if pos_order_ids:
            order_data = PosOrder.browse(cr, uid, pos_order_ids[0])
            if order_data.state == 'paid':
                return True
            else:
                return False
        return False


    @http.route('/pos_brew92/import_unpaid_order', type='json', auth='user', cors='*')
    def import_unpaid_order(self, **k):
        cr, uid, context, session = request.cr, request.uid, request.context, request.session
        PosOrder = request.registry['pos.order']
        pos_order_ids = PosOrder.search(cr, uid, [('state','=','draft'),('json_import','=',False)], context=context)
        print "pos_order_ids",pos_order_ids
        unpaid_orders = []

        for order in PosOrder.browse(cr, uid, pos_order_ids):
            if order.json_object:
                unpaid_orders.append(ast.literal_eval(order.json_object))
                order.write({'json_import': True})
        return unpaid_orders



