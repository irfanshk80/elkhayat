odoo.define('pos_reprint.pos_reprint', function (require) {
    "use strict";

    var devices = require('point_of_sale.devices');
    var screens = require('point_of_sale.screens');
    var session = require('web.session');
    var core = require('web.core');
    var exports = {};

    var _t = core._t;

    devices.ProxyDevice.include({
        keepalive: function(){
            var self = this;
            var pos = this.pos;
            function status(){
                self.connection.rpc('/hw_proxy/status_json',{},{
                    timeout:2500
                })
                .then(function(driver_status){
                    self.set_connection_status('connected',driver_status);
                },function(){
                    if(self.get('status').status !== 'connecting'){
                        self.set_connection_status('disconnected');
                    }
                }).always(function(){
                    setTimeout(status,20000);
                });
            }

            if(!this.keptalive){
                this.keptalive = true;
                status();
            }

            //code to delete paid order from waiter screen
            function delete_paid_orders(){

//                if(pos.get_cashier().role === 'waiter'){
                    var existing_orders = pos.get('orders').models
                    _.each(existing_orders, function(order){
                        self.connection.rpc('/pos_brew92/check_payment_status',{
                            name: order.name,
                            total_amount: order.get_total_with_tax()
                            },{timeout:2500}).then(function(payment_status)

                            {
                            if(payment_status==true){
                                order.finalize();
                            }
                        })
                    })
//                }

                //Custom code to bring unpaid order to casher screen
                if(pos.get_cashier().role !== 'waiter'){
                    try{
                        self.connection.rpc('/pos_brew92/import_unpaid_order',{'config_id':pos.config.id},{timeout:2500}).then(function(order_status){
                            if(order_status !==undefined  || order_status.length > 0) {
                                var orders = []
                                //              order_ids = order_status.filter(i.id);
                                var order_ids = order_status.map(function (i) {
                                    return i.uid
                                });
                                var existing_orders = pos.get('orders').models
                                _.each(existing_orders, function(obj){
                                    if(obj != undefined && order_ids.indexOf(obj.uid) != -1){
                                        obj.destroy({
                                            'reason':'Order Update'
                                        });
                                    }
                                        
                                    })
                                
                            _.each(order_status, function(order){
                            	if(order['pos_session_id'] !== undefined) {
	                            	order['pos_session_id'] = pos.pos_session.id
                            	}
                            })
                            
                            orders = JSON.stringify({
                                'unpaid_orders': order_status
                            });

                            var report = pos.import_orders(orders);
                            console.log(report);
                        //                pos.db.save('orders',order_status);
                            }
                        	});
                        

                    }
                    catch (e) {
                        console.log('Exception occurred');
                        console.log(e)
                    }
                }
            }

            setInterval(delete_paid_orders, 30000);
        },

        //Print via IP addresess
       print_receipt: function(receipt,options){
        options = options || {};
        console.log("options",options);
        var printer_ip = options.printer_ip || '';
        var self = this;
        if(receipt){
            this.receipt_queue.push(receipt);
        }
        function send_printing_job(){
            if (self.receipt_queue.length > 0){
                var r = self.receipt_queue.shift();
                console.log('receipt2333');
                console.log(r);
                self.message('print_xml_receipt',{ receipt: r ,network_ip:printer_ip},{ timeout: 5000 })
                    .then(function(){
                        send_printing_job();
                    },function(error){
                        if (error) {
                            self.pos.chrome.screen_selector.show_popup('error-traceback',{
                                'title': _t('Printing Error: ') + error.data.message,
                                'body':  error.data.debug,
                            });
                            return;
                        }
                        self.receipt_queue.unshift(r);
                    });
            }
        }
        send_printing_job();
    },


    });

});
