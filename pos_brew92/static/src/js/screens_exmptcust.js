odoo.define('pos_brew92.screens', function(require) {
    "use strict";

    var screens = require('point_of_sale.screens');

    var chrome = require('point_of_sale.chrome');
    var framework = require('web.framework');

    var core = require('web.core');
    var session = require('web.session');

    var Model = require('web.DataModel');

    var QWeb = core.qweb;
    var _t = core._t;
    var line_id = [];
    var oldMode = null;

    screens.ProductScreenWidget.include({
        click_product: function(product) {
            var self = this;
            var gui = this.gui;
            if (product.to_weight && this.pos.config.iface_electronic_scale) {
                this.gui.show_screen('scale', {
                    product: product
                });
            } else {
                console.log('Selection Items', product.selection_items);
                if (product.selection_items !== undefined) {
                   // this.pos.get_order().add_product(product);
                    gui.show_popup('combopack', {
                        cancel: function() {
                            this.gui.close_popup();
                        },
                        choice_items: product.selection_items,
                        product: product,
                        'title': 'Select a Choice'
                    });
                } else {

                    console.log('add  product is called')
                    this.pos.get_order().add_product(product);
                }
            }
        }
    });

    chrome.HeaderButtonWidget.include({

        renderElement: function() {
            var self = this;
            this._super();
            if (this.action) {
                this.$el.click(function() {
                    self.action_close();
                });
            }
        },

        action_close: function() {
            var self = this;
            if (!this.confirmed) {
                this.$el.addClass('confirm');
                this.$el.text(_t('Confirm'));
                this.confirmed = setTimeout(function() {
                    self.$el.removeClass('confirm');
                    self.$el.text(_t('Close'));
                    self.confirmed = false;
                }, 2000);
            } else {
                clearTimeout(this.confirmed);
                if (this.pos.get_cashier().role === 'waiter') {
                    framework.redirect('/web/session/logout');
                }
                this.gui.close();
            }
        },

    });

    screens.NumpadWidget.include({

        clickDeleteLastChar: function(){
        var self = this;
        var ui = this.gui;
        var order = this.pos.get_order('selectedOrder');
        var removed_items = order.removed_items || [];
        var selected_order_line = order.get_selected_orderline();
        console.log(this.pos.pos_session,"this.pos_session.security_pin");
        if(selected_order_line !== undefined){
        if(removed_items) {
            for (var oline in removed_items) {
                if(selected_order_line.quantity==0 && selected_order_line.cid == removed_items[oline]) {
                    return false;
                }
            }
        }
        var line_state = this.state;
        if ((this.state.get('buffer') == 0 || this.get('buffer') === "") && this.state.get('mode') === 'quantity' && !selected_order_line.mp_dirty){
            if(this.pos.pos_session.security_pin) {
                this.gui.ask_password(this.pos.pos_session.security_pin).then(function(){
                    var sel_order_id = selected_order_line.cid;
                    selected_order_line.mp_dirty = true;
                    removed_items.push(sel_order_id);
                    order.removed_items = removed_items;
                    line_state.trigger('set_value',line_state.get('buffer'));
                    ui.show_popup('textinput',{
                        title: _t('Provide Reason for Cancellation/Void'),
                        confirm: function(note) {
                            selected_order_line.set_void_reason(note);
                        },
                    });
                });
            } else  {
                this.gui.show_popup('error',_t('Please contact Cashier --- يرجى الاتصال أمين الصندوق'));
                return false;
            }
        } else {
            return self.state.deleteLastChar();
        }

        }

    },
        clickChangeMode: function(event) {
            var self = this;
            this._super(event);
            if (this.state.get('mode') === 'discount') {
                if (this.pos.pos_session.security_pin) {
                    this.gui.ask_password(this.pos.pos_session.security_pin).then(function() {
                        var newMode = event.currentTarget.attributes['data-mode'].nodeValue;
                        return self.state.changeMode(newMode);
                    });
                } else {
                    this.gui.show_popup('error', _t('Please contact Cashier --- يرجى الاتصال أمين الصندوق'));
                    return false;
                }
            }
            return self.state.changeMode(oldMode);
        }

    });


    screens.PaymentScreenWidget.include({

        renderElement: function() {
            var self = this;
            this._super();
            this.$('.confirm').click(function() {
                self.validate_payment_order();
            });
            //apply discount to social media
            this.$('.social_media_discount').click(function() {
                self.apply_media_discount();
            });

        },

    //Validation on click -> management credit
    click_paymentmethods: function(id) {
    	var self = this;
        var cashregister = null;
        for ( var i = 0; i < this.pos.cashregisters.length; i++ ) {
//        	console.log(this.pos.cashregisters[i].journal_id[1]);
        	
        	if ( this.pos.cashregisters[i].journal_id[0] === id ){
        		if(this.pos.cashregisters[i].journal_id[1].indexOf('Credit Management') >= 0){
                    if(this.pos.get_order().get_client() == null){
                        this.gui.show_popup('confirm',{
                            'title': _t('Please select the Customer'),
                            'body': _t('You need to select the customer before you can punch Management Credit order.'),
                            confirm: function(){
                                this.gui.show_screen('clientlist');
                            },
                        });
                    }else{
                        console.log(this.pos.get_order().get_client().barcode);
                        if(this.pos.get_order().get_client().barcode != '95'){
                            this.gui.show_popup('error', {
                                'title': _t('Select Customer'),
                                'body': _t('Invalid Customer. Please Select a Management Credit Customer'),
                            });
                            return;
                        } else {
                        	if(this.pos.pos_session.security_pin) {
                                this.gui.ask_password(this.pos.pos_session.security_pin).then(function(){
                    				cashregister = self.pos.cashregisters[i];
        	                    	self.pos.get_order().add_paymentline( cashregister );
        	                    	self.reset_input();
        	                        self.render_paymentlines();
                                });
                        	}
                        }
                    }
                    break;
        		} else {
    	    		if(this.pos.cashregisters[i].journal_id[1].indexOf('Credit Management') < 0){
    	    			cashregister = this.pos.cashregisters[i];
    		            break;
    	    		}
        		}
        	}
        }
        if(cashregister != null){
        	this.pos.get_order().add_paymentline( cashregister );
        }
        this.reset_input();
        this.render_paymentlines();
    

        },
        click_numpad: function(button) {
            var paymentlines = this.pos.get_order().get_paymentlines();
            var open_paymentline = false;

            for (var i = 0; i < paymentlines.length; i++) {
                if (!paymentlines[i].paid) {
                    open_paymentline = true;
                }
            }

            if (!open_paymentline) {
                //            this.pos.get_order().add_paymentline( this.pos.cashregisters[0]);
                this.render_paymentlines();
            }

            this.payment_input(button.data('action'));
        },
        apply_media_discount: function() {
            var self = this;
            var order = this.pos.get_order();
            if (order.get_client() == null) {
                this.gui.show_popup('error', {
                    'title': _t('Select Customer'),
                    'body': _t('Please Select Social Media Customer'),
                });
                return;
            }

            if (this.pos.pos_session.security_pin) {
                this.gui.ask_password(this.pos.pos_session.security_pin).then(function() {
                    var orderlines = order.get_orderlines();

                    for (var i = 0, len = orderlines.length; i < len; i++) {
                        orderlines[i].set_discount('10');
                    }
                    var total = order ? order.get_total_with_tax() : 0;
//                    console.log('paymentline.selected' + self.$('.paymentline.selected').length);
                    if (self.$('.paymentline.selected')) {
                        self.$('.paymentline.selected .col-due').text(self.format_currency(total));
                        self.$('.paymentline.selected .col-tendered').text(self.format_currency(total));
                    } else {
                        self.$('.paymentlines-empty .total').text(self.format_currency(total));
                    }
                    //		        this.$el.('.paymentlines-empty .total').textContent = self.format_currency(total);
                });
            }
        },
        validate_payment_order: function(force_validation) {
            var self = this;

            var order = this.pos.get_order();

            // FIXME: this check is there because the backend is unable to
            // process empty orders. This is not the right place to fix it.
            if (order.get_orderlines().length === 0) {
                this.gui.show_popup('error', {
                    'title': _t('Empty Order'),
                    'body': _t('There must be at least one product in your order before it can be validated'),
                });
                return;
            }

            session.rpc('/pos_brew92/check_payment_status', {
                    name: order.name,
                    total_amount: order.get_total_with_tax()
                })
                .then(function(output) {
                    if (output == false) {
                        self.gui.show_popup('error-traceback', {
                            'title': _t("Missing Payment"),
                            'body': _t('Please contact Cashier. Order is not paid or submitted to server.'),
                        });
                    } else if (output == true) {
                        order.finalize();
                    }
                });
        },
    validate_order: function(force_validation) {
        var self = this;

        var order = this.pos.get_order();

        // FIXME: this check is there because the backend is unable to
        // process empty orders. This is not the right place to fix it.
        if (order.get_orderlines().length === 0) {
            this.gui.show_popup('error',{
                'title': _t('Empty Order'),
                'body':  _t('There must be at least one product in your order before it can be validated'),
            });
            return;
        }

        var plines = order.get_paymentlines();
        for (var i = 0; i < plines.length; i++) {
            if (plines[i].get_type() === 'bank' && plines[i].get_amount() < 0) {
                this.pos_widget.screen_selector.show_popup('error',{
                    'message': _t('Negative Bank Payment'),
                    'comment': _t('You cannot have a negative amount in a Bank payment. Use a cash payment method to return money to the customer.'),
                });
                return;
            }
        }

        if (!order.is_paid() || this.invoicing) {
            return;
        }

        // The exact amount must be paid if there is no cash payment method defined.
        if (Math.abs(order.get_total_with_tax() - order.get_total_paid()) > 0.00001) {
            var cash = false;
            for (var i = 0; i < this.pos.cashregisters.length; i++) {
                cash = cash || (this.pos.cashregisters[i].journal.type === 'cash');
            }
            if (!cash) {
                this.gui.show_popup('error',{
                    title: _t('Cannot return change without a cash payment method'),
                    body:  _t('There is no cash payment method available in this point of sale to handle the change.\n\n Please pay the exact amount or add a cash payment method in the point of sale configuration'),
                });
                return;
            }
        }

        // if the change is too large, it's probably an input error, make the user confirm.
        if (!force_validation && (order.get_total_with_tax() * 1000 < order.get_total_paid())) {
            this.gui.show_popup('confirm',{
                title: _t('Please Confirm Large Amount'),
                body:  _t('Are you sure that the customer wants to  pay') +
                       ' ' +
                       this.format_currency(order.get_total_paid()) +
                       ' ' +
                       _t('for an order of') +
                       ' ' +
                       this.format_currency(order.get_total_with_tax()) +
                       ' ' +
                       _t('? Clicking "Confirm" will validate the payment.'),
                confirm: function() {
                    self.validate_order('confirm');
                },
            });
            return;
        }

        if (order.is_paid_with_cash() && this.pos.config.iface_cashdrawer) {

                this.pos.proxy.open_cashbox();
        }

        order.initialize_validation_date();

        if (order.is_to_invoice()) {
            var invoiced = this.pos.push_and_invoice_order(order);
            this.invoicing = true;

            invoiced.fail(function(error){
                self.invoicing = false;
                if (error.message === 'Missing Customer') {
                    self.gui.show_popup('confirm',{
                        'title': _t('Please select the Customer'),
                        'body': _t('You need to select the customer before you can invoice an order.'),
                        confirm: function(){
                            self.gui.show_screen('clientlist');
                        },
                    });
                } else if (error.code < 0) {        // XmlHttpRequest Errors
                    self.gui.show_popup('error',{
                        'title': _t('The order could not be sent'),
                        'body': _t('Check your internet connection and try again.'),
                    });
                } else if (error.code === 200) {    // OpenERP Server Errors
                    self.gui.show_popup('error-traceback',{
                        'title': error.data.message || _t("Server Error"),
                        'body': error.data.debug || _t('The server encountered an error while receiving your order.'),
                    });
                } else {                            // ???
                    self.gui.show_popup('error',{
                        'title': _t("Unknown Error"),
                        'body':  _t("The order could not be sent to the server due to an unknown error"),
                    });
                }
            });

            invoiced.done(function(){
                self.invoicing = false;
                order.finalize();
            });
        } else {
            this.pos.push_order(order,{'payment_done':true});
            this.gui.show_screen('receipt');
        }
    }
    });


    chrome.OrderSelectorWidget.include({

        deleteorder_click_handler: function(event, $el) {
            var self = this;
            var order = this.pos.get_order();
            if (!order) {
                return;
            } else if (!order.is_empty()) {
               // if (this.pos.pos_cashier.pos_security_pin) {
               //     this.gui.ask_password(this.pos.pos_cashier.pos_security_pin).then(function() {
               //         self.pos.delete_current_order();
               //     });
               // }
		this.gui.show_popup('error',{
                'title': _t('You can\'t delete order'),
                'body':  _t('Please void individual items and make payment'),
            });

            } else {
                this.pos.delete_current_order();
            }
        }
    });

    // Print payment recept as per floor

    screens.ReceiptScreenWidget.include({
        print_xml: function() {
            var env = {
                widget: this,
                pos: this.pos,
                order: this.pos.get_order(),
                receipt: this.pos.get_order().export_for_printing(),
                paymentlines: this.pos.get_order().get_paymentlines()
            };
            var receipt = QWeb.render('XmlReceipt', env);
            //get printer ip from floor configuration
            var printer_ip = this.pos.get_order().table.floor.network_ip || '';
            console.log('network_ip---', printer_ip);
            if (this.pos.get_cashier().role === 'waiter' && printer_ip != '') {
                this.pos.proxy.print_receipt(receipt, {
                    printer_ip: printer_ip
                });
            } else {
                this.pos.proxy.print_receipt(receipt);
            }

            this.pos.get_order()._printed = true;
        }
    });

    screens.ClientListScreenWidget.include({

        /*Overriden show to suppress render_list by default*/

        show: function() {
            var self = this;
            this._super();

            this.renderElement();
            this.details_visible = false;
            this.old_client = this.pos.get_order().get_client();

            this.$('.back').click(function() {
                self.gui.back();
            });

            this.$('.next').click(function() {
                self.save_changes();
                self.gui.back(); // FIXME HUH ?
            });

            this.$('.new-customer').click(function() {
                self.display_client_details('edit', {
                    'country_id': self.pos.company.country_id,
                });
            });

            //        var partners = this.pos.db.get_partners_sorted(1000);
            //        this.render_list(partners);

            this.reload_partners();

            if (this.old_client) {
                this.display_client_details('show', this.old_client, 0);
            }
            //        Customer not set, it will open new customer screen
            else {
                self.display_client_details('edit', {
                    'country_id': self.pos.company.country_id
                });
            }

            this.$('.client-list-contents').delegate('.client-line', 'click', function(event) {
                self.line_select(event, $(this), parseInt($(this).data('id')));
            });

            

            var search_timeout = null;
            
            if (this.pos.config.iface_vkeyboard && this.chrome.widget.keyboard) {
                this.chrome.widget.keyboard.connect(this.$('.searchbox input'));
            }

	        this.$('.searchbox input').on('keypress', function(event) {
	            clearTimeout(search_timeout);
	
	            var query = this.value;
	            //If loop to avoid search on empty query string on search box
	            if(query != '') {
	                search_timeout = setTimeout(function() {
	                	/*Toggle client-list after disable */
//	                    if( this.$('.client-list').is(":hidden")){
                    	self.$('.client-list').css('visibility','visible');
	                    self.perform_search(query, event.which === 13);
	                }, 70);
	            }
	        });

	        //search icon click disabled
	        /*this.$('.searchbox .search-clear').click(function(){
	            self.clear_search();
	        });*/
	},
	
	save_client_details: function(partner) {
            var self = this;

            var fields = {};
            this.$('.client-details-contents .detail').each(function(idx, el) {
                fields[el.name] = el.value;
            });

            if (!fields.name) {
                this.gui.show_popup('error', _t('A Customer Name Is Required'));
                return;
            } else if(!fields.phone) {
            	this.gui.show_popup('error', _t('A Customer Phone Is Required'));
                return;
            }

            if (this.uploaded_picture) {
                fields.image = this.uploaded_picture;
            }

            fields.id = partner.id || false;
            fields.country_id = fields.country_id || false;
            fields.barcode = fields.barcode || '';
            fields.date_of_birth = fields.date_of_birth || false;
            fields.organization_id = fields.organization_id || false;
            fields.last_visit_date = fields.last_visit_date || false;
            fields.total_visits = fields.total_visits || false;
            fields.most_order_product_id = fields.most_order_product_id || false;
            fields.max_order_product_count = fields.max_order_product_count || false;

            new Model('res.partner').call('create_from_ui', [fields]).then(function(partner_id) {
                self.saved_client_details(partner_id);
            }, function(err, event) {
                event.preventDefault();
                self.gui.show_popup('error', {
                    'title': _t('Error: Could not Save Changes'),
                    'body': _t('Your Internet connection is probably down.'),
                });
            });
    },
    reload_partners: function(){
        var self = this;
        return this.pos.load_new_partners().then(function(){
//        	Commented to disable default rendering of partner list after save partner
//            self.render_list(self.pos.db.get_partners_sorted(1000));
            
            // update the currently assigned client if it has been changed in db.
            var curr_client = self.pos.get_order().get_client();
            if (curr_client) {
                self.pos.get_order().set_client(self.pos.db.get_partner_by_id(curr_client.id));
            }
        });
    },
    
    order_history: function(id) {
    	/*Fetch Last 5 Order history of the customer*/
        new Model('res.partner').call('get_customer_order_history',[id]).then(function(result){
        	var order_history = JSON.parse(result);
        	if(order_history[id][0] != undefined){
	          	var orderdetails_html = QWeb.render('OrderDetails',{widget: this, orderdetails:order_history[id]});
	          	if(document.getElementsByClassName("client-history").length == 0) {  	
					var ordersection = document.createElement('div');
					ordersection.setAttribute("class", "client-details-box clearfix client-history");
	          	
				  ordersection.innerHTML = orderdetails_html;
				  var el = document.getElementsByClassName("client-details");
				  if(el[0] != undefined && ordersection.getAttribute("class").indexOf('client-details-box clearfix') == 0){
					  el[0].appendChild(ordersection);
				  }
	          	}
	          	
        	}
          });
    },
    
    line_select: function(event,$line,id){
    	var self = this;
        var partner = this.pos.db.get_partner_by_id(id);
        var order_details = {};
        var orderdetails;
        var contents = this.$('.client-details-contents');
        this.$('.client-list .lowlight').removeClass('lowlight');
        if ( $line.hasClass('highlight') ){
            $line.removeClass('highlight');
            $line.addClass('lowlight');
            this.display_client_details('hide',partner);
            this.new_client = null;
            this.toggle_save_button();
            this.$('.client-list').css('visibility','visible');
        }else{
            this.$('.client-list .highlight').removeClass('highlight');
            $line.addClass('highlight');
            var y = event.pageY - $line.parent().offset().top;
            this.display_client_details('show',partner,y);
            this.new_client = partner;
            this.toggle_save_button();
            this.$('.client-list').css('visibility','hidden');
        }
    },
    
    display_client_details: function(visibility,partner,clickpos){
        var self = this;
        var contents = this.$('.client-details-contents');
        var parent   = this.$('.client-list').parent();
        var scroll   = parent.scrollTop();
        var height   = contents.height();

        contents.off('click','.button.edit'); 
        contents.off('click','.button.save'); 
        contents.off('click','.button.undo'); 
        contents.on('click','.button.edit',function(){ self.edit_client_details(partner); });
        contents.on('click','.button.save',function(){ self.save_client_details(partner); });
        contents.on('click','.button.undo',function(){ self.undo_client_details(partner); });
        this.editing_client = false;
        this.uploaded_picture = null;

        if(visibility === 'show'){
            contents.empty();
            contents.append($(QWeb.render('ClientDetails',{widget:this,partner:partner})));

            var new_height   = contents.height();

            if(!this.details_visible){
                if(clickpos < scroll + new_height + 20 ){
                    parent.scrollTop( clickpos - 20 );
                }else{
                    parent.scrollTop(parent.scrollTop() + new_height);
                }
            }else{
                parent.scrollTop(parent.scrollTop() - height + new_height);
            }

            this.details_visible = true;
            this.toggle_save_button();
            this.order_history(partner.id);
        } else if (visibility === 'edit') {
            this.editing_client = true;
            contents.empty();
            contents.append($(QWeb.render('ClientDetailsEdit',{widget:this,partner:partner})));
            this.toggle_save_button();

            contents.find('.image-uploader').on('change',function(event){
                self.load_image_file(event.target.files[0],function(res){
                    if (res) {
                        contents.find('.client-picture img, .client-picture .fa').remove();
                        contents.find('.client-picture').append("<img src='"+res+"'>");
                        contents.find('.detail.picture').remove();
                        self.uploaded_picture = res;
                    }
                });
            });
        } else if (visibility === 'hide') {
            contents.empty();
            if( height > scroll ){
                contents.css({height:height+'px'});
                contents.animate({height:0},400,function(){
                    contents.css({height:''});
                });
            }else{
                parent.scrollTop( parent.scrollTop() - height);
            }
            this.details_visible = false;
            this.toggle_save_button();
        }
    },
});

/* Organizational Discount Button */
/*var DiscountButton = screens.ActionButtonWidget.extend({
    'template': 'DiscountButton',
    button_click: function(){
        var order  = this.pos.get_order();
        // var organizations_arr = this.pos.organizations;
        

        if(order.get_client()){    
	        var organization_id = order.get_client().organization_id;
	        for (var org=0; org < this.pos.organizations.length; org++) {
	            if (organization_id[0] == this.pos.organizations[org].id) {
	                 Code to apply Discount 
	                var discount = this.pos.organizations[org].discount
	                var orderlines = order.get_orderlines();
	                if(this.pos.pos_cashier.pos_security_pin) {
	                	this.gui.ask_password(this.pos.pos_cashier.pos_security_pin).then(function(){
			                for(var i = 0, len = orderlines.length; i < len; i++){
			                    orderlines[i].set_discount(discount);
			                }
	                	});
	                }     
	            }
	        }
        }else {
        	this.gui.show_popup('error',{
                'title': _t('Info: No Customer Selected'),
                'body': _t('Please select a customer to apply the Organisational Discount.'),
            });
        }
    },

        line_select: function(event, $line, id) {
            var partner = this.pos.db.get_partner_by_id(id);
            this.$('.client-list .lowlight').removeClass('lowlight');
            new Model('res.partner').call('get_customer_order_history', [id]).then(function(order_history) {
                console.log(order_history);
            })
            if ($line.hasClass('highlight')) {
                $line.removeClass('highlight');
                $line.addClass('lowlight');
                this.display_client_details('hide', partner);
                this.new_client = null;
                this.toggle_save_button();
            } else {
                this.$('.client-list .highlight').removeClass('highlight');
                $line.addClass('highlight');
                var y = event.pageY - $line.parent().offset().top;
                this.display_client_details('show', partner, y);
                this.new_client = partner;
                this.toggle_save_button();
            }
        },
    });*/

    /* Organizational Discount Button */
    var DiscountButton = screens.ActionButtonWidget.extend({
        'template': 'DiscountButton',
        button_click: function() {
            var order = this.pos.get_order();
            // var organizations_arr = this.pos.organizations;


            if (order.get_client()) {
                var organization_id = order.get_client().organization_id;
                for (var org = 0; org < this.pos.organizations.length; org++) {
                    if (organization_id[0] == this.pos.organizations[org].id) {
                        /* Code to apply Discount */
                        var discount = this.pos.organizations[org].discount
                        var orderlines = order.get_orderlines();
                        if (this.pos.pos_session.security_pin) {
                            this.gui.ask_password(this.pos.pos_session.security_pin).then(function() {
                                for (var i = 0, len = orderlines.length; i < len; i++) {
                                    orderlines[i].set_discount(discount);
                                }
                            });
                        }
                    }
                }
            } else {
                this.gui.show_popup('error', {
                    'title': _t('Info: No Customer Selected'),
                    'body': _t('Please select a customer to apply the Organisational Discount.'),
                });
            }
        },
    });

    screens.define_action_button({
        'name': 'organization_discount',
        'widget': DiscountButton,
    });

    var CustomerCommentsButton = screens.ActionButtonWidget.extend({
        'template': 'CustomerComments',
        button_click: function() {
            var order = this.pos.get_order();
            //        temp Code
            var orderlines = order.get_orderlines();
            var mp_dirty = false;
            if (orderlines.length === 0) {
                this.gui.show_popup('error', {
                    'title': _t('Empty Order'),
                    'body': _t('There must be at least one product in your order to take the feedback.'),
                });
                return;
            }
            _.each(orderlines, function(line) {
                if (line.mp_dirty === true) {
                    mp_dirty = true;
                    return;
//                    break;

                }
            });
            if (mp_dirty === true) { 
                this.gui.show_popup('error', {
                    'title': _t('Draft Order'),
                    'body': _t('Please click Order to send draft order to kitchen before taking feedback.'),
                });
                            return;
            }
            //        temp code end
            if (order) {
                this.gui.show_popup('textarea', {
                    title: _t('Customer Feedback'),
                    value: order.get_note(),
                    confirm: function(note) {
                        order.set_note(note);
                        this.pos.push_order(order);
                    },
                });
            }
        },
    });

    screens.define_action_button({
        'name': 'customer_comments',
        'widget': CustomerCommentsButton
    });

});
