odoo.define('pos_brew92.popups', function (require) {
"use strict";


var PopupWidget = require('point_of_sale.popups');
var gui = require('point_of_sale.gui');

var SelectionCompboPopupWidget = PopupWidget.extend({
    template: 'SelectionCompboPopupWidget',
  init: function(parent, args) {
        this._super(parent, args);
        this.choice_items = [];
        this.selected_items = {};
        this.product = false;
    },

    events: {
        'click .button.cancel':  'click_cancel',
        'click .button.confirm': 'click_confirm',
        'click .button.clear': 'click_clear',
        'click .selection-item': 'click_item',
        'click .product-beverage': 'click_item'
    },
    show: function(options){
        options = options || {};
        var self = this;
        this._super(options);

        this.choice_items    = options.choice_items    || [];
        this.product = options.product || false;
        this.choice_count = options.choice_count;
        this.count = 0;
	
	    _.each(this.choice_items, function(choice){
            choice.sort(function(a, b) {
                  var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                  var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                  if (nameA < nameB) {
                    return -1;
                  }
                  if (nameA > nameB) {
                    return 1;
                  }
                  // names must be equal
                  return 0;
            });
       })
	
        this.renderElement();
    },
    click_item : function(event) {
        // var line = this.pos.get_order().get_selected_orderline();
        var product_id = $(event.currentTarget).data("product-id");
        event.preventDefault();
        var quantity = $(event.currentTarget).data("quantity");
        if(this.count<this.choice_count && event.currentTarget.parentElement.className =='Unselected'){
             this.count += 1
            if(product_id in this.selected_items){
//                alert('More than one quantity is not allowed.');
                this.selected_items[product_id].count += 1
            }
            else{
                this.selected_items[product_id] = {'qty': parseFloat(quantity),'name' : event.currentTarget.title,'count':1};
            }
            if(this.count == 1){
                $('.footer .confirm').css('display','block');
            }

            $(event.currentTarget).css('border','2px solid blue');
            $(event.currentTarget).children(".count-indicator").html(this.selected_items[product_id].count);
            event.currentTarget.parentElement.className='Selected';

        }else if(this.count>this.choice_count)
        {
            alert('No More allowed.');
        }
        else if(event.currentTarget.parentElement.className =='Selected'){
            alert('Please select another choice.');
        }
        if (this.choice_count == 1){
            this.click_confirm();
        }
    },
    render_element : function(){

    },
    click_cancel: function(){
        this.gui.close_popup();
    },
    close: function(){
        this.choice_items = [];
        this.selected_items = {}
        this.count = 0;
    },
    click_clear: function(){
        this.choice_items = [];
        this.selected_items = []
        this.count = 0;
        $('.product-beverage div.count-indicator').html("");
        $('.footer .confirm').css('display','none');
        $('.product-beverage').css('border','');
        $('div.error').html("");
        var selected_nodes = $('div.Selected');
         _.each(selected_nodes, function(node){
             node.className ='Unselected'

         })
    },
    click_confirm: function(){
        this.pos.get_order().add_product(this.product)
        var line = this.pos.get_order().get_selected_orderline();
        var choice_notes = '';
        var line_details = []
        var items_selected = this.selected_items;
        for (var key in items_selected) {
            for(var i=0; i<parseInt(items_selected[key].count);i++){
                line_details.push([0,0,{'product_id': parseInt(key), 'qty': parseFloat(items_selected[key].qty)}]);
            }
//            line_details.push([0,0,{'product_id': parseInt(key), 'qty': parseFloat(items_selected[key].qty)*parseInt(items_selected[key].count)}]);
            choice_notes += items_selected[key].count + " - "+  items_selected[key].name + "\n";
        }

        if(choice_notes){
            line.set_note(choice_notes.slice(0,-1));
                line.set_choice(line_details);
        }
        this.gui.close_popup();
        if (this.options.cancel) {
            this.options.cancel.call(this);
        }
    },
});

gui.define_popup({name:'combopack', widget: SelectionCompboPopupWidget});

});
