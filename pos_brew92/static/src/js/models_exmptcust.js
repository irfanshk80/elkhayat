odoo.define('pos_brew92.models', function (require) {
    "use strict";

var models = require('point_of_sale.models');
var _super_posmodel = models.PosModel.prototype;
var utils = require('web.utils');

var round_pr = utils.round_precision;

models.PosModel = models.PosModel.extend({
    initialize: function (session, attributes) {
        this.pos_cashier = null;
//        this.organizations = [];
        this.card_type = null;
        

        var session_model = _.find(this.models, function(model){
            return model.model === 'pos.session';
        });
        //Load supervisor password
        //if user is smart, he can easily get password from browser
        //Need encription
        session_model.fields.push('security_pin');
        session_model.domain = function(self){
            return [['state','=','opened']];
        };

        var res_company_model = _.find(this.models, function(model){
            return model.model === 'res.company';
        });
        res_company_model.fields.push('vat_ar_image,total_vat_ar');

        var floor_model = _.find(this.models, function(model){
            return model.model === 'restaurant.floor';
        });
        floor_model.fields.push('no_cash','network_ip');

        var partner_model = _.find(this.models, function(model){
            return model.model === 'res.partner';
        });
        partner_model.fields.push('management_credit','security_pin','date_of_birth','organization_id','last_visit_date',
                 'total_visits','most_order_product_id','max_order_product_count');
        
        var account_journal_model = _.find(this.models, function(model){
            return model.model === 'account.journal';
        });
        account_journal_model.fields.push('card_type');

        var product_model = _.find(this.models, function(model){
            return model.model === 'product.product';
        });
        product_model.fields.push('ar_display_image')
        product_model.loaded = function(self, products){

        self.db.add_products(products);
        _.each(products, function(product){
            
             if(product.ar_display_image){
                 var  img = new Image;
                 var src = window.location.origin + '/web/image?model=product.product&field=ar_display_image&id='+product.id;
                    var img = new Image();
                    img.setAttribute('crossOrigin', 'anonymous');
                    img.onload = function () {
                        var canvas = document.createElement("canvas");
                        canvas.width =this.width;
                        canvas.height =this.height;

                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(this, 0, 0);

                        var dataURL = canvas.toDataURL("image/png");
                        product.ar_display_image = dataURL;
                    };
                    img.src = src;
                }
                //console.log('Products',products);
             });
          }
        
        return _super_posmodel.initialize.call(this, session, attributes);
    },
    push_order: function(order, opts) {
        opts = opts || {};
        var self = this;

        if(order){
            this.db.add_order(order.export_as_JSON(opts));
        }

        var pushed = new $.Deferred();

        this.flush_mutex.exec(function(){
            var flushed = self._flush_orders(self.db.get_orders(), opts);

            flushed.always(function(ids){
                pushed.resolve();
            });
        });
        return pushed;
    }
    
})

models.load_models([{
        model:'product.bom.choice',
        fields : ['product_id','qty','parent_id'],
        loaded: function(self, bom_choices){
            self.bom_choices = bom_choices;
        }
    },
    {
        model:'product.bom.detail',
        fields : ['product_id','qty','has_choice','bom_choice','parent_id'],
        domain: [['has_choice','=',true]],
        loaded: function(self, bom_details){
            self.bom_details = bom_details;
            _.each(self.bom_details, function(detail){
                var selection_items = []
                if(detail.has_choice==true){
                    selection_items.push({'id': detail.product_id[0],'name': detail.product_id[1],'qty':detail.qty})
                    _.each(self.bom_choices, function(choice){
                        if(choice.parent_id[0] == detail.id){
                            selection_items.push({'id': choice.product_id[0],'name': choice.product_id[1],'qty':choice.qty});
                        }
                    })
                }
                var main_product = self.db.get_product_by_id(detail.parent_id[0]);
                main_product.selection_items = selection_items;
                //console.log(main_product);
            })

        }
    },
    {
            model:'res.company',
            loaded: function(self,companies){
                self.company = companies[0];
                if(self.company.vat_ar_image){
                    var  img = new Image;                                                      
                    var src = window.location.origin + '/web/image?model=res.company&field=vat_ar_image&id='+self.company.id;
                        var img = new Image();
                        img.setAttribute('crossOrigin', 'anonymous');
                        img.onload = function () {
                            var canvas = document.createElement("canvas");
                            canvas.width =this.width;
                            canvas.height =this.height;

                            var ctx = canvas.getContext("2d");
                            ctx.drawImage(this, 0, 0);

                            var dataURL = canvas.toDataURL("image/png");
                            self.company.vat_ar_image = dataURL;
                        };
                        img.src = src;
                }
             console.log(self.company);
            }
        },
    {
        model:  'res.users',
        fields: ['name','pos_security_pin','groups_id','barcode','floor_id'],
        domain: function(self){ return [['groups_id','=', self.config.group_pos_order_taker_id[0]]]; },
        loaded: function(self,users){
            // we attribute a role to the user, 'cashier' or 'manager', depending
            // on the group the user belongs.
            var cashier = self.pos_session.user_id[0]
            var pos_users = [];
            for (var i = 0; i < users.length; i++) {
                var user = users[i];
                for (var j = 0; j < user.groups_id.length; j++) {
                    var group_id = user.groups_id[j];
                    if (group_id === self.config.group_pos_manager_id[0]) {
                        user.role = 'manager';
                        break;
                    } else if (group_id === self.config.group_pos_user_id[0]) {
                        user.role = 'cashier';
                    }
                    else if (group_id === self.config.group_pos_order_taker_id[0]) {
                        user.role = 'waiter';
                    }
                }
                if (user.role) {
                        pos_users.push(user);
                }
                // replace the current user with its updated version
                if (user.id === self.user.id) {
                    self.user = user;
                }
               if (cashier === user.id) {
                    self.pos_cashier = user;
                }
            }
            self.users = pos_users;
        }
    },{
        model:'organization.name',
        fields : ['id','name','discount'],
        loaded: function(self, organizations){
            self.organizations = organizations;
        }
    },
]);

//send waiter name as served by
var _super_order = models.Order.prototype;
models.Order = models.Order.extend({
    initialize: function(attributes,options) {
        _super_order.initialize.apply(this,arguments);
        this.note = this.note || '';
        this.removed_items = [];
        this.save_to_db();
    },
    //Calculate total discount with rounded display_price after discount
//    get_total_discount: function() {
//        return round_pr(this.orderlines.reduce((function(sum, orderLine) {
//            console.log((orderLine.get_unit_price() * orderLine.get_quantity()) - orderLine.get_display_price());
//            // return sum + (orderLine.get_unit_price() * (orderLine.get_discount()/100) * orderLine.get_quantity());
//            return sum + ((orderLine.get_unit_price() * orderLine.get_quantity()) - orderLine.get_display_price());
//        }), 0), this.pos.currency.rounding);
//    },
    export_as_JSON: function(opts) {
        opts = opts || {};
        var json = _super_order.export_as_JSON.apply(this,arguments);
        json.pos_waiter = this.pos.user.name;
        json.note = this.note;
         if(opts.payment_done){
            json.order_paid = true;
        }
        return json;
    },
     export_for_printing: function() {
        var json = _super_order.export_for_printing.apply(this,arguments);
        json.pos_waiter     = this.pos_waiter ? this.pos_waiter : this.pos.user.name;
        json.vat_arabic = this.pos.company.vat_ar_image;
        json.total_vat_ar = this.pos.company.total_vat_ar;
        return json;
    },
    init_from_JSON: function(json){
       
        _super_order.init_from_JSON.apply(this,arguments);
        this.pos_waiter = json.pos_waiter ? json.pos_waiter : this.pos.user.name ;
        this.note = json.note ? json.note : '' ;
        
    },
    get_note: function(){
        return this.note;
    },
    set_note: function(note) {
        this.note = note || '';
        this.trigger('change');
    },
});

var orderlineid = 1;
var _super_orderline = models.Orderline.prototype;
models.Orderline = models.Orderline.extend({
	initialize: function(attr, options) {
        _super_orderline.initialize.call(this,attr,options);
        this.line_detail = this.line_detail || [];
        this.void_reason = this.void_reason || '';
//	this.id       = orderlineid++;
	if (!options.json) {
            console.log('Not is json format');
            this.id       = orderlineid++;
        }


    },
    set_choice: function(choice){
        this.line_detail = choice;
        this.trigger('change',this);
    },
    get_choice: function(){
        return this.line_detail;
    },
    set_void_reason: function(reason){
        this.void_reason = reason;
        this.trigger('change',this);
    },
    get_void_reason: function(){
        return this.void_reason;
    },
    //Get each product taxes_ids, pass it for printing product wise taxes.
    get_tax_ids: function(){
        return this.get_product().taxes_id;
    },
    //currency_rounding removed to get rounded display_price (each line)
//    get_display_price: function(){
//        if (this.pos.config.iface_tax_included) {
//            return round_pr(this.get_price_with_tax());
//        } else {
//            return round_pr(this.get_base_price());
//        }
//    },
    //currency_rounding removed from total_excluded varaible, to get rounded Total(in the cart) after discount
//    compute_all: function(taxes, price_unit, quantity, currency_rounding) {
//        var self = this;
//        var total_excluded = round_pr(price_unit * quantity);
//        var total_included = total_excluded;
//        var base = total_excluded;
//        var list_taxes = [];
//        if (this.pos.company.tax_calculation_rounding_method == "round_globally"){
//           currency_rounding = currency_rounding * 0.00001;
//        }
//        _(taxes).each(function(tax) {
//            tax = self._map_tax_fiscal_position(tax);
//            if (tax.amount_type === 'group'){
//                var ret = self.compute_all(tax.children_tax_ids, price_unit, quantity, currency_rounding);
//                total_excluded = ret.total_excluded;
//                base = ret.total_excluded;
//                total_included = ret.total_included;
//                list_taxes = list_taxes.concat(ret.taxes);
//            }
//            else {
//                var tax_amount = self._compute_all(tax, base, quantity);
//                tax_amount = round_pr(tax_amount, currency_rounding);
//
//                if (tax_amount){
//                    if (tax.price_include) {
//                        total_excluded -= tax_amount;
//                        base -= tax_amount;
//                    }
//                    else {
//                        total_included += tax_amount;
//                    }
//                    if (tax.include_base_amount) {
//                        base += tax_amount;
//                    }
//                    var data = {
//                        id: tax.id,
//                        amount: tax_amount,
//                        name: tax.name,
//                    };
//                    list_taxes.push(data);
//                }
//            }
//        });
//        return {taxes: list_taxes, total_excluded: total_excluded, total_included: total_included};
//    },
    export_as_JSON: function(){
        var json = _super_orderline.export_as_JSON.call(this);
        json.line_detail = this.line_detail;
        json.void_reason = this.void_reason;
        return json;
    },
    init_from_JSON: function(json){
        _super_orderline.init_from_JSON.apply(this,arguments);
        this.line_detail = json.line_detail;
	this.id = json.pos_id || this.id;
        this.void_reason = json.void_reason || '';
	orderlineid = Math.max(this.id+1,orderlineid);
    },
    export_for_printing: function() {
        var json = _super_orderline.export_for_printing.apply(this,arguments);
        json.ar_display_name = this.get_product().ar_display_name || "";
        json.ar_display_image = this.get_product().ar_display_image || null;
        json.void_reason = this.void_reason;
        json.tax_ids = this.get_tax_ids();
        return json;
    },
});

});
