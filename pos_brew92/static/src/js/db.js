odoo.define('pos_brew92.DB', function (require) {
    "use strict";

var db = require('point_of_sale.DB');

//This class is inherited to store product information by template id
db.include({
    init: function(options){
        var self = this;
        this._super(options);
        this.product_by_tmpl_id = {};
    },
    add_products: function(products){
        this._super(products);
        for(var i = 0, len = products.length; i < len; i++){
            var product = products[i];
            this.product_by_tmpl_id[product.product_tmpl_id] = product;
        }
    },
    get_product_by_tmpl_id: function(id){
        return this.product_by_tmpl_id[id];
    },
});

});