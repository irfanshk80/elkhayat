# -*- encoding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2014 iFenSys Software Solutions Pvt Ltd
#    (http://wwww.ifensys.com) All Rights Reserved.
#     info@ifensys.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from ftplib import FTP
import logging

import pysftp

_logger = logging.getLogger(__name__)

class ftp_config(osv.osv):
    _name = 'ftp.config'
    _description = 'FTP Configuration module'
    _columns = {
        'name': fields.char('Site Name', size=256,required=True),
        'host':fields.char('Host', size=256,required=True),
        'port':fields.integer('Port',required=True),
        'username': fields.char('User Name', size=256),
        'password' : fields.char('Password', size=256),
        'protocol': fields.selection([('ftp','FTP'),('sftp','SFTP')], 'Protocol'),
        'emails': fields.text("Emails",help="Emails should be comma separated"),
    }
    
    def check_connection(self,cr,uid,ids,context=None):
        login = False
        try:
            ftp_data = self.browse(cr,uid,ids[0])
            host = ftp_data.host
            username = ftp_data.username
            password = ftp_data.password
            port = ftp_data.port
            if ftp_data.protocol=='ftp':
                ftp = FTP(host)# connect to host, default port on success ->>  <ftplib.FTP instance at 0x02A9B828>

                login_status = ftp.login(username, password)# user anonymous, passwd anonymous@
                ftp.close()
                if login_status=="230 Logged on":
                    login = True
                    _logger.info('Login Successful for: %s',host)
            elif ftp_data.protocol=='sftp':
                cnopts = pysftp.CnOpts()
                cnopts.hostkeys = None
                srv = pysftp.Connection(host=host, username=username,password=password, port=port, cnopts = cnopts)
                if srv:
                    login = True
            
        except Exception, e:  #you can specify type of Exception also
            _logger.error('Problem in current ftp connection checking process: %s: %s',host,str(e))
            raise osv.except_osv(_(''), _('Login Unsuccessful! May Be Incorrect Settings!'))
        if login:
            raise osv.except_osv(_(''), _('Login successful!'))
        else:
            _logger.info('Login Unsuccessful for: %s',host)
            raise osv.except_osv(_(''), _(u'Login Unsuccessful! May Be Incorrect Settings!'))
        
#     def check_connection(self,cr,uid,ids,context=None):
#         login = False
#         try:
#             ftp_data = self.browse(cr,uid,ids[0])
#             host = ftp_data.host
#             username = ftp_data.username
#             password = ftp_data.password
#             ftp = FTP(host)# connect to host, default port on success ->>  <ftplib.FTP instance at 0x02A9B828>
#             login_status = ftp.login(username, password)# user anonymous, passwd anonymous@
#             ftp.close()
#             if login_status=="230 Logged on":
#                 login = True
#                 _logger.info('Login Successful for: %s',host)
#             
#         except Exception, e:  #you can specify type of Exception also
#             _logger.error('Problem in current ftp connection checking process: %s: %s',host,str(e))
#             raise osv.except_osv(_(''), _('Login Unsuccessful! May Be Incorrect Settings!'))
#         if login:
#             raise osv.except_osv(_(''), _('Login successful!'))
#         else:
#             _logger.info('Login Unsuccessful for: %s',host)
#             raise osv.except_osv(_(''), _(u'Login Unsuccessful! May Be Incorrect Settings!'))

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
