# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
import datetime
import csv
from openerp.exceptions import UserError
import time
from openerp.tools.translate import _
from dateutil import tz
from ftplib import FTP
import logging
import os, sys
import zipfile
import base64
from reportlab.rl_config import _DEFAULTS
import pysftp


_logger = logging.getLogger(__name__)

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('Asia/Riyadh')

def change_tz(date):
    input_date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S' )
    utc = input_date.replace(tzinfo=from_zone)
    output_date = utc.astimezone(to_zone)
    return output_date

def full_path(base_url,filename):
    return os.path.join(base_url,filename)

class pos_category(osv.osv):
    _inherit = "pos.category"

    _columns = {
           'emis_id': fields.char('EMIS Product ID')
    }

class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
           'emis_id': fields.char('EMIS Customer ID')
    }


class res_users(osv.osv):
    _inherit = "res.users"

    _columns = {
           'emis_id': fields.char('EMIS USER ID')
    }

class pos_session(osv.osv):
    _inherit = "pos.session"

    _columns = {
           'is_synced': fields.boolean('Is Synced?'),
           'supershift_id':fields.char('Supervisor Shift #'),
           'cashier_session_ids':fields.many2many('supervisor.session','supervisor_cashier_sessions','cashier_id','supervisor_id',"Supervisor Sessions")
    }

    _defaults = {
        'is_synced': lambda *a: False,
    }

class pos_config(osv.osv):
    _inherit = "pos.config"

    _columns = {
           'super_user_id':fields.many2one('res.users',"Supervisor"),
    }

class pos_order(osv.osv):
    _inherit = "pos.order"

    _columns = {
        'is_synced':fields.boolean('Is Synced?'),
        'order_parent_id':fields.many2one('pos.order', "Parent ID")
    }

    _defaults = {
        'is_synced': lambda *a: False,
    }

    def refund(self, cr, uid, ids, context=None):
        """Create a copy of order for refund order"""
        clone_list = []
        line_obj = self.pool.get('pos.order.line')

        for order in self.browse(cr, uid, ids, context=context):
            current_session_ids = self.pool.get('pos.session').search(cr, uid, [
                ('state', '!=', 'closed'),
                ('user_id', '=', uid)], context=context)
            if not current_session_ids:
                raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))

            clone_id = self.copy(cr, uid, order.id, {
                'name': order.name + ' REFUND', # not used, name forced by create
                'session_id': current_session_ids[0],
                'date_order': time.strftime('%Y-%m-%d %H:%M:%S'),
                'order_parent_id':order.id
            }, context=context)
            clone_list.append(clone_id)

        for clone in self.browse(cr, uid, clone_list, context=context):
            for order_line in clone.lines:
                line_obj.write(cr, uid, [order_line.id], {
                    'qty': -order_line.qty
                }, context=context)

        abs = {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id':clone_list[0],
            'view_id': False,
            'context':context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        return abs

class ProductTemplate(osv.osv):
    _inherit = "product.template"

    _columns = {
        'bom_detail':fields.one2many('product.bom.detail', 'parent_id', 'BoM Details', copy=True),
        'emis_uom':fields.char('uom'),
        'emis_categ_id':fields.char('EMIS Category')
    }

class product_bom_detail(osv.osv):
    _name = "product.bom.detail"
    _rec_name = 'product_id'

    _columns = {
                'product_id': fields.many2one('product.template', 'Product', required=True, change_default=True),
                'qty': fields.float('Quantity'),
                'uom':fields.char('UOM'),
                'parent_id': fields.many2one('product.template', 'Parent Product', ondelete='cascade', domain=[('sale_ok', '=', True)] ),
                'bom_choice':fields.one2many('product.bom.choice', 'parent_id', 'BoM Choice', copy=True),
                'has_choice':fields.boolean('Has Choice?')
                }

    _defaults = {
        'qty': lambda *a: 1,
        'has_choice':lambda *a: False,
    }

class product_bom_choice(osv.osv):
    _name = "product.bom.choice"
    _rec_name = "product_id"

    _columns = {
                'product_id': fields.many2one('product.template', 'Product', required=True, change_default=True),
                'qty': fields.float('Quantity'),
                'uom':fields.char('UOM'),
                'parent_id': fields.many2one('product.bom.detail', 'Parent Product', ondelete='cascade'),
                }

    _defaults = {
        'qty': lambda *a: 1,
    }

class AccountJournal(osv.osv):
    _inherit = "account.journal"

    _columns = {
                'card_type': fields.boolean('Card Type?'),
                'emis_id': fields.char('EMIS Id'),
                }

class supervisor_session(osv.osv):
    _name = "supervisor.session"

    _columns = {
                'session_id':fields.char("Session #"),
                'emis_export_id':fields.many2one('emis.connect','EMIS Export ID'),
                'session_start_date':fields.date('Start Date'),
                'cashier_session_ids':fields.many2many('pos.session','supervisor_cashier_sessions','supervisor_id','cashier_id',"Cashier Sessions")
                }

class res_company(osv.osv):
    _inherit = "res.company"

    _columns = {
                'business_unit':fields.char("Business Unit"),
                'unit_id':fields.char("Unit"),
                'shift_prefix':fields.char("Shift Prefix"),
                'sales_prefix':fields.char("Sales Prefix"),
                'return_prefix':fields.char("Return Prefix"),
                'supervisor':fields.char("Supervisor"),
                'supervisor_pwd':fields.char("Supervisor Password")
                }

class emis_connect(osv.osv):
    _name = "emis.connect"

    _columns = {
        'name': fields.char('Name'),
        'supervisor_session_ids' : fields.one2many('supervisor.session','emis_export_id','Supervisor Sessions'),
        'email_to':fields.text('Email To'),
        'state':fields.selection([('draft',"Draft"),('prepared',"Prepared"),('placed',"Placed"),('postponed',"Postponed"),('posted',"Posted"),('cancelled',"Cancelled")],"State"),
        'prepared_date':fields.datetime(string='Prepared Date'),
        'placed_date':fields.datetime(string='Placed Date'),
        'postponed_date':fields.datetime(string='Postponed Date'),
        'posted_date':fields.datetime(string='Posted Date'),
        'cancelled_date':fields.datetime(string='Cancelled Date'),
    }

    _defaults = {
        'state': lambda *a: 'draft',
    }

    def set_draft(self,cr,uid,ids):
        res_dict = {'state':'draft'}
        self.pool.get('emis.connect').write(cr,uid,ids,res_dict)

    def set_prepared(self,cr,uid,ids):
        res_dict = {'state':'prepared','prepared_date':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        self.pool.get('emis.connect').write(cr,uid,ids,res_dict)

    def set_placed(self,cr,uid,ids):
        res_dict = {'state':'placed','placed_date':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        self.pool.get('emis.connect').write(cr,uid,ids,res_dict)

    def set_postponed(self,cr,uid,ids):
        res_dict = {'state':'postponed','postponed_date':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        self.pool.get('emis.connect').write(cr,uid,ids,res_dict)

    def set_posted(self,cr,uid,ids):
        res_dict = {'state':'posted','posted_date':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        self.pool.get('emis.connect').write(cr,uid,ids,res_dict)

    def set_cancelled(self,cr,uid,ids):
        res_dict = {'state':'cancelled','cancelled_date':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        self.pool.get('emis.connect').write(cr,uid,ids,res_dict)

    def send_log(self, cr, uid, context=None):
        if context is None:
            context = {}
        _logger.info('EMIS Scheduler Starting....')
        emis_connect_obj = self.pool.get('emis.connect')
        supervisor_session_obj = self.pool.get('supervisor.session')
        base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'emis_export_path')
        previous_date = change_tz(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')) - datetime.timedelta(1)
        print previous_date
        #previous_date = "2016-12-01"
        supervisor_shift_ids = supervisor_session_obj.search(cr,uid,[('session_start_date','=',previous_date)])
        filepath = os.path.join(base_url,'emis_sync_log.txt')
        f = open(filepath,'w')
        f.write('Sync Datetime\n')
        ec_id = False
        if supervisor_shift_ids:
            ec_id = supervisor_session_obj.browse(cr,uid,supervisor_shift_ids[0]).emis_export_id.id
        for ssid in supervisor_shift_ids:
            ss_obj = supervisor_session_obj.browse(cr,uid,ssid)
            if ss_obj.emis_export_id.placed_date:
                f.write(str(change_tz(ss_obj.emis_export_id.placed_date))+"\n")
        f.close()

        mail_obj = self.pool['mail.mail']

        model_obj = self.pool.get('ir.model.data')
        mail_template_id = model_obj.get_object(cr, uid, 'pos_connect_ecw', 'emis_export_email_template')
        data_attach = {}
        template_values = {}

        context.update({'active_id':ec_id,'active_model': 'emis.connect','default_model': 'emis.connect'})

        with open(filepath, "rb") as file:
            encoded_string = base64.b64encode(file.read())

        data_attach = {
            'name': 'EMIS Export Log',
            'datas': encoded_string,
            'datas_fname': "emis_export_log"+".txt",
            'description': 'EMIS Export log',
            'res_model': 'emis.connect',
            'res_id':ec_id
        }

        template_values = self.pool.get('mail.template').generate_email(cr, uid, mail_template_id.id, ec_id, context=context)
        template_values['attachment_ids'] = [(0, 0, data_attach)]
        template_values['subject'] = "EMIS Export Log"
        template_values['body_html'] = "Hello, \n\n Please find the attached log file"

        mail_id =  mail_obj.create(cr, uid, template_values)
        mail_obj.send(cr,uid,[mail_id])
        time.sleep(2)
        return True

    def push_files(self, cr, uid, files,base_url,context=None):
        try:
            _logger.info('EMIS Scheduler Starting....')
            ftp_config_obj = self.pool.get('ftp.config')
            ftp_config_ids = ftp_config_obj.search(cr, uid,[])
            ftp = False
            sftp = False
            if not ftp_config_ids:
                _logger.info('No ftp config found !....')
                return {"error":"No Ftp Config Found"}

            else:
                ftp_obj_list = ftp_config_obj.browse(cr,uid,ftp_config_ids)

                for ftp_obj_item in ftp_obj_list:

                    try:
                        if ftp_obj_item.protocol=='ftp':
                            ftp = FTP(ftp_obj_item.host)# connect to host, default port on success ->>  <ftplib.FTP instance at 0x02A9B828>
                            login_status = ftp.login(ftp_obj_item.username , ftp_obj_item.password)# user anonymous, passwd anonymous@
                            print login_status ## 530 Login or password incorrect! (or) 230 Logged on
                            ftp_files = ftp.nlst()
#                            ftp_files = ftp.GetList()
                            if ftp_files:
                                _logger.info('Old files found') ## if there is no new orders for a few days, check the ftp server or notify the client
                                for ftp_file in ftp_files:
                                    ftp.delete(ftp_file)
                        #sftp parameter
                        elif ftp_obj_item.protocol=='sftp':
                            cnopts = pysftp.CnOpts()
                            cnopts.hostkeys = None
                            cinfo = {'host':ftp_obj_item.host, 'username':ftp_obj_item.username, 'password':ftp_obj_item.password, 'port':ftp_obj_item.port, 'cnopts':cnopts}

                            sftp = pysftp.Connection(**cinfo)
#                            with pysftp.Connection(**cinfo) as sftp:
#                                ftp_files = sftp.GetList()
                            unit_directory = self.pool.get('ir.config_parameter').get_param(cr, uid, 'emis_unit_data')
                            with sftp.cd(unit_directory):
                                ftp_files = sftp.listdir()
                                print"old files",ftp_files
                                if ftp_files:
                                    _logger.info('Old files found') ## if there is no new orders for a few days, check the ftp server or notify the client
                                    for ftp_file in ftp_files:
    #                                    with pysftp.Connection(**cinfo) as sftp:
                                        sftp.remove(ftp_file)
                            ### start with the new client's ftp
                        for file_item in files:
                            try:
                                print'file name...', file_item
                                if ftp_obj_item.protocol=='ftp':
                                    fobj = open(full_path(base_url,file_item), 'rb') ## store the files in a specific location
                                    write_status = ftp.storbinary('STOR '+file_item, fobj) ## handle exception (550 File not found) - wrong file name
                                    print write_status ##226 Successfully transferred "/workfile.csv"
                                    fobj.close()
                                elif ftp_obj_item.protocol=='sftp':
                                    file_path = os.path.join(base_url, file_item)
#                                    with pysftp.Connection(**cinfo) as sftp:
#                                    sftp.put(file_path,'./01536')
                                    remote_path = '01536/'+file_item
#                                    sftp.put(file_path,remote_path)
                                    with sftp.cd(unit_directory):
                                        sftp.put(file_path)
#                                    sftp.close()


                                os.remove(full_path(base_url,file_item))

                            except Exception, e:  #you can specify type of Exception also
                                _logger.error('Exception: %s',str(e))
                                print "exception inner",str(e)
                                return {"error":str(e)}
                                        ###close the first transaction log on exception and start new one
                    except Exception, e:  #you can specify type of Exception also
                        _logger.info('FTP Exception Handler: %s',str(e))
                        print "exception middle",str(e)
                        return {"error":str(e)}
                    finally:
                        if ftp:
                            ftp.quit()
                            _logger.info('FTP Quit in Finally')
                        elif sftp:
                            sftp.close()
                            _logger.info('SFTP Quit in Finally')
        except Exception, e:  #you can specify type of Exception also
            _logger.info('FTP Exception Handler: %s',str(e))
            print "exception outer",str(e)
            return {"error":str(e)}

        return {"error":False}

    def send_email(self, cr, uid, data,context=None):
        if context is None:
            context = {}

        mail_obj = self.pool['mail.mail']

        model_obj = self.pool.get('ir.model.data')
        mail_template_id = model_obj.get_object(cr, uid, 'pos_connect_ecw', 'emis_export_email_template')
        data_attach = {}
        template_values = {}

        context.update({'active_id': data['id'],'active_model': 'emis.connect','default_model': 'emis.connect'})

        if data.get('is_error'):
            template_values = self.pool.get('mail.template').generate_email(cr, uid, mail_template_id.id, data['id'], context=context)
            template_values['subject'] = data.get('subject',"EMIS Export Failed")
            template_values['body_html'] = data.get('body')
        else:
            with open(data['zip'], "rb") as file:
                encoded_string = base64.b64encode(file.read())

            data_attach = {
                'name': 'EMIS Export File',
                'datas': encoded_string,
                'datas_fname': data['name']+".zip",
                'description': 'EMIS Export ' + data['name'],
                'res_model': 'emis.connect',
                'res_id': data['id']
            }

            template_values = self.pool.get('mail.template').generate_email(cr, uid, mail_template_id.id, data['id'], context=context)
            template_values['attachment_ids'] = [(0, 0, data_attach)]

        mail_id =  mail_obj.create(cr, uid, template_values)
        mail_obj.send(cr,uid,[mail_id])
        time.sleep(2)
        return True

    def sync_data(self, cr, uid, ids=False):
        company_brw = self.pool.get('res.users').browse(cr,uid,uid).company_id
        BUSINESS_UNIT   =   company_brw.business_unit # "6000"
        UNIT_ID         =   company_brw.unit_id # "01533"
        SHIFT_PFX       =   company_brw.shift_prefix # "H298"
        SALES_PFX       =   company_brw.sales_prefix # "C298"
        RETURN_PFX      =   company_brw.return_prefix # "N298"
        SUPERVISOR      =   company_brw.supervisor # "6000001842"
        SUPERVISOR_PWD  =   company_brw.supervisor_pwd # "JEFFREY1842"
        print "CSV Export Begins..."

        cash_difference_orders = []
        inprogress_shifts_only = True
        available_sales_orders = False
        # SALE ORDER DATA UPLOAD BEGINS

        emis_connect_obj = self.pool.get('emis.connect')
        supervisor_session_obj = self.pool.get('supervisor.session')

        emis_connect_dict = {}
        export_seq = self.pool.get('ir.sequence').get(cr, uid, 'emis.connect')
        emis_connect_dict['name'] = UNIT_ID + " - " + str(export_seq) + " - " + datetime.datetime.now().strftime("%Y_%m_%d")
        emis_connect_id = emis_connect_obj.create(cr,uid,emis_connect_dict)
        emis_connect_brw = emis_connect_obj.browse(cr,uid,emis_connect_id)

        ftp_config_obj = self.pool.get('ftp.config')
        ftp_config_ids = ftp_config_obj.search(cr, uid,[])

        if not ftp_config_ids:
            _logger.info('No ftp config found !....')
            subject = "EMIS Export Failed - " + emis_connect_dict['name']
            error_msg_body = "Error : FTP Configuration Not Available"
            data = {'subject':subject,'body':error_msg_body,'is_error':True,'id':emis_connect_id}
            return self.send_email(cr, uid, data)
        else:
            emis_connect_brw.email_to = ftp_config_obj.browse(cr,uid,ftp_config_ids[0]).emails
        #query = "SELECT DISTINCT start_at::date FROM pos_session where (state = 'closed') AND (is_synced = False) order by start_at"

        session_ids = self.pool.get('pos.session').search(cr,uid,[('state','in',('opened','closed')),('is_synced','=',False)],order='start_at')

        ###if there is no data, exit
        if not session_ids:
            print "No data to be exported"
            return False
        #session_date = datetime.datetime.strptime(super_session_date, '%Y-%m-%d')
        session_brws = self.pool.get('pos.session').browse(cr,uid,session_ids)
        session_data_dict = {}
        for session_brw in session_brws:
            session_date = change_tz(session_brw.start_at).strftime('%Y-%m-%d')
            if session_data_dict.get(session_date,False):
                print session_date
                print 'session id', session_brw.id
                session_data_dict[session_date].append(session_brw.id)
            else:
                session_data_dict[session_date]= [session_brw.id]

        session_dates = sorted(session_data_dict.keys())
        print session_dates

#         cr.execute(query)
#         session_date_list = cr.fetchall()
#         session_dates = [element for tupl in session_date_list for element in tupl]

        card_ids = self.pool.get('account.journal').search(cr,uid,[('card_type','=',True)])

        base_url = self.pool.get('ir.config_parameter').get_param(cr, uid, 'emis_export_path')

        hd = os.path.join(base_url,'OL1_MF_SO_HD_TMP.csv')
        ln = os.path.join(base_url,'OL1_MF_SO_LN_TMP.csv')
        lnd = os.path.join(base_url,'OL1_MF_SO_LN_DETAIL_TMP.csv')
        pay = os.path.join(base_url,'OL1_MF_SO_PAY_TMP.csv')
        st = os.path.join(base_url,'OL1_MF_SHIFT_TRACKING_TMP.csv')
        man1 = os.path.join(base_url,'OL1_MF_SHIFT_MANHOUR_HD_TMP.csv')
        s_hd = os.path.join(base_url,'OL1_MF_UNIT_SHIFT_HD_TMP.csv')
        s_op = os.path.join(base_url,'OL1_MF_UNIT_SHIFT_OPEN_TMP.csv')
        s_op_emp = os.path.join(base_url,'OL1_MF_UNIT_SHIFT_OPEN_EMP_TMP.csv')
        sh1 = os.path.join(base_url,'OL1_MF_UNIT_SHIFT_EMPLOYEE_TMP.csv')
        man5 = os.path.join(base_url,'OL1_MF_SHIFT_MANHOUR_LN4_HD_TMP.csv')
        man6 = os.path.join(base_url,'OL1_MF_SHIFT_MANHOUR_LN4_TMP.csv')


        with open(hd, 'wb') as salecsvfile , \
            open(ln, 'wb') as linecsvfile, \
            open(lnd,'wb') as linedetailfile, \
            open(pay, 'wb') as paymentcsvfile, \
            open(st, 'wb') as shifttrackingfile, \
            open(man1, 'wb') as super_shift_closing_file, \
            open(s_hd, 'wb') as super_shift_opening_file, \
            open(s_op, 'wb') as cashier_shift_opening_file, \
            open(s_op_emp, 'wb') as cashier_shift_open_emp_file, \
            open(sh1, 'wb') as cashier_shift_closing_file, \
            open(man5, 'wb') as credit_card_hd_file, \
            open(man6, 'wb') as credit_card_ln_file :

            try:
                ################# order

                fieldnames = ('SOH_BUS_UNIT', 'SOH_PFX', 'SOH_NO','SOH_FROM','SOH_CODE','SOH_SALES_TYPE','SOH_CLS',
                              'SOH_TYPE','SOH_DATE','SOH_YEAR','SOH_PERIOD','SOH_CUSTOMER','SOH_CUSTOMER_NAME','SOH_CUSTOMER_EMP',
                              'SOH_TERMS','SOH_CURRENCY','SOH_EXCHANGE_DATE','SOH_EXCHANGE_RATE','SOH_EMPLOYEE','SOH_SALESPERSON',
                              'SOH_WHSE','SOH_POST','SOH_SHIFT_PFX','SOH_SHIFT_NO','SOH_DISC_CARD','SOH_DISC_PCT','SOH_DISC_AMT',
                              'SOH_INV_DISC_FLAG','SOH_INV_DISC_PCT','SOH_LEFT_CASH','SOH_LEFT_RETURN','SOH_CASH_GROUP','SOH_RET_BUS_UNIT',
                              'SOH_RET_UNIT','SOH_RET_INV_PFX','SOH_RET_INV_NO','SOH_EXCLUDE_CARD_CHRG','SOH_CRE_BY','SOH_CRE_DATE',
                              'SOH_NOTES','SOH_CREDIT_HOLD','SOH_HOLD_REASON','SOH_OVER_DUE_AMT','SOH_PRINT_DATE','SOH_AR_DOC_PFX',
                              'SOH_AR_DOC_NO','SOH_DISC_LOCK','SOH_REFRESHED','SOH_ALLOW_MODIFY','SOH_REFRESH_DATE','SOH_SALESMAN',
                              'SOH_DIR_DEL_FLAG','SOH_SV_CUSTOMER','SOH_PIN_CODE','SOH_CENTRAL_DISC_CARD','SOH_COUNTER_NUMBER',
                              'SOH_SHIFT_NUMBER')

                writer = csv.DictWriter(salecsvfile, fieldnames=fieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                headers = dict( (n,n) for n in fieldnames )
                writer.writerow(headers)
                ################## line

                linefieldnames = ('SOL_BUS_UNIT', 'SOL_PFX', 'SOL_NO','SOL_LN','SOL_BAR_CODE','SOL_ITEM','SOL_ITEM_REV',
                      'SOL_WHSE','SOL_QTY','SOL_UOM','SOL_UOM_STOCK','SOL_FACTOR','SOL_COST','SOL_PRICE',
                      'SOL_DISC_PCT','SOL_FREE_QTY','SOL_CNS_PFX','SOL_CNS_NO','SOL_CNS_LN','SOL_CRE_BY',
                      'SOL_CRE_DATE','SOL_NOTES','SOL_SALESMAN','SOL_DISC_AMT','SOL_PRODUCTION_FLAG','SOL_PRODUCTION_QTY'
                     )

                line_writer = csv.DictWriter(linecsvfile, fieldnames=linefieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                line_headers = dict( (fn,fn) for fn in linefieldnames )
                line_writer.writerow(line_headers)

                ################## line detail

                linedetailfieldnames = ('SOLD_BUS_UNIT', 'SOLD_UNIT', 'SOLD_WHSE','SOLD_SHIFT_NO','SOLD_PFX','SOLD_NO','SOLD_LN',
                      'SOLD_LN1','SOLD_BAR_CODE','SOLD_ITEM','SOLD_ITEM_REV','SOLD_ITEM_CAT','SOLD_QTY','SOLD_UOM',
                      'SOLD_UOM_STOCK','SOLD_FACTOR','SOLD_COST','SOLD_PRICE','SOLD_BOM_ITEM','SOLD_BOM_ITEM_REV',
                      'SOLD_BOM_UOM','SOLD_BOM_QTY','SOLD_BOM_TOTQTY','SOLD_BOM_COST','SOLD_TYPE','SOLD_CLS','SOLD_SHIFT_PFX',
                      'SOLD_SHIFT_DATE','SOLD_YEAR','SOLD_PERIOD'
                     )

                linedetail_writer = csv.DictWriter(linedetailfile, fieldnames=linedetailfieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                linedetail_headers = dict( (fn,fn) for fn in linedetailfieldnames )
                linedetail_writer.writerow(linedetail_headers)

                ################# payment

                paymentfieldnames = ('PAY_BUS_UNIT', 'PAY_PFX', 'PAY_NO','PAY_LN','PAY_PAY','PAY_CURRENCY','PAY_EXCHANGE_DATE',
                      'PAY_EXCHANGE_RATE','PAY_FC_VALUE','PAY_AMT','PAY_CRED_CARD_CHRG','PAY_AMT_GL','PAY_LC_CHANGE','PAY_CARD_NO','PAY_CARD_TRANS_NO',
                      'PAY_PERSON_NAME','PAY_NATIONALITY','PAY_PASSPORT_NO','PAY_FLIGHT_NO','PAY_CHECK_NO','PAY_CHECK_DATE',
                      'PAY_CRE_BY','PAY_CRE_DATE','PAY_NOTES'
                     )

                payment_writer = csv.DictWriter(paymentcsvfile, fieldnames=paymentfieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                payment_headers = dict( (n,n) for n in paymentfieldnames )
                payment_writer.writerow(payment_headers)

                ################# super_shift_opening

                super_shift_opening_fields = ('SHFTH_BUS_UNIT', 'SHFTH_PFX', 'SHFTH_NO','SHFTH_UNIT','SHFTH_SHIFT','SHFTH_DATE','SHFTH_YEAR',
                      'SHFTH_PERIOD','SHFTH_SV_EMPLOYEE','SHFTH_PASSWORD','SHFTH_LEFT_CASH','SHFTH_POST','SHFTH_CRE_BY','SHFTH_CRE_DATE','SHFTH_NOTES'
                     )

                super_shift_opening_writer = csv.DictWriter(super_shift_opening_file, fieldnames=super_shift_opening_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                super_shift_opening_headers = dict( (n,n) for n in super_shift_opening_fields )
                super_shift_opening_writer.writerow(super_shift_opening_headers)

                ################# cashier_shift_opening

                cashier_shift_opening_fields = ('SHFTO_BUS_UNIT', 'SHFTO_PFX', 'SHFTO_NO','SHFTO_EMP','SHFTO_LN','SHFTO_PAYMENT','SHFTO_CURRENCY',
                      'SHFTO_EXCHANGE_DATE','SHFTO_EXCHANGE_RATE','SHFTO_AMT','SHFTO_AMT_GL','SHFTO_CRE_BY','SHFTO_CRE_DATE','SHFTO_NOTES'
                     )

                cashier_shift_opening_writer = csv.DictWriter(cashier_shift_opening_file, fieldnames=cashier_shift_opening_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                cashier_shift_opening_headers = dict( (n,n) for n in cashier_shift_opening_fields )
                cashier_shift_opening_writer.writerow(cashier_shift_opening_headers)

                ################# cashier_shift_opening_emp

                cashier_shift_open_emp_fields = ('SHFTE_BUS_UNIT', 'SHFTE_PFX', 'SHFTE_NO','SHFTE_EMP','SHFTE_POST','SHFTE_CRE_BY','SHFTE_CRE_DATE',
                      'SHFTE_NOTES'
                     )

                cashier_shift_open_emp_writer = csv.DictWriter(cashier_shift_open_emp_file, fieldnames=cashier_shift_open_emp_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                cashier_shift_open_emp_headers = dict( (n,n) for n in cashier_shift_open_emp_fields )
                cashier_shift_open_emp_writer.writerow(cashier_shift_open_emp_headers)

                ################# cashier_shift_closing

                cashier_shift_closing_fields = ('USE_BUS_UNIT', 'USE_SHIFT_PFX', 'USE_SHIFT_NO','USE_EMPLOYEE','USE_SHIFT_NO1','USE_SHIFT_DATE','USE_OPEN_AMT',
                      'USE_CASH_SALE','USE_CASH_INV','USE_CR_SALE','USE_CR_INV','USE_CASH_RET_SALE','USE_CASH_RET_INV','USE_CR_RET_SALE','USE_CR_RET_INV','USE_AIRLINE_SALE',
                      'USE_MANAGEMNET_SALE','USE_CLOSING_CASH','USE_CLOS_CASH_50H','USE_CLOS_CASH_1R','USE_CLOS_CASH_5R','USE_CLOS_CASH_10R',
                      'USE_CLOS_CASH_20R','USE_CLOS_CASH_50R','USE_CLOS_CASH_100R','USE_CLOS_CASH_500R','USE_FOR_CURR_AMT','USE_LEFT_CHANGE','USE_SHIFT_STATUS',
                      'USE_CRE_DATE','USE_CRE_BY','USE_NOTES','USE_COUNTER_NUMBER','USE_UNIT','USE_SUPERVISOR','USE_CLOSING_DATE',
                      'USE_CLOS_CASH_200R','USE_CLOS_CASH_1000R','USE_FIRST_INV','USE_LAST_INV','USE_NET_SALES','USE_SHIFT','USE_REASON','USE_NO','USE_TOTAL_SALES',
                      'USE_TOTAL_RETURN','USE_TOTAL_SALES_INV','USE_TOTAL_RETURN_INV','USE_NET_INV','USE_DIFF_AMT','USE_NET_DRAWER_AMT','USE_PETTY_CASH'
                     )

                cashier_shift_closing_writer = csv.DictWriter(cashier_shift_closing_file, fieldnames=cashier_shift_closing_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                cashier_shift_closing_headers = dict( (n,n) for n in cashier_shift_closing_fields )
                cashier_shift_closing_writer.writerow(cashier_shift_closing_headers)

                ################# credit_card_header

                credit_card_hd_fields = ('SML4H_BUS_UNIT', 'SML4H_UNIT', 'SML4H_SHIFT_PFX','SML4H_SHIFT_NO','SML4H_TRANS_NO','SML4H_AMOUNT','SML4H_CRE_DT',
                      'SML4H_CRE_BY','SML4H_NOTES')

                credit_card_hd_writer = csv.DictWriter(credit_card_hd_file, fieldnames=credit_card_hd_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                credit_card_hd_headers = dict( (n,n) for n in credit_card_hd_fields )
                credit_card_hd_writer.writerow(credit_card_hd_headers)

                ################# credit_card_ln

                credit_card_ln_fields = ('SML4_BUS_UNIT', 'SML4_UNIT', 'SML4_SHIFT_PFX','SML4_SHIFT_NO','SML4_TRAN_NO','SML4_TYPE','SML4_AMOUNT',
                      'SML4_CRE_DT','SML4_CRE_BY','SML4_NOTES','SML4_QTY')

                credit_card_ln_writer = csv.DictWriter(credit_card_ln_file, fieldnames=credit_card_ln_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                credit_card_ln_headers = dict( (n,n) for n in credit_card_ln_fields )
                credit_card_ln_writer.writerow(credit_card_ln_headers)

                ################ supervisor closing

                supervisor_shift_closing_fields = ('SMH_BUS_UNIT', 'SMH_UNIT', 'SMH_SHIFT_PFX','SMH_SHIFT_NO','SMH_TOTAL_EMP','SMH_SHIFT_DATE','SMH_SHIFT',
                      'SMH_500_CNT','SMH_200_CNT','SMH_100_CNT','SMH_50_CNT','SMH_20_CNT','SMH_10_CNT','SMH_5_CNT','SMH_1_CNT',
                      'SMH_50H_CNT','SMH_LEFT_CHANGE','SMH_AIRLINES_CR','SMH_MANAGEMENT_CR','SMH_1000_CNT'
                     )

                supervisor_shift_closing_writer = csv.DictWriter(super_shift_closing_file, fieldnames=supervisor_shift_closing_fields,quotechar = '"',quoting=csv.QUOTE_ALL)
                supervisor_shift_closing_headers = dict( (n,n) for n in supervisor_shift_closing_fields )
                supervisor_shift_closing_writer.writerow(supervisor_shift_closing_headers)

                ################## Shift Tracking
                shift_tracking_fieldnames = ('MST_BUS_UNIT', 'MST_SHIFT_PFX', 'MST_SHIFT_NO','MST_HD','MST_LN','MST_PAY','MST_LND')

                shift_tracking_writer = csv.DictWriter(shifttrackingfile, fieldnames=shift_tracking_fieldnames,quotechar = '"',quoting=csv.QUOTE_ALL)
                shift_tracking_headers = dict( (n,n) for n in shift_tracking_fieldnames )
                shift_tracking_writer.writerow(shift_tracking_headers)

                #################### logic to export csv files

                for super_session_date in session_dates:

                    session_date = datetime.datetime.strptime(super_session_date, '%Y-%m-%d' )
                    search_start_date   = session_date.strftime('%Y-%m-%d %H:%M:%S')
                    search_end_date     = session_date.strftime('%Y-%m-%d 23:59:59')

                    cashier_shift_ids = sorted(session_data_dict[super_session_date])
                    cashier_shift_obj = self.pool['pos.session']
#                     cashier_shift_ids = cashier_shift_obj.search(cr,uid,[('start_at','>=',search_start_date),('start_at','<=',search_end_date),('is_synced','=',False)],context=context)
                    order_hd_count  =   0
                    order_ln_count  =   0
                    order_py_count  =   0
                    order_lnd_count =   0
                    supervisor_mgt_credit = 0
                    supervisor_cash_sales = 0
                    supervisor_card_sales = 0

                    card_wise_sales = {}
                    card_wise_no_sales = {}

                    if cashier_shift_ids:
                        #### Supervisor Shift Opening
                        cashier_shift_brws = cashier_shift_obj.browse(cr,uid,sorted(cashier_shift_ids))
                        super_shift_opening_dict = {}
                        fixed_super_shift_dict = {"SHFTH_BUS_UNIT":BUSINESS_UNIT,"SHFTH_UNIT":UNIT_ID,"SHFTH_PFX":SHIFT_PFX,
                                                 "SHFTH_SHIFT":"DAY","SHFTH_LEFT_CASH":0,"SHFTH_CRE_BY":UNIT_ID}
                        #sv_date = datetime.datetime.strptime(cashier_shift_brws[0].start_at, '%Y-%m-%d %H:%M:%S' )
                        sv_date = change_tz(cashier_shift_brws[0].start_at)

                        super_shift_opening_dict['SHFTH_DATE'] = sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                        super_shift_opening_dict['SHFTH_YEAR'] = sv_date.strftime('%Y')
                        super_shift_opening_dict['SHFTH_PERIOD'] = sv_date.strftime('%m')
                        super_shift_opening_dict['SHFTH_SV_EMPLOYEE'] = SUPERVISOR
                        super_shift_opening_dict['SHFTH_PASSWORD'] = SUPERVISOR_PWD
                        super_shift_opening_dict['SHFTH_POST'] = "C"
                        super_shift_opening_dict['SHFTH_CRE_DATE'] = sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                        super_shift_opening_dict['SHFTH_NOTES'] = ""

                        if cashier_shift_brws[0].supershift_id:
                            print 'Supervisor shift ID if = ',cashier_shift_brws[0].supershift_id
                            super_shift_opening_dict['SHFTH_NO'] = cashier_shift_brws[0].supershift_id
                        else:
                            super_shift_opening_dict['SHFTH_NO'] = self.pool.get('ir.sequence').get(cr, uid, 'supervisor.shift')
                            print 'Supervisor shift ID else = ',super_shift_opening_dict['SHFTH_NO']
                            self.pool.get('pos.session').write(cr,uid,cashier_shift_brws[0].id,{'supershift_id':super_shift_opening_dict['SHFTH_NO']})

                        super_shift_opening_dict.update(fixed_super_shift_dict)
                        super_shift_opening_writer.writerow(super_shift_opening_dict)

                        #####
                        supervisor_session_vals = {
                                                     'emis_export_id':emis_connect_id,
                                                     'session_start_date':session_date,
                                                     'session_id':super_shift_opening_dict['SHFTH_NO'],
                                                     'cashier_session_ids':[(6, 0, cashier_shift_ids)],
                                                     }
                        supervisor_session_obj.create(cr,uid,supervisor_session_vals)
                        ###
                        ####
                        supervisor_left_change = 0

                        #### Cashier Shift
                        csb_ln = 1
                        for csb in cashier_shift_brws:
                            #### Cashier Shift Opening

                            cashier_left_change = 0
                            mgt_credit = 0
                            change_return_amount = 0
                            card_sale_amount = 0
                            cash_sale_amount = 0

                            actual_sales = 0
                            cash_difference = 0

                            cash_sale_invoice_ids = []
                            card_sale_invoice_ids = []
                            credit_sale_invoice_ids = []

                            cashier_shift_opening_dict = {}
                            fixed_cashier_shift_dict = {"SHFTO_BUS_UNIT":BUSINESS_UNIT,"SHFTO_PFX":SHIFT_PFX,"SHFTO_EXCHANGE_RATE":"1",
                                                     "SHFTO_PAYMENT":"CASH","SHFTO_CURRENCY":"DEFLT","SHFTO_CRE_BY":UNIT_ID,
                                                     "SHFTO_AMT":"0","SHFTO_AMT_GL":"0"
                                                     }

                            #ca_date = datetime.datetime.strptime(csb.start_at, '%Y-%m-%d %H:%M:%S' )
                            ca_date = change_tz(csb.start_at)
                            cashier_shift_opening_dict['SHFTO_NO'] = super_shift_opening_dict['SHFTH_NO']
                            cashier_shift_opening_dict['SHFTO_EMP'] = csb.user_id.emis_id
                            cashier_shift_opening_dict['SHFTO_LN'] = csb_ln
                            cashier_shift_opening_dict['SHFTO_EXCHANGE_DATE'] = ca_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_opening_dict['SHFTO_CRE_DATE'] = ca_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_opening_dict['SHFTO_NOTES'] = ""

                            cashier_shift_opening_dict.update(fixed_cashier_shift_dict)
                            if csb.state == "opened":
                                pass
                            elif csb.state == "closed":
                                cashier_shift_opening_writer.writerow(cashier_shift_opening_dict)
                            ####

                            #### Cashier Shift EMP
                            cashier_shift_emp_dict = {}
                            fixed_cashier_shift_emp_dict = {"SHFTE_BUS_UNIT":BUSINESS_UNIT,"SHFTE_PFX":SHIFT_PFX,"SHFTE_POST":"O",
                                                        "SHFTE_CRE_BY":UNIT_ID,
                                                     }

                            #cae_date = datetime.datetime.strptime(csb.start_at, '%Y-%m-%d %H:%M:%S' )
                            cae_date = change_tz(csb.start_at)
                            cashier_shift_emp_dict['SHFTE_NO'] = super_shift_opening_dict['SHFTH_NO']
                            cashier_shift_emp_dict['SHFTE_EMP'] = csb.user_id.emis_id
                            cashier_shift_emp_dict['SHFTE_CRE_DATE'] = cae_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_emp_dict['SHFTE_NOTES'] = ""

                            cashier_shift_emp_dict.update(fixed_cashier_shift_emp_dict)
                            if csb.state == "opened":
                                pass
                            elif csb.state == "closed":
                                cashier_shift_open_emp_writer.writerow(cashier_shift_emp_dict)
                            ####
                            pos_order_obj = self.pool['pos.order']
                            pos_orders = pos_order_obj.search(cr,uid,[('session_id','=',csb.id)])
                            #pos_orders = [3597,3598,3609]
                            orders_brws = []
                            if pos_orders:
                                orders_brws = pos_order_obj.browse(cr,uid,sorted(pos_orders))
                                available_sales_orders = True


                            cashier_journal_wise_amount_in = {}
                            cashier_journal_wise_amount_out = {}
                            cashier_journal_wise_total = {}

                            #### Sale order Header
                            for res in orders_brws:
                                sales_amount = 0
                                actual_sales_amount = 0
                                if not res.state in ['paid','done','invoiced']:
                                    continue

                                if res.amount_total==0 and not res.lines:
                                    continue
                                left_change = 0.0
                                sale_dict = {}

                                fixed_sale_dict = {"SOH_BUS_UNIT":BUSINESS_UNIT,"SOH_PFX":SALES_PFX ,"SOH_FROM":"UNIT" ,
                                              "SOH_CODE" : UNIT_ID,"SOH_SALES_TYPE":"I","SOH_CUSTOMER":"DEFLT","SOH_CUSTOMER_NAME":"DEFLT",
                                              "SOH_CUSTOMER_EMP":"DEFLT","SOH_TERMS":"DEFLT","SOH_CURRENCY":"DEFLT","SOH_EXCHANGE_RATE":"1",
                                              "SOH_SALESPERSON":"DEFLT","SOH_WHSE":UNIT_ID,"SOH_POST":"N","SOH_DISC_PCT":"0","SOH_DISC_AMT":"0",
                                              "SOH_INV_DISC_FLAG":"N","SOH_INV_DISC_PCT":"0","SOH_LEFT_CASH":"0","SOH_LEFT_RETURN":"N",
                                              "SOH_CASH_GROUP":"P","SOH_CRE_BY":UNIT_ID,"SOH_SHIFT_PFX":SHIFT_PFX}

                                ### type of sale (sales/return), payment type (cash/Credit)
                                sale_dict['SOH_TYPE'] = "C"
                                sale_dict['SOH_CLS'] = "N"

                                val1 =  0.0
                                for line in res.lines:
                                    val1 += (line.qty * line.price_unit)

                                if res.amount_total == 0 and val1>0:
                                    mgt_credit += val1
                                    supervisor_mgt_credit +=val1
                                    credit_sale_invoice_ids.append(res.id)
                                    sale_dict['SOH_TYPE'] = "T"

                                stmt_ids = res.statement_ids
                                for stmt in res.statement_ids:
                                    if stmt.amount < 0 and stmt.pos_statement_id.order_parent_id:
                                        sale_dict['SOH_CLS'] = "C"
                                        sale_dict["SOH_RET_BUS_UNIT"] = BUSINESS_UNIT
                                        sale_dict["SOH_RET_UNIT"] = UNIT_ID
                                        sale_dict["SOH_RET_INV_PFX"] = RETURN_PFX
                                        sale_dict["SOH_RET_INV_NO"] = stmt.pos_statement_id.order_parent_id.id
                                    else:
                                        sale_dict['SOH_CLS'] = "N"

                                    if(stmt.amount > 0) and ( stmt.journal_id.name == 'Credit Management' or stmt.journal_id.name == 'COMPLEMENTARY' ):
                                        sale_dict['SOH_TYPE'] = "T"

                                sale_dict["SOH_NO"] = res.id or "" ###change the no
                                #d = datetime.datetime.strptime(res.date_order, '%Y-%m-%d %H:%M:%S' )
                                d = change_tz(res.date_order)
                                sale_dict["SOH_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_YEAR"] = d.strftime('%Y')
                                sale_dict["SOH_PERIOD"] = d.strftime('%m')
                                sale_dict["SOH_CUSTOMER"] = res.partner_id.emis_id or "DEFLT"
                                sale_dict["SOH_CUSTOMER_NAME"] = res.partner_id and res.partner_id.name or ""
                                sale_dict["SOH_EXCHANGE_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_EMPLOYEE"] = csb.user_id.emis_id or ""
                                sale_dict["SOH_SHIFT_NO"] = super_shift_opening_dict['SHFTH_NO']

                                sale_dict["SOH_EXCLUDE_CARD_CHRG"] = "N"
                                sale_dict["SOH_CRE_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_NOTES"] = "C" ###customer C , Staff S
                                sale_dict["SOH_CREDIT_HOLD"] = "D"
                                sale_dict["SOH_HOLD_REASON"] = ""
                                sale_dict["SOH_OVER_DUE_AMT"] = "0"
                                sale_dict["SOH_PRINT_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_AR_DOC_PFX"]=""
                                sale_dict["SOH_AR_DOC_NO"] = ""
                                sale_dict["SOH_DISC_LOCK"] = "O"
                                sale_dict["SOH_REFRESHED"] = "N"
                                sale_dict["SOH_ALLOW_MODIFY"]= "Y"
                                sale_dict["SOH_REFRESH_DATE"] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                sale_dict["SOH_SALESMAN"] = csb.user_id.emis_id or ""
                                sale_dict["SOH_DIR_DEL_FLAG"] = "N"
                                sale_dict["SOH_SV_CUSTOMER"] = ""
                                sale_dict["SOH_PIN_CODE"]= ""
                                sale_dict["SOH_CENTRAL_DISC_CARD"]= ""
                                sale_dict["SOH_COUNTER_NUMBER"] = "1" ### counter no
                                sale_dict["SOH_SHIFT_NUMBER"] = csb_ln  ###cashier shift no autogenerated

                                sale_dict.update(fixed_sale_dict)
                                writer.writerow(sale_dict)
                                ###end of sale order

                                ## start of sale order lines
                                counter = 1
                                for line in res.lines:
                                    discount = ((line.qty * line.price_unit) * (line.discount*0.01))
                                    if line.product_id.name == 'Tip':
                                        left_change += ((line.price_unit * line.qty) - discount)
                                        continue

                                    if line.qty == 0:
                                        continue
                                    line_dict = {}
                                    line_detail_dict = {}
                                    fixed_line_dict = {"SOL_BUS_UNIT":BUSINESS_UNIT,"SOL_PFX":SALES_PFX,"SOL_FACTOR":"1" ,"SOL_COST":"0" ,
                                              "SOL_DISC_PCT" : "0","SOL_FREE_QTY":"0","SOL_CNS_NO":"0",
                                              "SOL_CNS_LN":"0","SOL_DISC_AMT":"0", "SOL_CRE_BY":UNIT_ID,"SOL_WHSE":UNIT_ID,
                                              "SOL_ITEM_REV":"0",
                                              }

                                    fixed_linedetail_dict = {"SOLD_BUS_UNIT":BUSINESS_UNIT,"SOLD_UNIT":UNIT_ID,"SOLD_WHSE":UNIT_ID,"SOLD_PFX":SALES_PFX,
                                                             "SOLD_FACTOR":"1","SOLD_COST":"0","SOLD_BOM_COST":"0","SOLD_ITEM_REV":"0", "SOLD_BOM_ITEM_REV":"0"
                                              }

                                    ### start logic for sale order line
                                    line_dict['SOL_NO'] = res.id or ""
                                    line_dict['SOL_LN'] = counter
                                    line_dict['SOL_BAR_CODE'] = line.product_id.default_code or ""
                                    line_dict['SOL_ITEM'] = line.product_id.default_code or ''

                                    line_detail_dict['SOLD_LN'] =   counter
                                    line_detail_dict['SOLD_NO'] =   res.id
                                    line_detail_dict['SOLD_SHIFT_NO'] = super_shift_opening_dict['SHFTH_NO'] ###dynamic cashier shift no
                                    price_unit_ln = 0
                                    if line.price_unit - ((line.price_unit) * (line.discount*0.01))== 0:
                                        price_unit_ln = line.price_unit
                                    else:
                                        price_unit_ln = line.price_unit - ((line.price_unit) * (line.discount*0.01))
                                    line_detail_dict["SOLD_BAR_CODE"] = line.product_id.default_code or ""
                                    line_detail_dict["SOLD_ITEM"] = line.product_id.default_code or ""
                                    line_detail_dict["SOLD_ITEM_CAT"] = "90"
                                    line_detail_dict["SOLD_QTY"] = line.qty
                                    line_detail_dict["SOLD_UOM"] =  "PCS" #line.product_id.uom_id.name
                                    line_detail_dict["SOLD_UOM_STOCK"] = "PCS" #line.product_id.uom_po_id.name or
                                    line_detail_dict["SOLD_PRICE"] = price_unit_ln
                                    line_detail_dict["SOLD_TYPE"] = sale_dict['SOH_TYPE']
                                    line_detail_dict["SOLD_CLS"] = sale_dict['SOH_CLS']
                                    line_detail_dict["SOLD_SHIFT_PFX"] = SHIFT_PFX
                                    line_detail_dict["SOLD_SHIFT_DATE"] = sale_dict["SOH_DATE"]
                                    line_detail_dict["SOLD_YEAR"] = sale_dict["SOH_YEAR"]
                                    line_detail_dict["SOLD_PERIOD"] = sale_dict["SOH_PERIOD"]

                                    bom_sequence = 1

                                    bom_details = self.pool.get("product.bom.detail").search(cr,uid,[('parent_id','=',line.product_id.id)])
                                    bom_details_brws = self.pool.get("product.bom.detail").browse(cr,uid,bom_details)

                                    choice_ids = [dtl.product_id.id for dtl in line.line_detail]

#                                     for bom_detail in bom_details_brws:
#                                         line_detail_dict['SOLD_LN1'] = bom_sequence
#                                         line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                         line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                         line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                         line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                         line_detail_dict.update(fixed_linedetail_dict)
#                                         linedetail_writer.writerow(line_detail_dict)
#                                         order_lnd_count +=1
#                                         bom_sequence +=1
#                                         if not bom_detail.has_choice:
#                                             line_detail_dict['SOLD_LN1'] = bom_sequence
#                                             line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                             line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                             line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                             line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                             line_detail_dict.update(fixed_linedetail_dict)
#                                             linedetail_writer.writerow(line_detail_dict)
#                                             order_lnd_count +=1
#                                             bom_sequence +=1
#                                         elif bom_detail.product_id.id not in choice_ids:
#                                             line_detail_dict['SOLD_LN1'] = bom_sequence
#                                             line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                             line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                             line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                             line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                             line_detail_dict.update(fixed_linedetail_dict)
#                                             linedetail_writer.writerow(line_detail_dict)
#                                             order_lnd_count +=1
#                                             bom_sequence +=1
#                                         elif bom_detail.has_choice and bom_detail.product_id.id not in choice_ids:
#                                             line_detail_dict['SOLD_LN1'] = bom_sequence
#                                             line_detail_dict["SOLD_BOM_ITEM"] = bom_detail.product_id.default_code or ""
#                                             line_detail_dict["SOLD_BOM_UOM"] = bom_detail.uom
#                                             line_detail_dict["SOLD_BOM_QTY"] = bom_detail.qty
#                                             line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * bom_detail.qty
#                                             line_detail_dict.update(fixed_linedetail_dict)
#                                             linedetail_writer.writerow(line_detail_dict)
#                                             order_lnd_count +=1
#                                             bom_sequence +=1

                                    for dtl in line.line_detail:
                                        line_detail_dict['SOLD_LN1'] = bom_sequence
                                        line_detail_dict["SOLD_BOM_ITEM"] = dtl.product_id.default_code or ""
                                        bom_dtls = self.pool.get("product.bom.detail").search(cr,uid,[('parent_id','=',line.product_id.id),('product_id','=',dtl.product_id.id)])
                                        bom_dtls_brws = self.pool.get("product.bom.detail").browse(cr,uid,bom_dtls)
                                        if bom_dtls_brws:
                                            line_detail_dict["SOLD_BOM_UOM"] = bom_dtls_brws[0].uom
                                        line_detail_dict["SOLD_BOM_QTY"] = dtl.qty
                                        line_detail_dict["SOLD_BOM_TOTQTY"] = line.qty * dtl.qty
                                        line_detail_dict.update(fixed_linedetail_dict)
                                        linedetail_writer.writerow(line_detail_dict)
                                        order_lnd_count +=1
                                        bom_sequence +=1


                                    line_dict['SOL_QTY'] = line.qty
                                    line_dict['SOL_UOM'] = "PCS" ##line.product_id.uom_id.name ##update the UOM in Odoo
                                    line_dict['SOL_UOM_STOCK'] = "PCS" ##line.product_id.uom_po_id.name ##update the UOM in Odoo
                                    line_dict['SOL_PRICE'] = price_unit_ln
                                    line_dict['SOL_CRE_DATE'] = d.strftime('%d-%m-%Y %I:%M:%S %p')
                                    line_dict['SOL_NOTES'] = ""
                                    line_dict['SOL_SALESMAN'] = csb.user_id.emis_id or ''
                                    counter = counter + 1

                                    sales_amount += line.qty * (line.price_unit - ((line.price_unit) * (line.discount*0.01)))
                                    actual_sales_amount += (line.qty * price_unit_ln)
                                    print sales_amount
                                    #print line.qty * line.price_unit

                                    ### end of logic for sale order lines
                                    line_dict.update(fixed_line_dict)
                                    line_writer.writerow(line_dict)

                                    order_ln_count +=1

                                actual_sales += actual_sales_amount
                                print 'actual sales = ',actual_sales
                                ## end of sale order lines

                                ## start of payment lines

                                journal_wise_amount_in = {}
                                journal_wise_amount_out = {}
                                journal_wise_total = {}
                                payment_amount = 0
                                for payment in stmt_ids:
                                    if payment.journal_id.name == "Credit Management" or payment.journal_id.name == "COMPLEMENTARY":
                                        mgt_credit +=  payment.amount
                                        supervisor_mgt_credit += abs(payment.amount)
                                        credit_sale_invoice_ids.append(res.id)
                                        payment_amount += payment.amount
                                        continue
                                    if payment.amount > 0:
                                        #if there is journal id present add again with current amount, else create a new with current payment amount
                                        journal_wise_amount_in[payment.journal_id.id] = (journal_wise_amount_in.get(payment.journal_id.id)+payment.amount)  if journal_wise_amount_in.get(payment.journal_id.id,False) else payment.amount
                                        journal_wise_total[payment.journal_id.id] = (journal_wise_total.get(payment.journal_id.id)+payment.amount)  if journal_wise_total.get(payment.journal_id.id,False) else payment.amount

                                        cashier_journal_wise_amount_in[payment.journal_id.id] = (cashier_journal_wise_amount_in.get(payment.journal_id.id)+payment.amount)  if cashier_journal_wise_amount_in.get(payment.journal_id.id,False) else payment.amount
                                        cashier_journal_wise_total[payment.journal_id.id] = (cashier_journal_wise_total.get(payment.journal_id.id)+payment.amount)  if cashier_journal_wise_total.get(payment.journal_id.id,False) else payment.amount
                                        if payment.journal_id.name == "Cash":
                                            cash_sale_invoice_ids.append(res.id)
                                        if payment.journal_id.card_type:
                                            cash_sale_invoice_ids.append(res.id)
                                            card_sale_invoice_ids.append(res.id)
                                            card_wise_no_sales[payment.journal_id.id] = (card_wise_no_sales.get(payment.journal_id.id)+1)  if card_wise_no_sales.get(payment.journal_id.id,False) else 1
                                    #Return Amount (Change)
                                    elif payment.amount < 0:
                                        journal_wise_amount_out[payment.journal_id.id] = (journal_wise_amount_out.get(payment.journal_id.id)+payment.amount)  if journal_wise_amount_out.get(payment.journal_id.id,False) else payment.amount
                                        journal_wise_total[payment.journal_id.id] = (journal_wise_total.get(payment.journal_id.id)+payment.amount)  if journal_wise_total.get(payment.journal_id.id,False) else payment.amount

                                        cashier_journal_wise_amount_out[payment.journal_id.id] = (cashier_journal_wise_amount_out.get(payment.journal_id.id)+payment.amount)  if cashier_journal_wise_amount_out.get(payment.journal_id.id,False) else payment.amount
                                        cashier_journal_wise_total[payment.journal_id.id] = (cashier_journal_wise_total.get(payment.journal_id.id)+payment.amount)  if cashier_journal_wise_total.get(payment.journal_id.id,False) else payment.amount

                                payment_sequence = 0
                                for key, value in journal_wise_total.iteritems():
                                    ### start logic for payment lines
                                    payment_sequence +=1
                                    payment_dict = {}
                                    fixed_payment_dict = {"PAY_BUS_UNIT"  : BUSINESS_UNIT,"PAY_PFX":SALES_PFX,"PAY_CRE_BY":UNIT_ID}
                                    payment_dict["PAY_NO"]= res.id
                                    payment_dict["PAY_LN"]= payment_sequence ###sequences
                                    payment_dict["PAY_PAY"]= self.pool.get('account.journal').browse(cr,uid,key).emis_id
                                    payment_dict["PAY_CURRENCY"]= "DEFLT"
                                    payment_dict["PAY_EXCHANGE_DATE"]= d.strftime('%d-%m-%Y %I:%M:%S %p')
                                    payment_dict["PAY_EXCHANGE_RATE"]= "1"
                                    payment_dict["PAY_FC_VALUE"]= journal_wise_amount_in.get(key,0)
                                    payment_dict["PAY_AMT"]= journal_wise_amount_in.get(key,0)
                                    payment_dict["PAY_CRED_CARD_CHRG"]= ""
                                    payment_dict["PAY_AMT_GL"]= res.amount_total - left_change

                                    payment_dict["PAY_LC_CHANGE"]= abs(journal_wise_amount_out.get(key,0))
                                    payment_dict["PAY_CARD_NO"]= ""
                                    payment_dict["PAY_CARD_TRANS_NO"]= ""

                                    payment_dict["PAY_PERSON_NAME"]=""
                                    payment_dict["PAY_NATIONALITY"]=""
                                    payment_dict["PAY_PASSPORT_NO"]=""
                                    payment_dict["PAY_FLIGHT_NO"]=""
                                    payment_dict["PAY_CHECK_NO"]=""
                                    payment_dict["PAY_CHECK_DATE"]=""

                                    payment_dict["PAY_CRE_DATE"]= d.strftime('%d-%m-%Y %I:%M:%S %p')
                                    payment_dict["PAY_NOTES"]= ""

                                    ### end of logic for payment lines
                                    payment_dict.update(fixed_payment_dict)
                                    payment_writer.writerow(payment_dict)

                                    payment_amount += (journal_wise_amount_in.get(key,0) - abs(journal_wise_amount_out.get(key,0)))

                                    order_py_count +=1

                                ## end of payment lines
                                if round((payment_amount-left_change),2) < round(sales_amount,2):
                                    cash_difference_orders.append(res.id)
                                cash_difference = (payment_amount - sales_amount)
                                print "order, cash difference",res.id,cash_difference
                                print "sales vs payment", sales_amount, payment_amount

                                order_hd_count +=1
                                cashier_left_change +=left_change
                            print "actual sales",actual_sales

                            change_return_amount = abs(sum(cashier_journal_wise_amount_out.values()))
                            print "change_return",change_return_amount
                            for key, value in cashier_journal_wise_total.iteritems():
                                if self.pool.get('account.journal').browse(cr,uid,key).name == "Cash":
                                    cash_sale_amount += cashier_journal_wise_amount_in.get(key,0)
                                if self.pool.get('account.journal').browse(cr,uid,key).card_type:
                                    card_sale_amount += cashier_journal_wise_amount_in.get(key,0)
                                    card_wise_sales[key] = (card_wise_sales.get(key) + cashier_journal_wise_amount_in.get(key,0))  if card_wise_sales.get(key,False) else cashier_journal_wise_amount_in.get(key,0)

                            print "cash",cash_sale_amount
                            print "card",card_sale_amount

                            if csb.state == "opened":
                                continue

                            #### Cashier Shift Closing
                            cashier_shift_close_dict = {}
                            fixed_cashier_shift_close_dict = {"USE_BUS_UNIT":BUSINESS_UNIT,"USE_SHIFT_PFX":SHIFT_PFX,"USE_UNIT":UNIT_ID,
                                                        "USE_CRE_BY":UNIT_ID }

                            #cac_date = datetime.datetime.strptime(csb.start_at, '%Y-%m-%d %H:%M:%S')
                            cac_date = change_tz(csb.start_at)
                            #cac_stop_date = datetime.datetime.strptime(csb.stop_at, '%Y-%m-%d %H:%M:%S')
                            cac_stop_date = change_tz(csb.stop_at)
                            cashier_shift_close_dict['USE_SHIFT_NO'] = super_shift_opening_dict['SHFTH_NO']
                            cashier_shift_close_dict['USE_EMPLOYEE'] = csb.user_id.emis_id
                            cashier_shift_close_dict['USE_SHIFT_NO1'] = csb_ln
                            cashier_shift_close_dict['USE_SHIFT_DATE'] = cac_date.strftime('%d-%m-%Y %I:%M:%S %p')

                            open_balance = csb.cash_register_balance_start
                            end_balance = csb.cash_register_balance_end_real
                            print 'open and close bal ',open_balance,end_balance
                            cashier_shift_close_dict['USE_OPEN_AMT'] = open_balance             #supervisor_cash_sales
                            #cashier_inputted_actual_sale = csb.total_sales
                            # cash_sale_real = end_balance - open_balance
                            # print 'Real cash Sale ',cash_sale_real
                            #use_cash_sale comes from POS system
                            cashier_shift_close_dict['USE_CASH_SALE'] = (cash_sale_amount - change_return_amount) + card_sale_amount - cashier_left_change            #cash_sale_real      #cashier_inputted_actual_sale #

                            cashier_shift_close_dict['USE_CASH_INV'] = len(set(cash_sale_invoice_ids + card_sale_invoice_ids))

                            cashier_shift_close_dict['USE_CR_SALE'] = mgt_credit #TODO + credit_sale
                            cashier_shift_close_dict['USE_CR_INV'] = "0" #len(set(credit_sale_invoice_ids))

                            cashier_shift_close_dict['USE_CASH_RET_SALE'] = "0"
                            cashier_shift_close_dict['USE_CASH_RET_INV'] = "0"

                            cashier_shift_close_dict['USE_CR_RET_SALE'] = "0"
                            cashier_shift_close_dict['USE_CR_RET_INV'] = "0"

                            cashier_shift_close_dict['USE_AIRLINE_SALE'] = "0"
                            #mgt_credit_real = csb.cash_register_id.cashbox_end_id.management_credit
                            cashier_shift_close_dict['USE_MANAGEMNET_SALE'] = mgt_credit

                            #Below data comes from cashier inpute while above data from POS

                            # print 'cashbox_line / management credit ',csb.cash_register_id.cashbox_end_id.management_credit
                            sale_total = 0
                            for close_dnm in csb.cash_register_id.cashbox_end_id.cashbox_lines_ids:#(cash_sale_amount - change_return_amount)
                                if(close_dnm.coin_value == 1):
                                    sale_total = close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_1R'] = close_dnm.number
                                if(close_dnm.coin_value == 5):
                                    sale_total += close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_5R'] = close_dnm.number
                                if(close_dnm.coin_value == 10):
                                    sale_total += close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_10R'] = close_dnm.number
                                if(close_dnm.coin_value == 20):
                                    sale_total += close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_20R'] = close_dnm.number
                                if(close_dnm.coin_value == 50):
                                    sale_total += close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_50R'] = close_dnm.number
                                if(close_dnm.coin_value == 100):
                                    sale_total += close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_100R'] = close_dnm.number
                                if(close_dnm.coin_value == 500):
                                    sale_total += close_dnm.subtotal
                                    cashier_shift_close_dict['USE_CLOS_CASH_500R'] = close_dnm.number

                            cash_sale_real = (sale_total - open_balance) + card_sale_amount
                            cashier_shift_close_dict['USE_CLOSING_CASH'] = sale_total   #(cash_sale_amount - change_return_amount)#(cash_sale_amount - change_return_amount)- cashier_left_change

                            cashier_shift_close_dict['USE_FOR_CURR_AMT'] = "0" ##no foreign currency
                            cashier_shift_close_dict['USE_LEFT_CHANGE'] = "0"

                            cashier_shift_close_dict['USE_SHIFT_STATUS'] = "C"

                            cashier_shift_close_dict['USE_CRE_DATE'] = cac_date.strftime('%d-%m-%Y %I:%M:%S %p')
                            cashier_shift_close_dict['USE_NOTES'] = ""

                            cashier_shift_close_dict['USE_COUNTER_NUMBER'] = "1" ##Need to dynamic

                            cashier_shift_close_dict['USE_SUPERVISOR'] = SUPERVISOR

                            cashier_shift_close_dict['USE_CLOSING_DATE'] = cac_stop_date.strftime('%d-%m-%Y %I:%M:%S %p')

                            cashier_shift_close_dict['USE_CLOS_CASH_200R'] = ""
                            cashier_shift_close_dict['USE_CLOS_CASH_1000R'] = card_sale_amount

                            cashier_shift_close_dict['USE_FIRST_INV'] = ""
                            cashier_shift_close_dict['USE_LAST_INV'] = ""

                            cashier_shift_close_dict['USE_NET_SALES'] = cash_sale_real + mgt_credit   #actual_sales#((cash_sale_amount - change_return_amount) + card_sale_amount + mgt_credit) - cashier_left_change

                            cashier_shift_close_dict['USE_SHIFT'] = "DAY" ### Need to be dynamic
                            cashier_shift_close_dict['USE_REASON'] = "Final Close"

                            cashier_shift_close_dict['USE_NO'] = "" ## Fixed

                            cashier_shift_close_dict['USE_TOTAL_SALES'] = cash_sale_real + mgt_credit     #actual_sales#(cash_sale_amount - change_return_amount) + card_sale_amount + mgt_credit - cashier_left_change
                            cashier_shift_close_dict['USE_TOTAL_RETURN'] = "0"

                            TOTAL_SALES_INV = cash_sale_invoice_ids + card_sale_invoice_ids + credit_sale_invoice_ids
                            # print 'invoices = ',len(cash_sale_invoice_ids),len(card_sale_invoice_ids),len(credit_sale_invoice_ids)

                            cashier_shift_close_dict['USE_TOTAL_SALES_INV'] = len(set(TOTAL_SALES_INV))
                            cashier_shift_close_dict['USE_TOTAL_RETURN_INV'] = 0 ## based on return invoice
                            cashier_shift_close_dict['USE_NET_INV'] = len(set(TOTAL_SALES_INV))

                            print 'cash_sale_amount = ',cash_sale_amount
                            diff_amount = csb.cash_register_difference         #closing_drawer_cash - (cash_sale_amount + csb.cash_register_balance_start)
                            print 'Diff amount = ',diff_amount
                            cashier_shift_close_dict['USE_DIFF_AMT'] = diff_amount
                            cashier_shift_close_dict['USE_NET_DRAWER_AMT'] = sale_total + mgt_credit      #end_balance#(cash_sale_amount - change_return_amount)
                            cashier_shift_close_dict['USE_PETTY_CASH'] = "0"

                            cashier_shift_close_dict.update(fixed_cashier_shift_close_dict)

                            cashier_shift_closing_writer.writerow(cashier_shift_close_dict)
                            csb_ln +=1

                            supervisor_cash_sales += ((cash_sale_amount - change_return_amount))
                            supervisor_card_sales += (card_sale_amount)

                        if cashier_shift_brws[0].state == "opened":
                            continue

                        #### Supervisor Shift Tracking
                        shift_tracking_dict = {}
                        shift_tracking_dict["MST_SHIFT_NO"] = super_shift_opening_dict['SHFTH_NO']
                        shift_tracking_dict["MST_BUS_UNIT"] = BUSINESS_UNIT
                        shift_tracking_dict["MST_SHIFT_PFX"] = SHIFT_PFX
                        shift_tracking_dict['MST_HD'] = str(order_hd_count)
                        shift_tracking_dict['MST_LN'] = str(order_ln_count)
                        shift_tracking_dict['MST_LND'] = str(order_lnd_count)
                        shift_tracking_dict['MST_PAY'] = str(order_py_count)

                        if csb.state == "opened":
                            pass
                        elif csb.state == "closed":
                            shift_tracking_writer.writerow(shift_tracking_dict)

                #### Supervisor Shift Closing
                super_shift_closing_dict = {}
                fixed_super_shift_close_dict = {"SMH_BUS_UNIT":BUSINESS_UNIT,"SMH_UNIT":UNIT_ID,"SMH_SHIFT_PFX":SHIFT_PFX}

                super_shift_closing_dict['SMH_SHIFT_DATE'] = sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                shift_num = super_shift_opening_dict['SHFTH_NO']
                super_shift_closing_dict['SMH_SHIFT_NO'] = shift_num

                sup_obj = self.pool['supervisor.shift']
                print 'supervisor shift id ',csb.supervisor_shift_id.id
                sup_srch = sup_obj.search(cr,uid,[('id','=',csb.supervisor_shift_id.id)])
                sup_shift_brws = sup_obj.browse(cr,uid,sup_srch)

                for sup_denom in sup_shift_brws.transaction_line_ids:
                    if sup_denom.coin_value == 1.00:
                        super_shift_closing_dict['SMH_1_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 5.00:
                        super_shift_closing_dict['SMH_5_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 10.00:
                        super_shift_closing_dict['SMH_10_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 20.00:
                        super_shift_closing_dict['SMH_20_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 50.00:
                        print 'here in 50 ', sup_denom.number
                        super_shift_closing_dict['SMH_50_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 100.00:
                        super_shift_closing_dict['SMH_100_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 500.00:
                        super_shift_closing_dict['SMH_500_CNT'] = sup_denom.number
                    if sup_denom.coin_value == 0.50:
                        super_shift_closing_dict['SMH_50H_CNT'] = sup_denom.number

                sum_cc = 0
                for cc_line in sup_shift_brws.card_transaction_lines:
                    sum_cc += cc_line.amount

                print 'Sum CC = ',sum_cc

                super_shift_closing_dict['SMH_LEFT_CHANGE'] = sup_shift_brws.total_left_change
                super_shift_closing_dict['SMH_AIRLINES_CR'] = "0"
                super_shift_closing_dict['SMH_MANAGEMENT_CR'] = sup_shift_brws.total_mgt_cr #supervisor_mgt_credit
                super_shift_closing_dict['SMH_1000_CNT'] = sum_cc
                super_shift_closing_dict['SMH_SHIFT'] = "DAY"
                super_shift_closing_dict['SMH_TOTAL_EMP'] = len(cashier_shift_ids)

                super_shift_closing_dict.update(fixed_super_shift_close_dict)
                supervisor_shift_closing_writer.writerow(super_shift_closing_dict)

                #### credit card hd
                cc_transaction_hd_dict = {}
                cc_transaction_hd_dict['SML4H_BUS_UNIT']    =  BUSINESS_UNIT
                cc_transaction_hd_dict['SML4H_UNIT']        =  UNIT_ID
                cc_transaction_hd_dict['SML4H_SHIFT_PFX']   =  SHIFT_PFX

                cc_transaction_hd_dict['SML4H_SHIFT_NO']    =   super_shift_opening_dict['SHFTH_NO']
                cc_transaction_hd_dict['SML4H_TRANS_NO']    =   sup_shift_brws.trans_no     #super_shift_opening_dict['SHFTH_NO']
                print 'credit card sale = ',supervisor_card_sales
                cc_transaction_hd_dict['SML4H_AMOUNT']      =   sum_cc
                cc_transaction_hd_dict['SML4H_CRE_DT']      =   sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                cc_transaction_hd_dict['SML4H_CRE_BY']      =   UNIT_ID
                cc_transaction_hd_dict['SML4H_NOTES']       =   ""

                credit_card_hd_writer.writerow(cc_transaction_hd_dict)

                cc_transaction_ln_dict = {}
                for cc_trans_line in sup_shift_brws.card_transaction_lines:
                    #### credit card ln
                    payment_type = self.pool.get('account.journal').browse(cr,uid,cc_trans_line.card_type.id)
                    cc_transaction_ln_dict['SML4_BUS_UNIT']     =   BUSINESS_UNIT
                    cc_transaction_ln_dict['SML4_UNIT']         =   UNIT_ID
                    cc_transaction_ln_dict['SML4_SHIFT_PFX']    =   SHIFT_PFX

                    cc_transaction_ln_dict['SML4_SHIFT_NO']     =   super_shift_opening_dict['SHFTH_NO']
                    cc_transaction_ln_dict['SML4_TRAN_NO']      =   sup_shift_brws.trans_no      #super_shift_opening_dict['SHFTH_NO']
                    cc_transaction_ln_dict['SML4_TYPE']         =   payment_type.emis_id     #payment_type.emis_id or ""
                    cc_transaction_ln_dict['SML4_AMOUNT']       =   cc_trans_line.amount        #card_wise_sales[key]
                    cc_transaction_ln_dict['SML4_CRE_DT']       =   sv_date.strftime('%d-%m-%Y %I:%M:%S %p')
                    cc_transaction_ln_dict['SML4_CRE_BY']       =   UNIT_ID
                    cc_transaction_ln_dict['SML4_NOTES']        =   ""
                    cc_transaction_ln_dict['SML4_QTY']          =   cc_trans_line.no_of_trans   #card_wise_no_sales[key]

                    credit_card_ln_writer.writerow(cc_transaction_ln_dict)

            finally:
                salecsvfile.close()
                linecsvfile.close()
                paymentcsvfile.close()
                linedetailfile.close()
                shifttrackingfile.close()
                super_shift_closing_file.close()
                super_shift_opening_file.close()
                cashier_shift_opening_file.close()
                cashier_shift_open_emp_file.close()
                cashier_shift_closing_file.close()
                credit_card_hd_file.close()
                credit_card_ln_file.close()

        inputfiles = ["OL1_MF_SO_HD_TMP.csv","OL1_MF_SO_LN_TMP.csv","OL1_MF_SO_LN_DETAIL_TMP.csv",
                          "OL1_MF_SO_PAY_TMP.csv","OL1_MF_SHIFT_TRACKING_TMP.csv","OL1_MF_SHIFT_MANHOUR_HD_TMP.csv",
                          "OL1_MF_UNIT_SHIFT_HD_TMP.csv","OL1_MF_UNIT_SHIFT_OPEN_TMP.csv","OL1_MF_UNIT_SHIFT_OPEN_EMP_TMP.csv",
                          "OL1_MF_UNIT_SHIFT_EMPLOYEE_TMP.csv","OL1_MF_SHIFT_MANHOUR_LN4_HD_TMP.csv","OL1_MF_SHIFT_MANHOUR_LN4_TMP.csv"]

        inputfiles_dict = {"OL1_MF_SO_HD_TMP.csv":"HD.txt","OL1_MF_SO_LN_TMP.csv":"LN.txt","OL1_MF_SO_LN_DETAIL_TMP.csv":"LND.txt",
                          "OL1_MF_SO_PAY_TMP.csv":"PY.txt","OL1_MF_SHIFT_TRACKING_TMP.csv":"ST.txt","OL1_MF_SHIFT_MANHOUR_HD_TMP.csv":"MAN1.txt",
                          "OL1_MF_UNIT_SHIFT_HD_TMP.csv":"S_HD.txt","OL1_MF_UNIT_SHIFT_OPEN_TMP.csv":"S_OP.txt","OL1_MF_UNIT_SHIFT_OPEN_EMP_TMP.csv":"S_OP_EMP.txt",
                          "OL1_MF_UNIT_SHIFT_EMPLOYEE_TMP.csv":"SH1.txt","OL1_MF_SHIFT_MANHOUR_LN4_HD_TMP.csv":"MAN5.txt","OL1_MF_SHIFT_MANHOUR_LN4_TMP.csv":"MAN6.txt"}

        realtime_files = []
        if available_sales_orders:
            realtime_files = ["HD.txt","LN.txt","LND.txt", "PY.txt","S_HD.txt"]
        else:
            realtime_files = ["S_HD.txt"]

        session_state_list = self.pool.get('pos.session').read(cr,uid,session_ids,['state'])
        session_states = [session_state_dict['state'] for session_state_dict in session_state_list]

        if 'opened' in session_states and 'closed' in session_states:
            inprogress_shifts_only = False
        elif 'opened' in session_states and 'closed' not in session_states:
            inprogress_shifts_only = True  #data to be sent every 30 minutes
        elif 'opened' not in session_states and 'closed' in session_states:
            inprogress_shifts_only = False


#         for infile in inputfiles:
#             infile_list = infile.split('_TMP')
#             outfile = "".join(infile_list)
#             print infile, outfile
#             with open(infile,'r') as f:
#                 with open(outfile,'w') as f1:
#                     f.next() # skip header line
#                     for line in f:
#                         f1.write(line)
        for infile,outfile in inputfiles_dict.iteritems():
            with open(full_path(base_url,infile),'r') as f:
                with open(full_path(base_url,outfile),'w') as f1:
                    f.next() # skip header line
                    for line in f:
                        f1.write(line)

        emis_connect_obj.set_prepared(cr,uid,emis_connect_id)
        ###creating a zip file
        zip_filename =  full_path(base_url, emis_connect_dict['name'] + ".zip" )
        zf = zipfile.ZipFile(zip_filename, mode='w')
        try:
            if inprogress_shifts_only:
                for key,value in inputfiles_dict.iteritems():
                    if value in realtime_files:
                        zf.write(full_path(base_url,key))
                        zf.write(full_path(base_url,value))
            else:
                for key,value in inputfiles_dict.iteritems():
                    zf.write(full_path(base_url,key))
                    zf.write(full_path(base_url,value))

        finally:
            zf.close()

        for infile in inputfiles_dict.iterkeys():
            os.remove(full_path(base_url,infile))
        if not cash_difference_orders:
            ### push files to server
            push_status = False
            if inprogress_shifts_only:
                push_status = self.push_files(cr, uid,realtime_files,base_url)
            else:
                push_status = self.push_files(cr, uid,inputfiles_dict.itervalues(),base_url)
            if not push_status.get('error'):
                emis_connect_obj.set_placed(cr,uid,emis_connect_id)

                ### close sessions
                session_brws = self.pool.get('pos.session').browse(cr,uid,session_ids)
                for session_brw in session_brws:
                    if session_brw.state == "closed":
                        self.pool.get('pos.session').write(cr,uid,session_brw.id,{'is_synced':True})

                ###send mail for export
                email_data = {
                              'id':emis_connect_id,
                              'name': emis_connect_dict['name'],
                              'zip' :  zip_filename
                              }
                if not inprogress_shifts_only:
                    self.send_email(cr, uid, email_data)
            else:
                subject = "EMIS Export Failed - " + emis_connect_dict['name']
                error_msg_body = "FTP Server Error : " + push_status.get('error')
                data = {'subject':subject,'body':error_msg_body,'is_error':True,'id':emis_connect_id}
                self.send_email(cr, uid, data)
        else:
            for infile in inputfiles_dict.itervalues():
                os.remove(full_path(base_url,infile))
            subject = "EMIS Export Failed - " + emis_connect_dict['name']
            error_msg_body = "Error : Cash Difference in these Orders : " + str(cash_difference_orders)
            data = {'subject':subject,'body':error_msg_body,'is_error':True,'id':emis_connect_id}
            self.send_email(cr, uid, data)

        os.remove(zip_filename)

        print "export ends"
                ### export ends
