# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import psycopg2

import time

import openerp
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import float_is_zero
from openerp.exceptions import UserError

_logger = logging.getLogger(__name__)

class restaurant_floor(osv.osv):
    _name = 'restaurant.floor'
    _columns = {
        'name':             fields.char('Floor Name', required=True, help='An internal identification of the restaurant floor'),
        'pos_config_id':    fields.many2one('pos.config','Point of Sale'),
        'background_image': fields.binary('Background Image', attachment=True, help='A background image used to display a floor layout in the point of sale interface'),
        'background_color': fields.char('Background Color', help='The background color of the floor layout, (must be specified in a html-compatible format)'),
        'table_ids':        fields.one2many('restaurant.table','floor_id','Tables', help='The list of tables in this floor'),
        'sequence':         fields.integer('Sequence',help='Used to sort Floors'),
    }

    _defaults = {
        'sequence': 1,
        'background_color': 'rgb(210, 210, 210)',
    }

    def set_background_color(self, cr, uid, id, background, context=None):
        self.write(cr, uid, [id], {'background_color': background}, context=context)

class restaurant_table(osv.osv):
    _name = 'restaurant.table'
    _columns = {
        'name':         fields.char('Table Name', size=32, required=True, help='An internal identification of a table'),
        'floor_id':     fields.many2one('restaurant.floor','Floor'),
        'shape':        fields.selection([('square','Square'),('round','Round')],'Shape', required=True),
        'position_h':   fields.float('Horizontal Position', help="The table's horizontal position from the left side to the table's center, in pixels"),
        'position_v':   fields.float('Vertical Position', help="The table's vertical position from the top to the table's center, in pixels"),
        'width':        fields.float('Width',   help="The table's width in pixels"),
        'height':       fields.float('Height',  help="The table's height in pixels"),
        'seats':        fields.integer('Seats', help="The default number of customer served at this table."),
        'color':        fields.char('Color',    help="The table's color, expressed as a valid 'background' CSS property value"),
        'active':       fields.boolean('Active',help='If false, the table is deactivated and will not be available in the point of sale'),
        'pos_order_ids':fields.one2many('pos.order','table_id','Pos Orders', help='The orders served at this table'),
    }

    _defaults = {
        'shape': 'square',
        'seats': 1,
        'position_h': 10,
        'position_v': 10,
        'height': 50,
        'width':  50,
        'active': True,
    }

    def create_from_ui(self, cr, uid, table, context=None):
        """ create or modify a table from the point of sale UI.
            table contains the table's fields. If it contains an
            id, it will modify the existing table. It then 
            returns the id of the table.  """

        if table.get('floor_id',False):
            floor_id = table['floor_id'][0]
            table['floor_id'] = floor_id

        if table.get('id',False):   # Modifiy existing table
            table_id = table['id']
            del table['id']
            self.write(cr, uid, [table_id], table, context=context)
        else:
            table_id = self.create(cr, uid, table, context=context)

        return table_id

class restaurant_printer(osv.osv):
    _name = 'restaurant.printer'

    _columns = {
        'name' : fields.char('Printer Name', size=32, required=True, help='An internal identification of the printer'),
        'proxy_ip': fields.char('Proxy IP Address', size=32, help="The IP Address or hostname of the Printer's hardware proxy"),
        'product_categories_ids': fields.many2many('pos.category','printer_category_rel', 'printer_id','category_id',string='Printed Product Categories'),
        'network_ip' : fields.char('Network IP'),
        'printer_type': fields.selection([('dot_matrix','Dot Matrix'),('thermal','Thermal')], 'Printer Type')
    }

    _defaults = {
        'name' : 'Printer',
    }

class pos_config(osv.osv):
    _inherit = 'pos.config'
    _columns = {
        'iface_splitbill': fields.boolean('Bill Splitting', help='Enables Bill Splitting in the Point of Sale'),
        'iface_printbill': fields.boolean('Bill Printing', help='Allows to print the Bill before payment'),
        'iface_orderline_notes': fields.boolean('Orderline Notes', help='Allow custom notes on Orderlines'),
        'floor_ids':       fields.one2many('restaurant.floor','pos_config_id','Restaurant Floors', help='The restaurant floors served by this point of sale'),
        'printer_ids':     fields.many2many('restaurant.printer','pos_config_printer_rel', 'config_id','printer_id',string='Order Printers'),
    }
    _defaults = {
        'iface_splitbill': False,
        'iface_printbill': False,
    }
            
class pos_order(osv.osv):
    _inherit = 'pos.order'
    _columns = {
        'table_id': fields.many2one('restaurant.table','Table', help='The table where this order was served'),
        'customer_count' : fields.integer('Guests', help='The amount of customers that have been served by this order.'),
    }

    def _order_fields(self, cr, uid, ui_order, context=None):
	_logger.info('Order information (%s)', (ui_order))
        fields = super(pos_order,self)._order_fields(cr,uid,ui_order,context)
        fields['table_id']       = ui_order.get('table_id',0)
        fields['customer_count'] = ui_order.get('customer_count',0)
#        fields['config_id'] = ui_order.get('config_id',False)
        return fields


#    def _process_order(self, cr, uid, order, context=None):
#        if context is None: context={}
#        prec_acc = self.pool.get('decimal.precision').precision_get(cr, uid, 'Account')
#        session = self.pool.get('pos.session').browse(cr, uid, order['pos_session_id'], context=context)
#
#        if session.state == 'closing_control' or session.state == 'closed':
#            session_id = self._get_valid_session(cr, uid, order, context=context)
#            if session_id:
#                session = self.pool.get('pos.session').browse(cr, uid, session_id, context=context)
#                order['pos_session_id'] = session_id
#            else:
#                raise UserError(_("No active cashier session.\n Please contact the cashier."))
#        if not context.get('order_id',False):
#            order_id = self.create(cr, uid, self._order_fields(cr, uid, order, context=context),context=context)
#        else:
#            order_id = context['order_id']
#            self.write(cr,uid,[order_id],self._order_fields(cr, uid, order, context=context),context=context)
#        journal_ids = set()
#	_logger.info('Checking duplicate payment (%s)', (order['statement_ids']))
#        for payments in order['statement_ids']:
#            if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
#                self.add_payment(cr, uid, order_id, self._payment_fields(cr, uid, payments[2], context=context), context=context)
#            journal_ids.add(payments[2]['journal_id'])
#
#        if session.sequence_number <= order['sequence_number']:
#            session.write({'sequence_number': order['sequence_number'] + 1})
#            session.refresh()
#
#        if not float_is_zero(order['amount_return'], self.pool.get('decimal.precision').precision_get(cr, uid, 'Account')):
#            cash_journal = session.cash_journal_id.id
#            if not cash_journal:
#                # Select for change one of the cash journals used in this payment
#                cash_journal_ids = self.pool['account.journal'].search(cr, uid, [
#                    ('type', '=', 'cash'),
#                    ('id', 'in', list(journal_ids)),
#                ], limit=1, context=context)
#                if not cash_journal_ids:
#                    # If none, select for change one of the cash journals of the POS
#                    # This is used for example when a customer pays by credit card
#                    # an amount higher than total amount of the order and gets cash back
#                    cash_journal_ids = [statement.journal_id.id for statement in session.statement_ids
#                                        if statement.journal_id.type == 'cash']
#                    if not cash_journal_ids:
#                        raise UserError(_("No cash statement found for this session. Unable to record returned cash."))
#                cash_journal = cash_journal_ids[0]
#            self.add_payment(cr, uid, order_id, {
#                'amount': -order['amount_return'],
#                'payment_date': time.strftime('%Y-%m-%d %H:%M:%S'),
#                'payment_name': _('return'),
#                'journal': cash_journal,
#            }, context=context)
#        return order_id
#
#    def create_from_ui(self, cr, uid, orders, context=None):
#        if context is None:
#            context = {}
#        # Keep only new orders
#        submitted_references = [o['data']['name'] for o in orders]
#        existing_order_ids = self.search(cr, uid, [('pos_reference', 'in', submitted_references)], context=context)
#        existing_orders = self.read(cr, uid, existing_order_ids, ['pos_reference'], context=context)
#        existing_references = set([o['pos_reference'] for o in existing_orders])
#        orders_to_save = [o for o in orders if o['data']['name'] not in existing_references]
#        orders_to_update = [o for o in orders if o['data']['name'] in existing_references]
#        order_ids = []
#
#        for tmp_order in orders_to_save:
#            to_invoice = tmp_order['to_invoice']
#            order = tmp_order['data']
#
#            if to_invoice:
#                self._match_payment_to_invoice(cr, uid, order, context=context)
#
#            order_id = self._process_order(cr, uid, order, context=context)
#            order_ids.append(order_id)
#
#            try:
#                self.signal_workflow(cr, uid, [order_id], 'paid')
#            except psycopg2.OperationalError:
#                # do not hide transactional errors, the order(s) won't be saved!
#                raise
#            except Exception as e:
#                _logger.error('Could not fully process the POS Order: %s', tools.ustr(e))
#
#            if to_invoice:
#                self.action_invoice(cr, uid, [order_id], context)
#                order_obj = self.browse(cr, uid, order_id, context)
#                self.pool['account.invoice'].signal_workflow(cr, SUPERUSER_ID, [order_obj.invoice_id.id], 'invoice_open')
#        for tmp_order_update in orders_to_update:
#            to_invoice = tmp_order_update['to_invoice']
#            order = tmp_order_update['data']
#
#            update_orderid = self.search(cr,uid, [('pos_reference','=',order['name'])])
#
#            context.update({'order_id': update_orderid[0],'from_pos_interface':True})
#            order_data = self.browse(cr, uid, update_orderid[0])
#	    if order_data.state == 'paid' and order_data.amount_total == order['amount_total']:
#                continue
##            if order_data.state == 'paid':
##                raise UserError(_("Order is already paid. Please remove this item and create a new order."))
#
#            order_id = self._process_order(cr, uid, order, context)
#            try:
#                self.signal_workflow(cr, uid, [order_id], 'paid')
#            except Exception as e:
#                _logger.error('Could not fully process the POS Order: %s', tools.ustr(e))
#
#            if to_invoice:
#                self.action_invoice(cr, uid, [order_id], context)
#                order_obj = self.browse(cr, uid, order_id, context)
#                self.pool['account.invoice'].signal_workflow(cr, SUPERUSER_ID, [order_obj.invoice_id.id], 'invoice_open')
#
#        return order_ids
#
#    #update existing line
#    def write(self, cr, uid, ids, vals, context=None):
#        if type(ids) is int:
#            ids = [ids]
#        if context is None: context={}
#        if context.get('from_pos_interface', False):
#            new_lines = []
#            older_lines = self.read(cr, uid, ids, ['lines'], context=context)[0]['lines']
#            if vals.get('name',False):
#                del vals['name']
#            order_line_obj = self.pool.get('pos.order.line')
#            if vals.get('lines',False):
#                for line in vals['lines']:
#                    if line[2] and line[2].get('pos_id',False):
#                        existing_lines = order_line_obj.search(cr,uid,[('order_id','=',ids[0]),('pos_id','=',line[2]['pos_id'])])
#                        if existing_lines:
#                            order_line_obj.write(cr,uid,existing_lines[0],line[2])
#                            if existing_lines[0] in older_lines:
#                                older_lines.remove(existing_lines[0])
#                        else:
#                            new_lines.append(line)
#                    else:
#                        new_lines.append(line)
#                if older_lines:
#                    order_line_obj.unlink(cr,uid,older_lines)
#            vals['lines'] = new_lines
#        return super(pos_order, self).write(cr, uid, ids, vals, context=context)
    
class pos_order_line(osv.osv):
    _inherit = "pos.order.line"

    _columns = {
        'pos_id' : fields.integer("Interface Id"),
        'note' : fields.text('Note')
    }


