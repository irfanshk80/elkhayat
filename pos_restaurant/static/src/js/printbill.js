odoo.define('pos_restaurant.printbill', function (require) {
"use strict";

var core = require('web.core');
var screens = require('point_of_sale.screens');
var gui = require('point_of_sale.gui');

var QWeb = core.qweb;

var BillScreenWidget = screens.ReceiptScreenWidget.extend({
    template: 'BillScreenWidget',
    click_next: function(){
        this.gui.show_screen('products');
    },
    click_back: function(){
        this.gui.show_screen('products');
    },
    render_receipt: function(){
        this._super();
        this.$('.receipt-paymentlines').remove();
        this.$('.receipt-change').remove();
    },
    print_web: function(){
        window.print();
    },
});

gui.define_screen({name:'bill', widget: BillScreenWidget});

var PrintBillButton = screens.ActionButtonWidget.extend({
    template: 'PrintBillButton',
    print_xml: function(){
        var order = this.pos.get('selectedOrder');
	var printer_ip = order.table.floor.network_ip || '';
        if(order.get_orderlines().length > 0){
            console.log('ORder--',order)
            var receipt = order.export_for_printing();
            receipt.bill = true;
            if(order.pos_waiter != null){
                receipt.pos_waiter = order.pos_waiter;
            }else{
                receipt.pos_waiter = receipt.cashier;
            }
            this.pos.proxy.print_receipt(QWeb.render('BillReceipt',{
                receipt: receipt, widget: this, pos: this.pos, order: order,
            }),{printer_ip: printer_ip});
            
            if(order.hasChangesToPrint()){
                order.printChanges();
                order.saveChanges();
//                this.pos.push_order(order);
                // var configUrl = 'http://192.168.1.12:3000';
                // var socket = io(configUrl);
                // $.when(this.pos.push_order(order)).then(function () {
                // socket.emit('orderPlaced');
                // });
            }

        }
    },
    button_click: function(){
        if (!this.pos.config.iface_print_via_proxy) {
            this.gui.show_screen('bill');
        } else {
            this.print_xml();
        }
    },
});

screens.define_action_button({
    'name': 'print_bill',
    'widget': PrintBillButton,
    'condition': function(){ 
        return this.pos.config.iface_printbill;
    },
});

});
